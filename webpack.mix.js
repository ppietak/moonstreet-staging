let mix = require( 'laravel-mix' );
let argv = require( 'minimist' )( process.argv.slice( 2 ) );

mix.options( {
	processCssUrls: false,
	cssNano: {
		discardComments: {
			removeAll: true,
		},
	},
	manifest: false,
	terser: {
		extractComments: false,
		terserOptions: {
			compress: {
				drop_console: true
			},
			output: {
				comments: false,
			},
		}
	}
} );

mix.disableNotifications();

mix.setPublicPath( 'wp-content/themes/hello-elementor-child/assets/dist' );

mix.js( [ 'wp-content/themes/hello-elementor-child/assets/src/js/theme.js' ], 'wp-content/themes/hello-elementor-child/assets/dist' );
mix.sass( 'wp-content/themes/hello-elementor-child/assets/src/scss/theme.scss', 'wp-content/themes/hello-elementor-child/assets/dist/theme.css' );
mix.sass( 'wp-content/themes/hello-elementor-child/assets/src/scss/gutenberg.scss', 'wp-content/themes/hello-elementor-child/assets/dist/gutenberg.css' );
// mix.sass( 'wp-content/themes/hello-elementor-child/assets/src/admin/scss/Admin.scss', 'web/app/themes/custom-child/assets/dist/admin.css' );

if ( process.env.BROWSER_SYNC === 'true' ) {
	mix.browserSync(
		process.env.WP_HOME,
		[
			'wp-content/themes/hello-elementor-child/assets/dist/theme.css',
			'wp-content/themes/hello-elementor-child/assets/dist/theme.js',
		]
	);
}

if ( ! mix.inProduction() ) {
	mix.sourceMaps();
}
