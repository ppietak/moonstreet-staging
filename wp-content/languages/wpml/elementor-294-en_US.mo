��    p      �                      %  	   +     5     >  �   M  '     �   (	  �   �	  	  �
  �   �  '  �  F  �       �        �  �  �     �  #   �  #   �  '   �  9     $   I     n     �  ,   �     �  %   �     �     �     �       0     ,   A     n     v  3   �     �     �     �     �     �     �     �     
     )     E     `     |     �     �     �  *   �  %        =     P     c  @   u     �  )   �  )   �     &     F     f     �     �     �     �       (   "  -   K  -   y  -   �  -   �  -     -   1  -   _     �     �     �     �     �     �               5     K     a     w     �     �     �     �     �                .     E     \     r     �     �     �     �     �     �               2     M     U      ]  '   ^  '   �  -   �  .   �       �       �  �   �  �   �  �   k  �   [   '  #!  (  K"     t#  �   �#     V$  �   i$     ?%  )   R%  -   |%  &   �%  9   �%  +   &     7&     V&  0   Z&     �&  '   �&     �&     �&     �&     �&  )   �&  -   '     5'     <'  9   H'     �'     �'     �'     �'     �'     �'     �'  )   �'  �   �'     �(     �(  (  �(  �   *     �*  (  +     *,     .,     :,     J,     Z,  @   j,     �,  )   �,  )   �,     -     "-     *-     /-  -   6-  .   d-  '   �-  '   �-     �-    �-  �   �.  '  �/  �   1  �   �1  �   �2  �   R3     4     4     #4     ,4     34     ;4     B4     I4     U4     a4     m4     y4  '   �4  -   �4  )   �4  0   5     65  )   U5     5  9   �5     �5  -   �5  +   6  &   36  9   Z6  '   �6  '   �6  .   �6  -   7  @   A7     �7  )   �7     �7     �7   /krakow /lodz /warszawa /wroclaw <b>Graj w:</b> <h3><strong>FIRMY</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-firm.webp"></div>


 <font size="3">Integrujcie się w emocjonujący sposób.</font> <h3><strong>ODKRYWAJ MIEJSCA</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/03/escape-room-krakow-zagadka-w-ksiegarni-1-e1680029753152.webp"></div>


 <font size="3">Odwiedzasz rzeczywiste miejsca osadzone w fabule gry, gdzie czekają na Ciebie miłe niespodzianki.
</font> <h3><strong>PRZYJACIELE</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-przyjaciol-1.webp"></div>


 <font size="3">Zaplanuj wyjątkową przygodę z przyjaciółmi.</font> <h3><strong>RODZINY</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-rodzin1.jpg"></div>


 <font size="3">Twórzcie niezapomniane wspomnienia razem z bliskimi.</font> <h3><strong>ROZWIĄZUJ ZAGADKI</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/DSC05580-e1676923.jpg"></div>


 <font size="3">Zwiedzając miasto będziesz rozwiązywać zagadki przy pomocy rekwizytów, które znajdziesz na szlaku.  </font>
 <h3><strong>WYCIECZKI</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-szkol.webp"></div>


 <font size="3">Wycieczki szkolne stają się jeszcze bardziej edukacyjne i emocjonujące.</font>
 <h3><strong>ZWIEDZAJ MIASTO</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/03/escape-room-krakow-moonstreet.jpg"></div>


 <font size="3">Podczas przygody odkryjesz miasto, podążając za wskazówkami w telefonie i podejmiesz decyzję wpływającą na zakończenie.</font> <p><span style="font-family: var( --e-global-typography-text-font-family ), Sans-serif; font-size: 1rem; font-weight: var( --e-global-typography-text-font-weight );">Zabierz telefon i zanurz się w przygodę escape roomu na świeżym powietrzu! Odkrywaj swoje miasto, rozwiązując zagadki w nieograniczonym czasie.</span></p> <p>OTWARTE</p> <p>Twoje miasto połączone jest z dwóch miast, które łączy cienka nić, a przejście pomiędzy nimi czasem się otwiera. Gdy to się dzieje miasto spotykają tragedie i nawiedzają tajemnicze stwory!</p> <p>W PRZYGOTOWANIU</p> <p>W Warszawie osadzonej w krainie snów, w którym Klucznik rozdaje wszystkie karty, przyjdzie Ci rozwiązać szereg zadań. Wcielisz się tytułowego Kruka i wyruszysz w najbardziej heroiczną i niebezpieczną ze swoich podróży. Będziesz podążać jego ścieżką zdobywając mistyczne umiejętności i tajemnicze artefakty, Od Ciebie będzie zależało jaki los czeka Kruka na końcu.</p> <p>WYBIERZ MIASTO</p> <strong>"król dwumiasta" </strong> <strong>"warszawski kruk" </strong> <strong>DOSTĘPNE SCENARIUSZE </strong> <strong>ESCAPE CITY</strong> 
<br>
W TWOIM 
<br>
MIEŚCIE <strong>GDZIE CHCESZ GRAĆ?</strong> <strong>Wasze opinie</strong> All DLA KOGO JEST
<strong>TA PRZYGODA?</strong>  Gdańsk Jak to<br> 
<strong>działa?</strong> KRAKÓW Katowice Kraków New Gallery PRZEŻYJ
<strong>miejską przygodę</strong><br> PRZEŻYJ PRZYGODĘ NA ULICACH SWOJEGO MIASTA Poznań Rozdzielacz Tak się bawicie <br>
<strong>podczas gry</strong>
 WARSZAWA WROCŁAW Warszawa Wrocław [grw id="8193"] [grw id="8197"] [grw id="8198"] dailymotion_url-video-1139a591 editor-text-editor-12afe2c6 editor-text-editor-1374260 editor-text-editor-314cedd5 editor-text-editor-473901f3 editor-text-editor-48310f09 editor-text-editor-49510000 editor-text-editor-52c04002 filters_all_text-uael-image-gallery-14a4d5 gallery-gallery_title-47bd416-300f7c0 html-html-2e1a54cd html-html-7f6f1469 html-html-f8c0f18 https://moonstreet.pl/wp-content/uploads/2023/03/hero-mobile.mp4 https://vimeo.com/235215203 https://www.dailymotion.com/video/x6tqhqb https://www.youtube.com/embed/ilQ4W42QchY icon-list-text-4fa4705f-6b44671 icon-list-text-771543e9-6b44671 icon-list-text-771543e9-980f2bc icon-list-text-771543e9-fc019a2 icon-list-url-4fa4705f-6b44671 icon-list-url-771543e9-6b44671 icon-list-url-771543e9-980f2bc icon-list-url-771543e9-fc019a2 show_all_galleries_label-gallery-47bd416 testimonial-carousel-content-4131bf49-43c4196 testimonial-carousel-content-4131bf49-c805a51 testimonial-carousel-content-4131bf49-d290f6b testimonial-carousel-content-7e77e750-43c4196 testimonial-carousel-content-7e77e750-c805a51 testimonial-carousel-content-7e77e750-d17eab9 testimonial-carousel-content-7e77e750-d290f6b text-button-289b1756 text-button-3fdb6993 text-button-4bfd7870 text-button-4c8bb23f text-button-6b519650 text-button-6ec8a2f0 text-button-7a19d93 text-divider-1a7f0c14 text-divider-1b39922c text-divider-3e8a5f60 text-divider-4bfd0c50 text-divider-5284d29f title-heading-145a3595 title-heading-15e31d66 title-heading-21a70d23 title-heading-299e56c7 title-heading-3a447081 title-heading-4b052543 title-heading-4bfe6a64 title-heading-55dbe7bf title-heading-647c2c8b title-heading-7864ec1 title-heading-7b1a17ad title-heading-7f5c17ac title-heading-83a4735 url-button-289b1756 url-button-3fdb6993 url-button-6b519650 url-button-6ec8a2f0 url-video-1139a591 vimeo_url-video-1139a591 youtube_url-video-1139a591 ŁÓDŹ Łódź  https://moonstreet.pl/en/?page_id=10761 https://moonstreet.pl/en/?page_id=10760 https://moonstreet.pl/en/warsaw-landing-page/ https://moonstreet.pl/en/wroclaw-landing-page/ <b>Play in:</b> <h3><strong>COMPANIES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-firm.webp"></div>


 <font size="3">Integrate in an exciting way.</font> <h3><strong>DISCOVER PLACES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/03/escape-room-krakow-zagadka-w-ksiegarni-1-e1680029753152.webp"></div>


 <font size="3">You visit real places set in the game's plot, where pleasant surprises await you.
</font> <h3><strong>ADVERTISEMENTS</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-przyjaciol-1.webp"></div>


 <font size="3">Plan a unique adventure with your friends.</font> <h3><strong>FAMILIES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-rodzin1.jpg"></div>


 <font size="3">Create unforgettable memories together with your loved ones.</font> <h3><strong>SOLVE PUZZLES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/DSC05580-e1676923.jpg"></div>


 <font size="3">As you explore the city, you'll solve puzzles using props you find along the trail.  </font>
 <h3><strong>EXCLUSIONS</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-szkol.webp"></div>


 <font size="3">School trips become even more educational and exciting.</font>
 <h3><strong>EXPLORE THE CITY</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/03/escape-room-krakow-moonstreet.jpg"></div>


 <font size="3">During the adventure, you will discover the city by following the clues on your phone and make a decision affecting the ending.</font> <p><span style="font-family: var( --e-global-typography-text-font-family ), Sans-serif; font-size: 1rem; font-weight: var( --e-global-typography-text-font-weight );">Take your phone and dive into an outdoor escape room adventure! Explore your city by solving puzzles in unlimited time.</span></p> <p>OPEN</p> <p>Your city is connected from two cities, which are linked by a thin thread, and the passage between them sometimes opens. When this happens the city is met with tragedies and haunted by mysterious creatures!</p> <p>IN PROGRESS</p> <p>Your city is connected from two cities, which are linked by a thin thread, and the passage between them sometimes opens. When this happens the city is met with tragedies and haunted by mysterious creatures!</p> <p>SELECT CITY</p> <strong>"king of the twin city" </strong> <strong>"in the footsteps of raven" </strong> <strong>SCENARIOS AVAILABLE  </strong> <strong>ESCAPE CITY</strong> 
<br>
IN YOUR 
<br>
HOMETOWN <strong>WHERE DO YOU WANT TO PLAY?</strong> <strong>Your opinions</strong> All FOR WHOM IT IS
<strong>THIS ADVENTURE?</strong>  Gdansk How<br> 
<strong>does it work?</strong> KRAKOW Katowice Krakow New Gallery LIVE
<strong>urban adventure</strong><br> HAVE AN ADVENTURE ON THE STREETS OF YOUR CITY Poznan Distributor This is how you play  <br>
<strong>when playing</strong>
 WARSAW WROCLAW Warsaw Wroclaw [grw id="8193"] [grw id="8197"] [grw id="8198"] https://www.dailymotion.com/video/x6tqhqb <p>Your city is connected from two cities, which are linked by a thin thread, and the passage between them sometimes opens. When this happens the city is met with tragedies and haunted by mysterious creatures!</p> <p>SELECT CITY</p> <p>IN PROGRESS</p> <p><span style="font-family: var( --e-global-typography-text-font-family ), Sans-serif; font-size: 1rem; font-weight: var( --e-global-typography-text-font-weight );">Take your phone and dive into an outdoor escape room adventure! Explore your city by solving puzzles in unlimited time.</span></p> <p>Your city is connected from two cities, which are linked by a thin thread, and the passage between them sometimes opens. When this happens the city is met with tragedies and haunted by mysterious creatures!</p> <p>OPEN</p> <p><span style="font-family: var( --e-global-typography-text-font-family ), Sans-serif; font-size: 1rem; font-weight: var( --e-global-typography-text-font-weight );">Take your phone and dive into an outdoor escape room adventure! Explore your city by solving puzzles in unlimited time.</span></p> All New Gallery [grw id="8193"] [grw id="8198"] [grw id="8197"] https://moonstreet.pl/wp-content/uploads/2023/03/hero-mobile.mp4 https://vimeo.com/235215203 https://www.dailymotion.com/video/x6tqhqb https://www.youtube.com/embed/ilQ4W42QchY Warsaw Wroclaw Lodz Krakow https://moonstreet.pl/en/warsaw-landing-page/ https://moonstreet.pl/en/wroclaw-landing-page/ https://moonstreet.pl/en/?page_id=10760 https://moonstreet.pl/en/?page_id=10761 All <h3><strong>DISCOVER PLACES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/03/escape-room-krakow-zagadka-w-ksiegarni-1-e1680029753152.webp"></div>


 <font size="3">You visit real places set in the game's plot, where pleasant surprises await you.
</font> <h3><strong>SOLVE PUZZLES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/DSC05580-e1676923.jpg"></div>


 <font size="3">As you explore the city, you'll solve puzzles using props you find along the trail.  </font>
 <h3><strong>EXPLORE THE CITY</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/03/escape-room-krakow-moonstreet.jpg"></div>


 <font size="3">During the adventure, you will discover the city by following the clues on your phone and make a decision affecting the ending.</font> <h3><strong>COMPANIES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-firm.webp"></div>


 <font size="3">Integrate in an exciting way.</font> <h3><strong>EXCLUSIONS</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-szkol.webp"></div>


 <font size="3">School trips become even more educational and exciting.</font>
 <h3><strong>FAMILIES</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-rodzin1.jpg"></div>


 <font size="3">Create unforgettable memories together with your loved ones.</font> <h3><strong>ADVERTISEMENTS</strong></h3>

<div class="image"><img src="/wp-content/uploads/2023/09/dla-przyjaciol-1.webp"></div>


 <font size="3">Plan a unique adventure with your friends.</font> KRAKOW LODZ Katowice Gdansk WROCLAW WARSAW Poznan Distributor Distributor Distributor Distributor Distributor How<br> 
<strong>does it work?</strong> HAVE AN ADVENTURE ON THE STREETS OF YOUR CITY LIVE
<strong>urban adventure</strong><br> FOR WHOM IT IS
<strong>THIS ADVENTURE?</strong>  <strong>Your opinions</strong> <strong>"king of the twin city" </strong> <b>Play in:</b> This is how you play  <br>
<strong>when playing</strong>
 <b>Play in:</b> <strong>"in the footsteps of raven" </strong> <strong>WHERE DO YOU WANT TO PLAY?</strong> <strong>SCENARIOS AVAILABLE  </strong> <strong>ESCAPE CITY</strong> 
<br>
IN YOUR 
<br>
HOMETOWN https://moonstreet.pl/en/?page_id=10761 https://moonstreet.pl/en/?page_id=10760 https://moonstreet.pl/en/wroclaw-landing-page/ https://moonstreet.pl/en/warsaw-landing-page/ https://moonstreet.pl/wp-content/uploads/2023/03/hero-mobile.mp4 https://vimeo.com/235215203 https://www.youtube.com/embed/ilQ4W42QchY LODZ Lodz 