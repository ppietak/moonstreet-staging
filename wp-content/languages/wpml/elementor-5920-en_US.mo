��    �      �      �      �     �     �     	     	  u   #	     �	     �	     �	  �   �	  �  n
       $   (     M     Q     Z     a     s     |     �  *   �     �     �     �     �  	          #   $     H     f      r     �     �     �  e   �  m   (  F   �     �     �  
   �     
  1     =   H     �  '   �  '   �  '   �  S   
  !   ^  D   �     �  "   �  "   �  "     "   7  !   Z  "   |  "   �  "   �  "   �  "     "   +  #   N  #   r  #   �  #   �  #   �  #     #   &  #   J  #   n  "   �     �     �     �     �     �          4     P     l     �     �     �     �  &   �          3     A     P  )   c     �  )   �     �     �     �          (     >     T     j     �     �     �     �     �     �               2     I     `     w     �     �     �     �     �          (     E     a     ~     �     �     �     �          -     K     i     �     �     �     �     �          :     N     Z  7   l      �     �     �     �     �  x   �     j     y     �  �   �  j  J     �  !   �     �  	   �            	            #      ,   #   9      ]      k            �      �      �      �      �      �      
!     (!  
   6!     A!  k   Q!  r   �!  @   0"     q"     �"     �"     �"  )   �"  -   �"     �"  +   #  ,   7#  )   d#  M   �#     �#  D   �#  	   A$     K$  
   k$     v$  )   �$  r   �$  ;   )%     e%     y%     �%     �%     �%  @   �%     &  k   &  
   �&     �&     �&     �&     �&     �&  -   	'     7'     K'     Q'     ^'     o'  j  ~'     �)     �)     *     *  �   %*  x   �*  j  S+     �-     �-     �-     �-     �-  *   	.  D   4.     y.     }.     �.  	   �.     �.     �.     �.     �.     �.     �.     �.     �.     /     /     /     %/  )   @/  *   j/  #   �/  !   �/  +   �/  ,   0     40  M   O0     �0     �0     �0     �0     �0     �0     �0      1  	   1     1     $1     ,1     91     B1     P1     ]1     q1     w1  	   �1     �1     �1     �1     �1  	   �1  ;   �1   /faq/ <h3>GRACZE</h3> <h3>OGÓLNE</h3> <h3>TRASA</h3> <h4>Poniedziałek - ZAMKNIĘTE</h4>
<h4>Wtorek - Piątek - 12:00-19:00</h4>
<h4>Sobota - Niedziela - 10:00-19:00</h4> <p>GRACZE</p> <p>OGÓLNE</p> <p>TRASA</p> <p>Trasa rozgrywki przebiega przez kilka punktów stacjonarnych, które mają swoje godziny otwarcia. Dlatego grę zalecamy aby rozpocząć w poniższych godzinach :)</p> <p>Łódź składa się z dwóch miast, między którymi istnieje cienka, niewidzialna bariera. Czasami ta granica otwiera się, umożliwiając mrocznej stronie miasta przedostanie się do naszego świata. W takich sytuacjach miasto staje się miejscem wielu tragedii i domem dla licznych potworów. Na straży granicy stoi tylko Król Dwumiasta.</p><p>W grze wcielasz się w rolę wybrańca, którego zadaniem jest zastąpienie obecnego Króla. Przejdź przez liczne próby, które sprawdzą, czy podołasz utrzymać równowagę. Podczas podróży podejmuj decyzje, które prowadzą do różnych zakończeń, a los Łodzi i samego Króla zależy już tylko od Ciebie.</p> <strong>Fabuła</strong> <strong>PUNKT</strong>
<br>
STARTOWY All Czas gry DZIECI Dostępne języki Drużyny Dzieci Długość trasy Godziny <br>
<strong>rozpoczęcia</strong> Godziny rozpoczęcia Gra przyjazna dzieciom Już od +1 osoby Już od 1 osoby Kup bilet Liczba graczy Możesz podzielić się na drużyny Możliwość podzielenia się New Gallery Nie dokonuje się rezerwacji :)  Około 2 godziny Około 4 km Polski, Angielski Poniedziałek - ZAMKNIĘTE 
<br>Wtorek - Piątek - 12:00-19:00 
<br> Sobota - Niedziela - 10:00-19:00 Poniedziałek - zamknięte 
<br>Wtorek - piątek od 11:50 do 19:00 
<br>Sobota i niedziela - od 9:50 do 19:00 Przyjazny rodzinom z dziećmi  <br />
i osób z niepełnosprawnością Punkt startowy Punkty stacjonarne Rezerwacja Rozdzielacz Trzy miejsca w których 
możesz zrobić przerwę Trzy miejsca w których <br />
możesz zrobić sobie przerwę Ułatwienia WAŻNE
<br>
<strong>INFORMACJE</strong> Ważne <br>
<strong>informacje</strong> Zdjęcia<br>
<strong>ze szlaku</strong> Zdrój na Alei Artura Rubinsteina<br/>
(wodopój z figurami dziewczynki i chłopca) Zdrój w Aleja Artura Rubinsteina [faqs style="accordion" filter="szczegoly" order="ASC" orderby="id"] czas gry description_text-icon-box-125b2454 description_text-icon-box-205eeb47 description_text-icon-box-20b9140b description_text-icon-box-2d53d3b7 description_text-icon-box-34cf940 description_text-icon-box-36864004 description_text-icon-box-5776b48e description_text-icon-box-57e12fc2 description_text-icon-box-610324fa description_text-icon-box-6c5545c8 description_text-image-box-28d613f description_text-image-box-4dd22472 description_text-image-box-536cea0f description_text-image-box-598c045f description_text-image-box-5ade6188 description_text-image-box-6b1855e7 description_text-image-box-7234bc46 description_text-image-box-77a9ae64 description_text-image-box-7cfa0cfd description_text-image-box-7d3b3d28 description_text-image-box-c5e9bcf dostępne języki drużyny długość trasy editor-text-editor-175dd0d7 editor-text-editor-26b39fb editor-text-editor-2a23ec3f editor-text-editor-2b40d0f8 editor-text-editor-2e7fba7b editor-text-editor-55e09c96 editor-text-editor-5b9d186b editor-text-editor-710002f3 editor-text-editor-7598010d editor-text-editor-c10f10a gallery-gallery_title-73d99f95-300f7c0 godziny rozpoczęcia liczba graczy punkt startowy punkty stacjonarne pytania<br>
<strong>i odpowiedzi</strong> shortcode-shortcode-51a3999c show_all_galleries_label-gallery-73d99f95 text-button-1751910a text-button-434b2dc3 text-button-72d27cf4 text-divider-248c1d9 text-divider-2bed2416 text-divider-2fc4fd69 text-divider-45776b54 text-divider-4e6f23ba text-divider-51d4b862 text-divider-666d1a00 text-divider-680de5d6 text-divider-697d03fa text-divider-727826b9 text-divider-7f45c1cb title-heading-1548c275 title-heading-1ffd373a title-heading-2610d48a title-heading-3aa44881 title-heading-544beebd title-heading-574a30c title-heading-605d9634 title-heading-79bbe6ea title-heading-8f0c8de title_text-icon-box-125b2454 title_text-icon-box-205eeb47 title_text-icon-box-20b9140b title_text-icon-box-2d53d3b7 title_text-icon-box-34cf940 title_text-icon-box-36864004 title_text-icon-box-5776b48e title_text-icon-box-57e12fc2 title_text-icon-box-610324fa title_text-icon-box-6c5545c8 title_text-image-box-28d613f title_text-image-box-4dd22472 title_text-image-box-536cea0f title_text-image-box-598c045f title_text-image-box-5ade6188 title_text-image-box-6b1855e7 title_text-image-box-7234bc46 title_text-image-box-77a9ae64 title_text-image-box-7cfa0cfd title_text-image-box-7d3b3d28 title_text-image-box-c5e9bcf url-button-72d27cf4 ułatwienia zobacz pozostałe Łatwość trasy dla osób 
z dziećmi oraz na wózkach  https://moonstreet.pl/faq/ <h3>PLAYERS</h3> <h3>GENERAL</h3> <h3>TRACK</h3> <h4>Monday - CLOSED</h4>
<h4>Tuesday - Friday - 12:00 - 7:00 pm</h4>
<h4>Saturday - Sunday - 10:00 a.m. - 7:00 p.m.</h4> <p>PLAYERS</p> <p>GENERAL</p> <p>TRACK</p> <p>The route of the game passes through several stationary points, which have their own opening hours. Therefore, we recommend that you start the game at the following times :)</p> <p>Lodz consists of two cities, between which there is a thin, invisible barrier. Sometimes this boundary opens, allowing the dark side of the city to enter our world. In such situations, the city becomes the site of many tragedies and home to numerous monsters. Only the King of the Two Cities guards the border.</p><p>In the game, you play the role of a chosen one whose task is to replace the current King. Go through numerous tests to see if you can manage to keep your balance. During your journey, make decisions that lead to different endings, and the fate of the boat and the King himself is now up to you.</p> <strong>Storyline</strong> <strong>POINT</strong>
<br>
START All Game time CHILDREN Available languages Teams Children Route length Opening <br>
<strong>hours</strong> Opening hours Child-friendly game As early as +1 person From as little as 1 person Buy a ticket Number of players You can divide into teams Opportunity to share New Gallery No reservations are made :)   About 2 hours About 4 km Polish, English Monday - CLOSED  
<br>Tuesday - Friday - 12:00 - 7:00 pm  
<br>  Saturday - Sunday - 10:00 a.m. - 7:00 p.m. Monday - closed 
<br>Tuesday - Friday from 11:50 am to 7:00 pm  
<br>Saturday and Sunday - from 9:50 am to 7:00 pm Family-friendly with children  <br>
and people with disabilities Starting point Stationary points Booking Distributor Three places where  
you can take a break Three places where  <br>
you can take a break Facilitation IMPORTANT
<br>
<strong>INFORMATION</strong> Important  <br>
<strong>information</strong> Photos<br>
<strong>off the trail</strong> Spa on Arthur Rubinstein Avenue<br>
(watering hole with girl and boy figures) Spa in Arthur Rubinstein Avenue [faqs style="accordion" filter="szczegoly" order="ASC" orderby="id"] game time Spa in Arthur Rubinstein Avenue About 4 km As early as +1 person Three places where  
you can take a break Monday - closed 
<br>Tuesday - Friday from 11:50 am to 7:00 pm  
<br>Saturday and Sunday - from 9:50 am to 7:00 pm Ease of route for people  
with children and in wheelchairs Child-friendly game Opportunity to share About 2 hours Polish, English No reservations are made :)   Family-friendly with children  <br>
and people with disabilities Child-friendly game Monday - CLOSED  
<br>Tuesday - Friday - 12:00 - 7:00 pm  
<br>  Saturday - Sunday - 10:00 a.m. - 7:00 p.m. About 4 km Polish, English You can divide into teams Spa in Arthur Rubinstein Avenue About 2 hours From as little as 1 person Three places where  <br>
you can take a break languages available teams route length <h3>PLAYERS</h3> <p>PLAYERS</p> <p>Lodz consists of two cities, between which there is a thin, invisible barrier. Sometimes this boundary opens, allowing the dark side of the city to enter our world. In such situations, the city becomes the site of many tragedies and home to numerous monsters. Only the King of the Two Cities guards the border.</p><p>In the game, you play the role of a chosen one whose task is to replace the current King. Go through numerous tests to see if you can manage to keep your balance. During your journey, make decisions that lead to different endings, and the fate of the boat and the King himself is now up to you.</p> <p>TRACK</p> <h3>TRACK</h3> <p>GENERAL</p> <h3>GENERAL</h3> <p>The route of the game passes through several stationary points, which have their own opening hours. Therefore, we recommend that you start the game at the following times :)</p> <h4>Monday - CLOSED</h4>
<h4>Tuesday - Friday - 12:00 - 7:00 pm</h4>
<h4>Saturday - Sunday - 10:00 a.m. - 7:00 p.m.</h4> <p>Lodz consists of two cities, between which there is a thin, invisible barrier. Sometimes this boundary opens, allowing the dark side of the city to enter our world. In such situations, the city becomes the site of many tragedies and home to numerous monsters. Only the King of the Two Cities guards the border.</p><p>In the game, you play the role of a chosen one whose task is to replace the current King. Go through numerous tests to see if you can manage to keep your balance. During your journey, make decisions that lead to different endings, and the fate of the boat and the King himself is now up to you.</p> New Gallery start times number of players starting point stationary points questions<br>
<strong>and answers</strong> [faqs style="accordion" filter="szczegoly" order="ASC" orderby="id"] All Buy a ticket Buy a ticket see other Distributor Distributor Distributor Distributor Distributor Distributor Distributor Distributor Distributor Distributor Distributor <strong>Storyline</strong> Photos<br>
<strong>off the trail</strong> questions<br>
<strong>and answers</strong> Opening <br>
<strong>hours</strong> <strong>POINT</strong>
<br>
START IMPORTANT
<br>
<strong>INFORMATION</strong> Important  <br>
<strong>information</strong> <strong>Storyline</strong> Spa on Arthur Rubinstein Avenue<br>
(watering hole with girl and boy figures) starting point route length number of players stationary points start times facilitations Children teams Game time languages available Booking Facilitation CHILDREN Opening hours Route length Available languages Teams Starting point game time Number of players Stationary points https://moonstreet.pl/faq/ facilitations see other Ease of route for people  
with children and in wheelchairs 