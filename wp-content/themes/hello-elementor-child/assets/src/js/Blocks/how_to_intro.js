class HowToIntro {
    constructor() {
        this.customSelectFilter = document.querySelector('.js--how-to-intro-custom-select-filter');
        this.customSelectDropdown = document.querySelector('.js--how-to-intro-custom-select-dropdown');
        this.container = document.querySelector('.page-content');
        this.inputs = document.querySelectorAll('.js--how-to-intro-custom-select-dropdown input[type="radio"]');

        this.ajaxUrl = ajaxurl;
        this.homeUrl = homeurl;

        this.sectionID = null;
        this.localizationName = '';
        this.localizationSlug = '';

        if (!this.customSelectFilter) {
            return;
        }

        this.toggleDropdown();
        this.selectVal();
        // this.observer();

    }

    toggleDropdown() {
        // this.customSelectFilter.addEventListener('click', event => {
        //     this.customSelectDropdown.classList.toggle('active');
        // })

        // document.addEventListener('click', event => {
        //     if (!event.target.closest('.js--how-to-intro-custom-select-filter')) {
        //         this.customSelectDropdown.classList.remove('active');
        //     }
        // })

        document.addEventListener('click', event => {
            if (event.target.closest('.js--how-to-intro-custom-select-filter')) {
                event.target.parentNode.querySelector('.js--how-to-intro-custom-select-dropdown').classList.toggle('active');
            } else {
                document.querySelectorAll('.js--how-to-intro-custom-select-dropdown').forEach(element => {
                    element.classList.remove('active');
                })
            }
        })
    }

    reloadContent() {
        fetch(this.ajaxUrl, {
            method: "post",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: "action=get_new_content" + "&name=" + this.localizationName + "&slug=" + this.localizationSlug + "&id=" + this.sectionID,
        })
        .then( res => res.text() )
        
        .then( data => this.container.innerHTML = data )
        .catch( error => console.log(error) )
    }

    selectVal() {
        // this.inputs.forEach(element => {
        //     element.addEventListener('click', event => {
        //         this.localizationName = element.getAttribute('data-name');
        //         this.localizationSlug = element.getAttribute('id');
        //         this.sectionID = parseInt(element.getAttribute('data-id'));

        //         this.reloadContent();
        //     })
        // })

        document.addEventListener('click', event => {
            if (event.target.closest('.js--how-to-intro-custom-select-dropdown input[type="radio"]')) {
                this.localizationName = event.target.getAttribute('data-name');
                this.localizationSlug = event.target.getAttribute('id');
                this.sectionID = parseInt(event.target.getAttribute('data-id'));

                this.reloadContent();

                const obj = { Title: 'some title', Url: currentpageurl + '?id=' + this.localizationSlug };
                history.pushState(obj, obj.Title, obj.Url);
            }
        })
    }

    observer() {

        // Tworzymy nową instancję MutationObserver i przekazujemy funkcję zwrotną, która zostanie wywołana za każdym razem, gdy zostanie wykryta zmiana
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
            // Przetwarzamy każdą pojedynczą mutację
            // console.log(mutation.type); // Typ mutacji (np. "attributes", "childList", "characterData")
            // console.log(mutation.target); // Element, który uległ mutacji
        
            // Dodatkowe informacje zależne od typu mutacji
            // if (mutation.type === 'attributes') {
            //     console.log('Zmiana atrybutu:', mutation.attributeName);
            //     console.log('Nowa wartość:', mutation.target.getAttribute(mutation.attributeName));
            // } else if (mutation.type === 'childList') {
            //     console.log('Dodane węzły:', mutation.addedNodes);
            //     console.log('Usunięte węzły:', mutation.removedNodes);
            // } else if (mutation.type === 'characterData') {
            //     console.log('Zmienione dane tekstu:', mutation.target.data);
            // }

            if (mutation.type === 'childList') {
                
                const queryString = window.location.search;
                console.log(queryString);
        
                const urlParams = new URLSearchParams(queryString);
        
                const loc_id = urlParams.get('id');
        
                console.log(loc_id);
            }


            });
        });
        
        // Konfigurujemy naszego obserwatora, określając, które typy zmian chcemy śledzić
        var config = { attributes: false, childList: true, subtree: false, characterData: false };
        
        // Rozpoczynamy obserwację wybranego elementu (w tym przypadku cały dokument)
        observer.observe(document.querySelector('.page-content'), config);
        
        // Aby przerwać obserwację, używamy metody disconnect
        // observer.disconnect();
  
    }
}

export default HowToIntro;