<section id="how-to-steps" class="how-to-steps">
    <?php if ( have_rows( 'how_to_steps' ) ) : ?>   
        <?php while ( have_rows( 'how_to_steps' ) ) : the_row(); ?>
            <div class="how-to-steps__container">
                <div class="how-to-steps__steps">
                    <?php if ( have_rows( 'step_1' ) ) : ?>   
                        <?php while ( have_rows( 'step_1' ) ) : the_row();
                        $icon   = get_sub_field( 'icon' );
                        $title = get_sub_field( 'title' );
                        $desc    = get_sub_field( 'description' );
                        $label    = get_sub_field( 'label' );
                        $url    = get_sub_field( 'url' );
                        ?>
                            <div class="how-to-steps__step how-to-steps__step--step-1">
                                <?php if ($icon) : ?>
                                    <div class="icon">
                                        <?php
                                        echo wp_get_attachment_image(
                                            $icon,
                                            'how_to_steps_icon',
                                            false,
                                            [
                                                'loading' => 'lazy',
                                            ]
                                        );
                                        ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($title) : ?>
                                    <h3 class="title"><?php echo wp_kses_post($title); ?></h3>
                                <?php endif; ?>
                                <?php if ($desc) : ?>
                                    <p class="desc"><?php echo wp_kses_post($desc); ?></p>
                                <?php endif; ?>
                                <?php if ($url) : ?>
                                    <div class="url">
                                        <?php if ($label) : ?>
                                            <span class="label"><?php echo wp_kses_post($label); ?></span>
                                        <?php endif; ?>
                                        <a href="<?php echo esc_url($url); ?>"><?php echo esc_html_e('tutaj', 'hello-elementor-child'); ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>

                    <?php if ( have_rows( 'step_2' ) ) : ?>   
                        <?php while ( have_rows( 'step_2' ) ) : the_row();
                        $icon   = get_sub_field( 'icon' );
                        $title = get_sub_field( 'title' );
                        $desc    = get_sub_field( 'description' );
                        $desc2    = get_sub_field( 'description_2' );
                        $link    = get_sub_field( 'link' );
                        ?>
                            <div class="how-to-steps__step how-to-steps__step--step-2">
                                <?php if ($icon) : ?>
                                    <div class="icon">
                                        <?php
                                        echo wp_get_attachment_image(
                                            $icon,
                                            'how_to_steps_icon',
                                            false,
                                            [
                                                'loading' => 'lazy',
                                            ]
                                        );
                                        ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($title) : ?>
                                    <h3 class="title"><?php echo wp_kses_post($title); ?></h3>
                                <?php endif; ?>
                                <?php if ($desc) : ?>
                                    <p class="desc"><?php echo wp_kses_post($desc); ?></p>
                                <?php endif; ?>
                                <?php if ($link) : ?>
                                    <a class="link" href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr( $link['target'] ); ?>"><?php echo esc_html( $link['title'] ); ?></a>
                                <?php endif; ?>
                                <?php if ($desc2) : ?>
                                    <p class="desc2"><?php echo wp_kses_post($desc2); ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>

                    <?php if ( have_rows( 'step_3' ) ) : ?>   
                        <?php while ( have_rows( 'step_3' ) ) : the_row();
                        $icon   = get_sub_field( 'icon' );
                        $title = get_sub_field( 'title' );
                        $desc    = get_sub_field( 'description' );
                        ?>
                            <div class="how-to-steps__step how-to-steps__step--step-3">
                                <?php if ($icon) : ?>
                                    <div class="icon">
                                        <?php
                                        echo wp_get_attachment_image(
                                            $icon,
                                            'how_to_steps_icon',
                                            false,
                                            [
                                                'loading' => 'lazy',
                                            ]
                                        );
                                        ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($title) : ?>
                                    <h3 class="title"><?php echo wp_kses_post($title); ?></h3>
                                <?php endif; ?>
                                <?php if ($desc) : ?>
                                    <p class="desc"><?php echo wp_kses_post($desc); ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>   
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</section>