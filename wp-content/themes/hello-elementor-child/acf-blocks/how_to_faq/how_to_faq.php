<?php 
$faqs = new Quick_And_Easy_FAQs\Includes\FAQs();
// echo $faqs->get_faqs_markup( 'accordion', 'reservation', 'id', 'ASC' );
?>

<section id="how-to-faq" class="how-to-faq">
    <?php if ( have_rows( 'how_to_faq' ) ) : ?>   
        <?php while ( have_rows( 'how_to_faq' ) ) : the_row(); ?>
            <div class="how-to-faq__container">

                <?php if ( have_rows( 'column_1' ) ) : ?>
                    <?php while ( have_rows( 'column_1' ) ) : the_row(); ?>
                        <div class="how-to-faq__col how-to-faq__col--column-1">
                            
                            <?php if ( have_rows( 'video_block' ) ) : ?>
                                <div class="video-block">
                                    <?php while ( have_rows( 'video_block' ) ) : the_row(); 
                                    $yt_url = get_sub_field('youtube_url'); 
                                    $tytul_linia_1 = get_sub_field('tytul_linia_1'); 
                                    $tytul_linia_2 = get_sub_field('tytul_linia_2');
                                    $desc = get_sub_field('description');
                                    $time = get_sub_field('time');
                                    $number = get_sub_field('number');
                                    $language = get_sub_field('language');
                                    $btn = get_sub_field('button');
                                    ?>
                                        <?php if ($yt_url) : ?>
                                            <div class="video">
                                                <iframe width="420" height="315" src="<?php echo esc_url($yt_url); ?>" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($tytul_linia_1 || $tytul_linia_2) : ?>
                                            <div class="title">
                                                <?php if ($tytul_linia_1) : ?>
                                                    <h5 class="title-1"><?php echo wp_kses_post($tytul_linia_1); ?></h5>
                                                <?php endif; ?>
                                                <?php if ($tytul_linia_2) : ?>
                                                    <h5 class="title-2"><?php echo wp_kses_post($tytul_linia_2); ?></h5>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($desc) : ?>
                                            <p class="desc"><?php echo wp_kses_post($desc); ?></p>
                                        <?php endif; ?>
                                        <?php if ($time) : ?>
                                            <div class="time">
                                                <span><?php echo esc_html_e('Czas gry:', 'hello-elementor-child'); ?></span>
                                                <span><?php echo wp_kses_post($time); ?></span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($number) : ?>
                                            <div class="number">
                                                <span><?php echo esc_html_e('Liczba graczy:', 'hello-elementor-child'); ?></span>
                                                <span><?php echo wp_kses_post($number); ?></span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($language) : ?>
                                            <div class="language">
                                                <span><?php echo esc_html_e('Dostępny język:', 'hello-elementor-child'); ?></span>
                                                <span><?php echo wp_kses_post($language); ?></span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($btn) : ?>
                                            <a class="btn" href="<?php echo esc_url( $btn['url'] ); ?>" target="<?php echo esc_attr( $btn['target'] ); ?>"><?php echo esc_html( $btn['title'] ); ?></a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>

                            <?php if ( have_rows( 'remaining' ) ) : ?>   
                                <?php while ( have_rows( 'remaining' ) ) : the_row();
                                $img   = get_sub_field( 'image' );
                                $ttl = get_sub_field( 'title' );
                                ?>
                                    <div class="remaining-block">
                                        <?php if ($img) : ?>
                                            <div class="image">
                                                <?php
                                                echo wp_get_attachment_image(
                                                    $img,
                                                    'faq_remaining_img',
                                                    false,
                                                    [
                                                        'loading' => 'lazy',
                                                    ]
                                                );
                                                ?>
                                            </div>
                                        <?php endif; ?>
                                        <div class="meta">
                                            <?php if ($ttl) : ?>
                                                <h6 class="title"><?php echo wp_kses_post($ttl); ?></h6>
                                            <?php endif; ?>
                                            <?php if ( have_rows( 'available' ) ) : ?> 
                                                <div class="cities">
                                                    <span><?php echo esc_html_e('Dostępne miasta:', 'hello-elementor-child'); ?></span>
                                                    <?php while ( have_rows( 'available' ) ) : the_row(); 
                                                    $link = get_sub_field( 'link' );
                                                    ?>
                                                        <?php if ($link) : ?>
                                                            <a class="link" href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr( $link['target'] ); ?>"><?php echo esc_html( $link['title'] ); ?></a>
                                                        <?php endif; ?>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

                <?php if ( have_rows( 'column_2' ) ) : ?>
                    <?php while ( have_rows( 'column_2' ) ) : the_row(); 
                    $btn = get_sub_field( 'button' );
                    ?>
                        <div class="how-to-faq__col how-to-faq__col--column-2">
                            
                            <div class="heading">
                                <h2 class="heading__title">Pytania <span>i odpowiedzi</span></h2>
                            </div>
                            
                            <?php if ( have_rows( 'faq' ) ) : ?>   
                                <?php while ( have_rows( 'faq' ) ) : the_row();
                                $heading = get_sub_field( 'heading' );
                                $group    = get_sub_field( 'group' );
                                ?>
                                    <?php if ($heading) : ?>
                                        <h3 class="heading"><?php echo wp_kses_post($heading); ?></h3>
                                    <?php endif; ?>

                                    <?php echo $faqs->get_faqs_markup( 'accordion', $group, 'id', 'ASC' ); ?>

                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if ($btn) : ?>
                                <a class="btn" href="<?php echo esc_url( $btn['url'] ); ?>" target="<?php echo esc_attr( $btn['target'] ); ?>"><?php echo esc_html( $btn['title'] ); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</section>