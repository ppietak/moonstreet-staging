<?php 

?>

<section id="how-to-intro" class="how-to-intro">
    <?php if ( have_rows( 'how_to_intro' ) ) : ?>   
        <?php while ( have_rows( 'how_to_intro' ) ) : the_row();
        $heading = get_sub_field( 'heading' );
        $desc    = get_sub_field( 'description' );
        $btn    = get_sub_field( 'button' );
        $img    = get_sub_field( 'image' );
        $img    = get_sub_field( 'image' );
        $default_city    = get_sub_field( 'default_city' );
        ?>
            <div class="how-to-intro__container">
                <div class="how-to-intro__col how-to-intro__col--column-1">
                    <div class="how-to-intro__heading">
                        <?php if ($heading) : ?>
                            <h1 class="heading"><?php echo wp_kses_post($heading); ?></h1>
                        <?php else : ?>
                            <h1 class="heading">
                                <?php echo esc_html_e('Jak', 'hello-elementor-child'); ?>
                                <span><?php echo esc_html_e('grać', 'hello-elementor-child'); ?></span>
                            </h1>
                        <?php endif; ?>
                    </div>
                    <?php if ($desc) : ?>
                        <p class="desc"><?php echo wp_kses_post($desc); ?></p>
                    <?php endif; ?>
                    <div class="how-to-intro__buttons">
                        <?php if ($btn) : ?>
                            <a class="btn" href="<?php echo esc_url( $btn['url'] ); ?>" target="<?php echo esc_attr( $btn['target'] ); ?>"><?php echo esc_html( $btn['title'] ); ?></a>
                        <?php endif; ?>
                        <a class="btn-video" href="#how-to-video">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="15" viewBox="0 0 20 15" fill="none">
                            <path d="M19.4951 2.69053C19.2661 1.82829 18.5914 1.14921 17.7347 0.91876C16.182 0.5 9.95557 0.5 9.95557 0.5C9.95557 0.5 3.72921 0.5 2.17642 0.91876C1.31976 1.14925 0.645057 1.82829 0.416062 2.69053C0 4.25339 0 7.51415 0 7.51415C0 7.51415 0 10.7749 0.416062 12.3378C0.645057 13.2 1.31976 13.8508 2.17642 14.0812C3.72921 14.5 9.95557 14.5 9.95557 14.5C9.95557 14.5 16.1819 14.5 17.7347 14.0812C18.5914 13.8508 19.2661 13.2 19.4951 12.3378C19.9111 10.7749 19.9111 7.51415 19.9111 7.51415C19.9111 7.51415 19.9111 4.25339 19.4951 2.69053ZM7.91919 10.4747V4.55362L13.1232 7.51422L7.91919 10.4747Z" fill="white"/>
                            </svg>
                            <span><?php echo esc_html_e('Zobacz video', 'hello-elementor-child'); ?></span>
                        </a>
                    </div>
                    <div class="how-to-intro__select">
                        <h2 class="heading"><?php echo esc_html_e('Wybierz miasto:', 'hello-elementor-child'); ?></h2>
                        <div class="custom-select">
                            <div class="custom-select__filter js--how-to-intro-custom-select-filter">
                                <span class="filter-label"><?php echo wp_kses_post($default_city[0]->post_title); ?></span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="7" viewBox="0 0 10 7" fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.66435 0.73317C10.0771 1.07186 10.1143 1.65532 9.7474 2.03635L5.74742 6.19018C5.55765 6.38725 5.28564 6.5 5.00001 6.5C4.71437 6.5 4.44237 6.38725 4.2526 6.19018L0.252603 2.03637C-0.114316 1.65534 -0.0771369 1.07189 0.335645 0.733193C0.748427 0.394498 1.3805 0.428817 1.74742 0.809847L5 4.18751L8.25258 0.809828C8.6195 0.428798 9.25157 0.394477 9.66435 0.73317Z" fill="white"/>
                                </svg>
                            </div>
                            <div class="custom-select__dropdown js--how-to-intro-custom-select-dropdown">
                                <?php
                                    $args = array(
                                        'post_type' => 'jak-grac', // Zastąp 'nazwa_twojego_typu_niestandardowego' nazwą swojego CPT
                                        'posts_per_page' => -1 // -1 aby wyświetlić wszystkie wpisy, możesz też ustawić inna liczbę
                                    );

                                    $query = new WP_Query($args);
                                ?>
                                <?php if ($query->have_posts()) : ?>
                                    <?php while ($query->have_posts()) : $query->the_post(); 
                                    $search = array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż');
                                    $replace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'A', 'C', 'E', 'L', 'N', 'O', 'S', 'Z', 'Z');
                                    $slug = get_the_title();
                                    // $slug = str_replace($search, $replace, $slug);
                                    $slug = sanitize_title($slug);
                                    $slug = strtolower($slug);
                                    $slug = str_replace(' ', '_', $slug);
                                    ?>

                                        <div>
                                            <input type="radio" id="<?php echo wp_kses_post($slug); ?>" name="city" data-name="<?php the_title(); ?>" data-id="<?php echo wp_kses_post(get_the_ID()); ?>">
                                            <label for="<?php echo wp_kses_post($slug); ?>"><?php the_title(); ?></label>
                                            
                                        </div>

                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="how-to-intro__col how-to-intro__col--column-2">
                    <?php if ($img) : ?>
                        <div class="how-to-intro__image">
                            <?php
                            echo wp_get_attachment_image(
                                $img,
                                'how_to_intro_img',
                                false,
                                [
                                    'loading' => 'eager',
                                    'sizes'   => '(max-width: 1024px) 100vw, 1024px',
                                    'srcset'  => wp_get_attachment_image_srcset( $img, [ 550, 550, '2x' ] ),
                                ]
                            );
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</section>

<style>
    header.page-header {
        display:none;
    }
</style>