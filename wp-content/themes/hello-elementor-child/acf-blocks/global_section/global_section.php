<?php

$slug = $_GET['id'];
$post = get_page_by_path($slug, OBJECT, 'jak-grac');
$post_id = $post->ID;


$section_id = get_field( 'select_section' );

// if ( empty( $section_id ) ) {
// 	return;
// }

if ($post_id) {
	$get_post = get_post( $post_id );
} else {
	$get_post = get_post( $section_id->ID );
}

// $get_post = get_post( $post_id );
echo apply_filters( 'the_content', $get_post->post_content ); //phpcs:ignore

?>