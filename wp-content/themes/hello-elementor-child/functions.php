<?php

/** STYLES & SCRIPTS */ 
function enqueue_theme_scripts() {
    // Dodaj style.css z motywu potomnego
    wp_enqueue_style( 'theme', get_stylesheet_directory_uri() . '/assets/dist/theme.css' );

    // Dodaj theme.js z motywu potomnego
    wp_enqueue_script( 'theme', get_stylesheet_directory_uri() . '/assets/dist/theme.js', array(), '', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts' );

function enqueue_block_editor_styles() {
    wp_enqueue_style( 'gutenberg', get_stylesheet_directory_uri() . '/assets/dist/gutenberg.css' );
}

add_action( 'enqueue_block_editor_assets', 'enqueue_block_editor_styles' );

/** AJAX URL */
add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

/** HOME URL */
add_action('wp_head', 'myplugin_homeurl');

function myplugin_homeurl() {

   echo '<script type="text/javascript">
           var homeurl = "' . home_url() . '";
         </script>';
}

/** CURRENT PAGE URL */
add_action('wp_head', 'myplugin_currentpageurl');

function myplugin_currentpageurl() {

   echo '<script type="text/javascript">
           var currentpageurl = "' . get_permalink( get_the_ID() ) . '";
         </script>';
}

/** OPTIONS */ 
// acf_add_options_page(
// 	[
// 		'page_title'    => 'Jak grać?',
// 		'menu_title'    => 'Jak grać?',
// 		'menu_slug'     => 'jak-grac',
// 		'capability'    => 'edit_posts',
// 		'redirect'      => false,
// 		'icon_url'      => 'dashicons-admin-site',
// 		'position'      => '5',
// 	]
// );

/** ACF BLOCKS */ 
add_action( 'init', 'register_acf_blocks' );
function register_acf_blocks() {
    register_block_type( __DIR__ . '/acf-blocks/how_to_intro' );
    register_block_type( __DIR__ . '/acf-blocks/global_section' );
    register_block_type( __DIR__ . '/acf-blocks/how_to_steps' );
    register_block_type( __DIR__ . '/acf-blocks/how_to_faq' );
}


/** CPT */ 
function custom_post_type_jak_grac() {
    $labels = array(
        'name'               => 'Jak grać?',
        'singular_name'      => 'Jak grać?',
        'menu_name'          => 'Jak grać?',
        'name_admin_bar'     => 'Jak grać?',
        'add_new'            => 'Dodaj nową lokalizację',
        'add_new_item'       => 'Dodaj nową lokalizację',
        'new_item'           => 'Nowa lokalizacja',
        'edit_item'          => 'Edytuj',
        'view_item'          => 'Zobacz',
        'all_items'          => 'Wszystkie',
        'search_items'       => 'Szukaj ',
        'parent_item_colon'  => 'Nadrzędne:',
        'not_found'          => 'Nie znaleziono.',
        'not_found_in_trash' => 'Nie znaleziono.'
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        // 'rewrite'            => array( 'slug' => 'ksiazki' ),
        'rewrite'            => false,
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title','editor' )
    );

    register_post_type( 'jak-grac', $args );
}

add_action( 'init', 'custom_post_type_jak_grac' );

/** IMAGE SIZES */ 
add_image_size( 'how_to_intro_img', 550, 550, true );
add_image_size( 'how_to_steps_icon', 128, 128, true );
add_image_size( 'faq_remaining_img', 335, 335, true );

/** AJAX */
add_action( 'wp_ajax_nopriv_get_new_content', 'new_content' );
add_action( 'wp_ajax_get_new_content', 'new_content' );

function new_content() {
    $name = wp_unslash( $_POST['name'] ?? 'all' ); // phpcs:ignore
    $slug = wp_unslash( $_POST['slug'] ?? 'all' ); // phpcs:ignore
    $id = wp_unslash( $_POST['id'] ?? null ); // phpcs:ignore

    if ($id !== null) {
        $section_id = $id;

        // echo '<input type="hidden" value="'. get_the_title($section_id) . '" class=""></input>';

        $get_post = get_post( $section_id );
        echo apply_filters( 'the_content', $get_post->post_content ); //phpcs:ignore
    }

    die(0);
}