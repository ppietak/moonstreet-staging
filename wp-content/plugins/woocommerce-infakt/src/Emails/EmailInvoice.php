<?php

namespace WPDesk\inFakt\Emails;

use WPDesk\inFakt\Documents\DocumentType;
use \WC_Order;
use WPDesk\inFakt\Documents\Invoice;

class EmailInvoice extends EmailAbstract implements EmailTrigger {

	/**
	 * @var string
	 */
	protected $download_url;

	/**
	 * @var string
	 */
	protected $template_path;

	/**
	 * @param string $plugin_dir
	 */
	public function __construct( $plugin_dir ) {
		$this->id             = 'customer_infakt_' . Invoice::TYPE;
		$this->title          = __( '[inFakt] Faktura VAT', 'woocommerce-infakt' );
		$this->description    = __( 'E-mail zawierający wygenerowaną fakturę z systemu inFakt', 'woocommerce-infakt' );
		$this->subject        = __( 'Faktura VAT dla zamówienia #{order-number} z dnia {order-date}', 'woocommerce-infakt' );
		$this->heading        = __( 'Faktura VAT numer {invoice-number}', 'woocommerce-infakt' );
		$this->template_html  = 'emails/customer-infakt-invoice.php';
		$this->template_plain = 'emails/plain/customer-infakt-invoice.php';

		parent::__construct();

		$this->template_base  = trailingslashit( $plugin_dir ) . 'templates/';
		$this->template_path  = trailingslashit( $plugin_dir ) . 'templates/';
		$this->customer_email = true;
		$this->manual         = true;
		$this->enabled        = 'yes';
	}


}

