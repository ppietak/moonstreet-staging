<?php

namespace WPDesk\inFakt\Emails;

use WC_Email;
use WC_Order;
use WPDesk\inFakt\Documents\DocumentType;

abstract class EmailAbstract extends WC_Email {

	public function get_attachments() {
		return apply_filters( 'infakt_email_attachments', array(), $this->id, $this->object, $this );
	}

	/**
	 * Tries to send email concerning given order
	 *
	 * @param WC_Order     $order
	 * @param DocumentType $document
	 */
	public function trigger( WC_Order $order, DocumentType $document ) {
		$this->object                           = wc_get_order( $order );
		$this->download_url                     = $document->get_url();
		$this->recipient                        = $this->object->get_billing_email();
		$this->placeholders['{order-number}']   = $this->object->get_id();
		$this->placeholders['{order-date}']     = date_i18n( wc_date_format(), $this->object->get_date_created()->getTimestamp() );
		$this->placeholders['{invoice-number}'] = $document->get_number();

		if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
			return;
		}

		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
	}

	public function init_form_fields() {
		$this->form_fields = [
			'subject'    => [
				'title'       => __( 'Email Subject', 'woocommerce' ),
				'type'        => 'text',
				'description' => sprintf( __( 'Defaults to <code>%s</code>', 'woocommerce' ), $this->subject ),
				'placeholder' => '',
				'default'     => '',
				'desc_tip'    => false,
			],
			'heading'    => [
				'title'       => __( 'Email Heading', 'woocommerce' ),
				'type'        => 'text',
				'description' => sprintf( __( 'Defaults to <code>%s</code>', 'woocommerce' ), $this->heading ),
				'placeholder' => '',
				'default'     => '',
				'desc_tip'    => false,
			],
			'email_type' => [
				'title'       => __( 'Email type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'     => 'html',
				'class'       => 'email_type wc-enhanced-select',
				'options'     => $this->get_email_type_options(),
				'desc_tip'    => true,
			],
		];
	}

	/**
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html(
			$this->template_html,
			[
				'order'              => $this->object,
				'download_url'       => $this->download_url,
				'email_heading'      => $this->get_heading(),
				'sent_to_admin'      => false,
				'plain_text'         => false,
				'email'              => $this,
				'additional_content' => '',
			],
			'',
			$this->template_base
		);
	}

	/**
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html(
			$this->template_plain,
			[
				'order'              => $this->object,
				'download_url'       => $this->download_url,
				'email_heading'      => $this->get_heading(),
				'sent_to_admin'      => false,
				'plain_text'         => false,
				'email'              => $this,
				'additional_content' => '',
			],
			'',
			$this->template_base
		);
	}

	/**
	 * Remove handed files
	 *
	 * @param  $list_filenames string[] Attached filenames
	 *
	 * @return void
	 */
	private function remove_files( array $list_filenames ) {
		foreach ( $list_filenames as $file ) {
			if ( is_string( $file ) && file_exists( $file ) ) {
				unlink( $file );
			}
		}
	}
}
