<?php

namespace WPDesk\inFakt\Emails;

use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\inFakt\Plugin;

/**
 * Register document emails.
 */
class RegisterEmails implements Hookable {

	const EMAIL_INVOICE_SLUG  = 'WC_Email_Customer_InFakt_Invoice';
	const EMAIL_PROFORMA_SLUG = 'WC_Email_Customer_InFakt_Proform';

	/**
	 * @var Plugin
	 */
	private $plugin;

	/**
	 * @param Plugin $plugin
	 */
	public function __construct( Plugin $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * Fire hooks.
	 */
	public function hooks() {
		add_filter( 'woocommerce_email_classes', [ $this, 'register_emails' ], 999 );
	}

	/**
	 * @param array $emails Emails.
	 *
	 * @return array
	 */
	public function register_emails( array $emails ): array {
		$emails[ self::EMAIL_INVOICE_SLUG ]  = new EmailInvoice( $this->plugin->get_plugin_dir() );
		$emails[ self::EMAIL_PROFORMA_SLUG ] = new EmailProforma( $this->plugin->get_plugin_dir() );

		return $emails;
	}

}
