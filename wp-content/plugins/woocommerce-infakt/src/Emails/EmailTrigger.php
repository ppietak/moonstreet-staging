<?php

namespace WPDesk\inFakt\Emails;

use WPDesk\inFakt\Documents\DocumentType;

interface EmailTrigger {

	public function trigger( \WC_Order $order, DocumentType $document_type );

}
