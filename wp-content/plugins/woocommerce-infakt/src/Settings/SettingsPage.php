<?php

namespace WPDesk\inFakt\Settings;

use Psr\Log\LoggerInterface;
use WInfaktVendor\WPDesk\Persistence\Adapter\WordPress\WordpressTransientContainer;
use WPDesk\inFakt\Api\Api;
use WPDesk\inFakt\Helpers\DocumentHelper;

class SettingsPage extends \WC_Integration {

	const OPTION_API_KEY                   = 'api_key';
	const OPTION_SELLER_SIGNATURE          = 'seller_signature';
	const OPTION_BACS_ACCOUNT              = 'bacs_account';
	const OPTION_ONLY_COMPANY              = 'only_company';
	const OPTION_VALIDATE_VAT_NUMBER       = 'validate_vat_number';
	const OPTION_CLIENT_ADDRESS2           = 'client_address2';
	const OPTION_VAT_EXEMPTION_REASON      = 'vat_exemption_reason';
	const OPTION_INVOICE_GENERATE_MODE     = 'invoice_generate_mode';
	const OPTION_INVOICE_GENERATE_STATUS   = 'invoice_generate_status';
	const OPTION_INVOICE_AUTO_SEND         = 'invoice_auto_send';
	const OPTION_INVOICE_ZW_RATE           = 'invoice_rate_zw';
	const OPTION_INVOICE_0_RATE            = 'invoice_rate_zero';
	const OPTION_INVOICE_PAYMENT_DATE      = 'invoice_payment_date';
	const OPTION_INVOICE_DOCUMENT_MARKINGS = 'invoice_document_markings';
	const OPTION_PROFORMA_GENERATE_STATUS  = 'proforma_generate_status';
	const OPTION_PROFORMA_AUTO_SEND        = 'proforma_auto_send';
	const OPTION_LUMP_SUM_ENABLED          = 'lump_sum_enabled';
	const OPTION_LUMP_SUM_ATTRIBUTE        = 'lump_sum_attribute';
	const OPTION_LUMP_SUM_DEFAULT          = 'lump_sum_default';
	const OPTION_DEBUG_MODE                = 'debug_mode';
	const OPTION_DEBUG_MODE_WITH_EXIT      = 'debug_mode_with_exit';
	const OPTION_DEBUG_MODE_AUTO_DISABLE   = 'debug_mode_auto_disable';

	/**
	 * @var Api
	 */
	public static $api;

	/**
	 * @var LoggerInterface
	 */
	public static $logger;

	public function __construct() {
		$this->id                 = 'integration-infakt';
		$this->method_title       = __( 'inFakt', 'woocommerce-infakt' );
		$this->method_description = __( 'Wystawianie faktur w serwisie infakt.pl. <a href="https://www.wpdesk.pl/docs/infakt-woocommerce-docs/" target="_blank">Instrukcja konfiguracji &rarr;</a>' );


		$this->init_form_fields();
		$this->init_settings();

		add_action( 'woocommerce_update_options_integration_' . $this->id, [ $this, 'process_admin_options' ] );
	}

	public function process_admin_options() {
		parent::process_admin_options();

		if ( isset( $_REQUEST['woocommerce_integration-infakt_debug_mode'] ) ) {
			add_option( 'infakt_debug_timestamp', time() );
		} else {
			delete_option( 'infakt_debug_timestamp' );
		}
	}

	public function init_settings() {
		parent::init_settings();
		if ( isset( $_POST['section'] ) && $_POST['section'] === 'integration-infakt' ) {
			$cache = new WordpressTransientContainer( Api::CACHE_TRANSIENT_NAMESPACE, WEEK_IN_SECONDS );
			$cache->delete( Api::CACHE_BANK_ACCOUNTS );
			$cache->delete( Api::CACHE_VAT_EXEMPTIONS );
		}
	}

	/**
	 * @param Api $api
	 */
	public static function set_api( Api $api ) {
		static::$api = $api;
	}

	/**
	 * @return Api
	 */
	private function get_api() {
		return static::$api;
	}

	public static function set_logger( LoggerInterface $logger ) {
		static::$logger = $logger;
	}

	/**
	 * @return LoggerInterface
	 */
	private function get_logger() {
		return static::$logger;
	}

	/**
	 * @return array
	 */
	private function get_options_flat_rates(): array {
		$markings_option = [
			'' => 'Brak',
		];
		$markings        = $this->get_api()->get_flat_rates();
		foreach ( $markings as $marking ) {
			if ( (int) $marking['rate'] === 2 ) {
				continue;
			}
			$markings_option[ $marking['rate'] ] = $marking['name'];
		}

		return $markings_option;
	}

	/**
	 * @return array
	 */
	private function get_document_options_markings(): array {
		$markings_option = [
			'' => 'Brak',
		];
		$markings        = $this->get_api()->get_documents_markings();
		foreach ( $markings as $marking ) {
			$name                              = $marking['code'] . ' ' . $marking['name'];
			$markings_option[ $marking['id'] ] = $name;
		}

		return $markings_option;
	}

	/**
	 * @return array
	 */
	private function get_order_statuses(): array {
		$order_status = [];
		$statuses     = wc_get_order_statuses();
		foreach ( $statuses as $slug => $name ) {
			$slug                  = str_replace( 'wc-', '', $slug );
			$order_status[ $slug ] = $name;
		}

		return $order_status;
	}

	public function init_form_fields() {
		if ( is_admin() && isset( $_GET['page'] ) && isset( $_GET['tab'] ) && $_GET['page'] === 'wc-settings' && $_GET['tab'] === 'integration' ) {
			$this->form_fields = [
				[
					'title'   => '',
					'type'    => 'submenu',
					'options' => [
						'auth-tab'     => 'Autoryzacja',
						'invoices-tab' => 'Wystawianie faktur',
						'checkout-tab' => 'Formularz zamówienia',
						'debug-tab'    => 'Tryb debugowania',
					],
				],
				'table-auth-table'                     => [
					'type'  => 'tab',
					'class' => 'table-auth-tab',
				],
				self::OPTION_API_KEY                   => [
					'title'       => __( 'Klucz API', 'woocommerce-infakt' ),
					'type'        => 'text',
					'description' => sprintf( __( 'Klucz API można wygenerować w %sustawieniach InFaktu%s', 'woocommerce-infakt' ), '<a href="https://www.infakt.pl/app/ustawienia/inne_opcje/api" target="_blank">', '</a>' ),
					'class'       => 'auth-tab',
				],
				[
					'title' => __( 'Status połączenia z inFakt', 'woocommerce-infakt' ),
					'type'  => 'connection_status',
					'class' => 'auth-tab',
				],
				'table-invoices-table'                 => [
					'type'  => 'tab',
					'class' => 'table-invoices-tab',
				],
				[
					'title' => __( 'Faktury', 'woocommerce-infakt' ),
					'type'  => 'title',
					'class' => 'invoices-tab',
				],
				self::OPTION_INVOICE_GENERATE_MODE     => [
					'title'       => __( 'Wystawianie faktur', 'woocommerce-infakt' ),
					'description' => __( 'Sposób wystawiania faktur', 'woocommerce-infakt' ),
					'type'        => 'select',
					'options'     => [
						'manual'     => 'Ręcznie',
						'manual_ask' => 'Pytaj kupującego i nie wystawiaj automatycznie',
						'auto_ask'   => 'Pytaj kupującego i wystawiaj automatycznie',
						'auto'       => 'Zawsze (automatycznie)',
					],
					'class'       => 'invoices-tab',
				],
				self::OPTION_INVOICE_GENERATE_STATUS   => [
					'title'             => __( 'Status zamówienia', 'woocommerce-infakt' ),
					'type'              => 'multiselect',
					'options'           => $this->get_order_statuses(),
					'custom_attributes' => [
						'multiple' => 'multiple',
					],
					'class'             => 'woo-select2-multiple invoices-tab',
					'description'       => __( 'Wybierz status zamówienia dla którego faktura będzie wystawiana automatycznie.', 'woocommerce-infakt' ),
				],
				self::OPTION_INVOICE_AUTO_SEND         => [
					'title' => __( 'Automatycznie wysyłaj faktury po wystawieniu', 'woocommerce-infakt' ),
					'type'  => 'checkbox',
					'class' => 'invoices-tab',
				],
				self::OPTION_INVOICE_ZW_RATE           => [
					'title'       => __( 'Stawka podatkowa ZW', 'woocommerce-infakt' ),
					'type'        => 'select',
					'options'     => wc_get_product_tax_class_options(),
					'class'       => 'invoices-tab',
					'description' => __( 'Wybierz klasę podatkową dla produktów zwolnionych z opodatkowania.', 'woocommerce-infakt' ),
				],
				self::OPTION_INVOICE_0_RATE            => [
					'title'       => __( 'Stawka podatkowa 0', 'woocommerce-infakt' ),
					'type'        => 'select',
					'options'     => wc_get_product_tax_class_options(),
					'class'       => 'invoices-tab',
					'description' => __( 'Wybierz klasę podatkową dla produktów z zerowym VAT.', 'woocommerce-infakt' ),
				],
				self::OPTION_INVOICE_PAYMENT_DATE      => [
					'title'             => __( 'Termin płatności faktury', 'woocommerce-infakt' ),
					'type'              => 'number',
					'default'           => 7,
					'description'       => __( 'Ustaw domyślną ilość dni na zapłatę faktury dla zamówień opłacanych za pobraniem', 'woocommerce-infakt' ),
					'custom_attributes' => [
						'min' => 0,
						'max' => 60,
					],
					'class'             => 'invoices-tab',
				],
				self::OPTION_INVOICE_DOCUMENT_MARKINGS => [
					'title'       => __( 'Oznaczenie transakcji przychodowej', 'woocommerce-infakt' ),
					'description' => __( 'Wybierz oznaczenie dla transakcji przychodowej.', 'woocommerce-infakt' ),
					'type'        => 'select',
					'options'     => $this->get_document_options_markings(),
					'class'       => 'invoices-tab',
				],
				[
					'title' => __( 'Faktury Proformy', 'woocommerce-infakt' ),
					'type'  => 'title',
					'class' => 'invoices-tab',
				],
				self::OPTION_PROFORMA_GENERATE_STATUS  => [
					'title'             => __( 'Status zamówienia dla proform', 'woocommerce-infakt' ),
					'type'              => 'multiselect',
					'options'           => $this->get_order_statuses(),
					'custom_attributes' => [
						'multiple' => 'multiple',
					],
					'class'             => 'woo-select2-multiple invoices-tab',
					'description'       => __( 'Wybierz status zamówienia dla którego faktura będzie wystawiana automatycznie.', 'woocommerce-infakt' ),
				],
				self::OPTION_PROFORMA_AUTO_SEND        => [
					'title' => __( 'Automatycznie wysyłaj faktury proformy po wystawieniu', 'woocommerce-infakt' ),
					'type'  => 'checkbox',
					'class' => 'invoices-tab',
				],
				self::OPTION_SELLER_SIGNATURE          => [
					'title' => __( 'Podpis Sprzedawcy na Dokumentach', 'woocommerce-infakt' ),
					'type'  => 'text',
					'class' => 'invoices-tab',
				],
				self::OPTION_BACS_ACCOUNT              => [
					'title'       => __( 'Konto bankowe do przelewów', 'woocommerce-infakt' ),
					'type'        => 'select',
					'options'     => $this->bacs_accounts(),
					'description' => __( 'Konto bankowe będzie widniało na fakturach dla zamówień opłacanych przelewem bankowym', 'woocommerce-infakt' ),
					'class'       => 'invoices-tab',
				],
				self::OPTION_VAT_EXEMPTION_REASON      => [
					'title'       => __( 'Podstawa prawna zwolnienia', 'woocommerce-infakt' ),
					'description' => __( 'Wybierz podstawę zwolnienia z VAT jeżeli masz wyłączone podatki w sklepie i w koncie inFaktu.', 'woocommerce-infakt' ),
					'type'        => 'select',
					'options'     => $this->get_api()->get_vat_exemptions(),
					'class'       => 'invoices-tab',
				],
				[
					'title' => __( 'Ryczałt', 'wooifirma' ),
					'type'  => 'title',
					'class' => 'invoices-tab',
				],
				self::OPTION_LUMP_SUM_ENABLED          => [
					'title'    => '',
					'type'     => 'checkbox',
					'desc_tip' => false,
					'label'    => __( 'Jestem ryczałtowcem', 'wooifirma' ),
					'default'  => 'no',
					'class'    => 'invoices-tab',
				],
				self::OPTION_LUMP_SUM_ATTRIBUTE        => [
					'title'       => __( 'Atrybut produktu', 'wooifirma' ),
					'type'        => 'text',
					'default'     => 'ryczalt',
					'description' => __(
						'Atrybut produktu, z którego będzie pobierana stawka ryczałtu na fakturę. Jeżeli produkt nie będzie miał tego atrybutu, to zostanie ustawiona stawka domyślna.
					Dopuszczalne wartości atrybutu to: 3, 5,5, 8,5, 10, 12, 12,5, 14, 15, 17 i 20.',
						'wooifirma'
					),
					'class'       => 'infakt-lump-sum invoices-tab',
				],
				self::OPTION_LUMP_SUM_DEFAULT          => [
					'title'       => __( 'Stawka domyślna', 'wooifirma' ),
					'type'        => 'select',
					'default'     => 'ryczalt',
					'description' => __( 'Domyślna stawka ryczałtu.', 'wooifirma' ),
					'options'     => $this->get_options_flat_rates(),
					'class'       => 'infakt-lump-sum invoices-tab',
				],
				'table-checkout-table'                 => [
					'type'  => 'tab',
					'class' => 'table-checkout-tab',
				],
				self::OPTION_VALIDATE_VAT_NUMBER       => [
					'title'       => __( 'Włącz walidację numeru NIP', 'woocommerce-infakt' ),
					'label'       => __( 'Włącz', 'woocommerce-infakt' ),
					'description' => __( 'Zaznacz tę opcję żeby włączyć walidację numeru NIP i numerów VAT krajów EU.', 'woocommerce-infakt' ),
					'type'        => 'checkbox',
					'class'       => 'checkout-tab',
				],
				self::OPTION_ONLY_COMPANY              => [
					'title'       => __( 'Pole NIP jest wymagane', 'woocommerce-infakt' ),
					'label'       => __( 'Włącz', 'woocommerce-infakt' ),
					'description' => __( 'Zaznacz tę opcję, aby pole NIP było wymagane (zamówienia B2B).', 'woocommerce-infakt' ),
					'type'        => 'checkbox',
					'class'       => 'checkout-tab',
				],
				self::OPTION_CLIENT_ADDRESS2           => [
					'title'       => __( 'Wydziel nr budynku/lokalu do osobnego pola', 'woocommerce-infakt' ),
					'label'       => __( 'Włącz', 'woocommerce-infakt' ),
					'description' => __( 'UWAGA! Włączenie tej opcji spowoduje dodanie pól Nr budynku/Nr lokalu po polu Ulica. Dodatkowo domyślne pole druga linia adresu nie będzie dostępne w formularzu. Włączenie tej opcji nie jest obowiązkowe. Dowiedz się więcej na temat zmian <a href="#" target="_blank">tutaj</a>.', 'woocommerce-infakt' ),
					'type'        => 'checkbox',
					'class'       => 'checkout-tab',
				],
				'table-debug-table'                    => [
					'type'  => 'tab',
					'class' => 'table-debug-tab',
				],
				self::OPTION_DEBUG_MODE                => [
					'title'       => __( 'Tryb debugowania', 'woocommerce-infakt' ),
					'label'       => __( 'Włącz tryb debugowania', 'woocommerce-infakt' ),
					'type'        => 'checkbox',
					'description' => sprintf( __( 'Włączenie spowoduje utworzenie pliku infakt.log. Zapis logów można sprawdzić <a href="%s">tutaj &rarr;</a><br/><a href="https://wpde.sk/infakt-debugowanie" target="_blank">Przeczytaj jak przeprowadzić proces debugowania &rarr;</a>', 'woocommerce-infakt' ), admin_url( 'admin.php?page=wc-status&tab=logs' ) ),
					'class'       => 'debug-tab',
				],
				self::OPTION_DEBUG_MODE_WITH_EXIT      => [
					'title'       => __( 'Zablokuj żądanie API', 'woocommerce-infakt' ),
					'label'       => __( 'Włącz', 'woocommerce-infakt' ),
					'type'        => 'checkbox',
					'description' => __( 'Zablokuj tworzenie faktury, gdy włączony jest tryb debugowania. Ustawienie to będzie włączone jedynie dla konta administratora.', 'woocommerce-infakt' ),
					'class'       => 'infakt-debug-mode debug-tab',
				],
				self::OPTION_DEBUG_MODE_AUTO_DISABLE   => [
					'title'       => __( 'Automatyczne wyłaczenie', 'woocommerce-infakt' ),
					'options'     => [
						0       => __( 'Wyłączone', 'woocommerce-infakt' ),
						3600    => __( '1 godzina', 'woocommerce-infakt' ),
						14400   => __( '4 godziny', 'woocommerce-infakt' ),
						86400   => __( 'Po 1 dniu', 'woocommerce-infakt' ),
						604800  => __( 'Po 7 dniach', 'woocommerce-infakt' ),
						2592000 => __( 'Po 30 dniach', 'woocommerce-infakt' ),
					],
					'type'        => 'select',
					'description' => __( 'Wybierz po jakim czasie logowanie ma zostać wyłączone automatycznie.', 'woocommerce-infakt' ),
					'class'       => 'infakt-debug-mode debug-tab',
				],
			];
		}
	}

	public function validate_api_key_field( $key, $value ) {
		if ( ! empty( $value ) && ! preg_match( '/^[0-9a-fA-F]{40}$/', $value ) ) {
			\WC_Admin_Settings::add_error( __( 'Klucz API jest niepoprawny.', 'woocommerce-infakt' ) );
		}

		return $value;
	}

	/**
	 * Callable validation method. Do not remove!
	 *
	 * @param $key
	 * @param $value
	 *
	 * @return int|mixed
	 */
	public function validate_invoice_payment_date_field( $key, $value ) {
		if ( empty( $value ) ) {
			$value = 0;
		}

		if ( ! is_numeric( $value ) || $value < 0 || $value > 60 ) {
			\WC_Admin_Settings::add_error( __( 'Termin płatności faktury jest niepoprawny', 'woocommerce-infakt' ) );
		}

		return $value;
	}

	/**
	 * Callable menu method. Do not remove!
	 *
	 * @param string $key
	 * @param array  $data
	 *
	 * @return string
	 */
	public function generate_submenu_html( $key, $data ): string {
		$output = '</table>' . PHP_EOL;
		$output .= '<div class="nav-tab-wrapper infakt-js-nav-tab-wrapper">' . PHP_EOL;
		foreach ( $data['options'] as $sub => $title ) {
			$output .= '<a class="nav-tab nav-tab-' . $sub . ' nav-tab-active" href="#' . $sub . '">' . $title . '</a>';
		}
		$output .= '</div>' . PHP_EOL;
		$output .= '<table class="form-table" id="infakt-settings-table">' . PHP_EOL;

		return $output;
	}


	/**
	 * Callable menu method. Do not remove!
	 *
	 * @param string $key
	 * @param array  $data
	 *
	 * @return string
	 */
	public function generate_tab_html( $key, $data ): string {
		$output = '</table>' . PHP_EOL;
		$output .= '<table class="infakt-form-table form-table ' . esc_attr( $data['class'] ) . '" id="infakt-' . esc_attr( $key ) . '">' . PHP_EOL;

		return $output;
	}

	/**
	 * Callable connection status method. Do not remove!
	 *
	 * @param string $key
	 * @param mixed  $data
	 *
	 * @return false|string
	 */
	public function generate_connection_status_html( $key, $data ) {
		$field_key    = $this->get_field_key( $key );
		$is_connected = $this->get_api()->is_connected();
		$status       = $this->connection_status();
		ob_start(); ?>
		<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="<?php echo esc_attr( $field_key ); ?>">
				<?php echo wp_kses_post( $data['title'] ); ?>
			</label>
		</th>
		<td class="forminp">
			<?php printf( '<span class="%s">%s</span>', $is_connected ? 'success ' . $data['class'] : 'error ' . $data['class'], $is_connected ? 'OK' : $status ); ?>
		</td>
		</tr><?php

		return ob_get_clean();
	}

	/**
	 * Generate Title HTML.
	 *
	 * @param string $key  Field key.
	 * @param array  $data Field data.
	 *
	 * @return string
	 * @since  1.0.0
	 */
	public function generate_title_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = [
			'title' => '',
			'class' => '',
		];

		$data = wp_parse_args( $data, $defaults );
		ob_start();
		?>
		<tr>
			<th class="titledesc infakt-settings-form-title">
				<h3 class="wc-settings-sub-title <?php echo esc_attr( $data['class'] ); ?>" id="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></h3>
				<?php if ( ! empty( $data['description'] ) ) : ?>
					<p><?php echo wp_kses_post( $data['description'] ); ?></p>
				<?php endif; ?>
			</th>
			<td></td>
		</tr>
		<?php

		return ob_get_clean();
	}

	/**
	 * @return array
	 */
	private function bacs_accounts(): array {
		$list = [
			'' => __( 'Wybierz konto bankowe', 'woocommerce-infakt' ),
		];

		$ifirma_bacs_accounts = $this->get_api()->get_bank_account();

		$accounts             = $ifirma_bacs_accounts;
		$woocommerce_accounts = get_option( 'woocommerce_bacs_accounts' );
		if ( ! empty( $woocommerce_accounts ) ) {
			$accounts = array_merge( $accounts, $woocommerce_accounts );
		}
		if ( $accounts ) {
			foreach ( $accounts as $account ) {
				$list[] = sprintf( '%s (%s: %s)', $account['account_name'], $account['bank_name'], $account['account_number'] );
			}
		}

		return $list;
	}

	/**
	 * @return string
	 */
	private function connection_status(): string {
		$api_key = DocumentHelper::get_infakt_option( 'api_key', false );


		if ( empty( $api_key ) ) {
			return 'Brak klucza API.';
		}

		if ( ! preg_match( '/^[0-9a-fA-F]{40}$/', $api_key ) ) {
			return 'Błędny klucz API. Klucz powinien składać się z 40 znaków (litery A-F i cyfry 0-9)';
		}

		return 'Brak połączenia';
	}


}
