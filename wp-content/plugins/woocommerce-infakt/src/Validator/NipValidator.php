<?php
/**
 * Validators: NIP validator.
 *
 * @package WooCommerceWFirma
 */

namespace WPDesk\inFakt\Validator;

use WP_Error;

/**
 * @package WPDesk\WooCommerceWFirma\Field\Validator
 */
class NipValidator {
	const INVALID_NIP_ERROR_CODE       = 100;
	const VALID_NIP_LENGTH             = 10;
	const VALID_NIP_LENGTH_WITH_PREFIX = 12;

	/** @var array[] */
	private $errorMessages = [];
	/**
	 * @var mixed
	 */
	private $field_id;

	public function __construct( $field_id ) {
		$this->field_id = $field_id;
	}

	/**
	 * Is valid when converted to number.
	 *
	 * @param string|null $number
	 *
	 * @return bool
	 */
	private function is_valid_nip_number( $number ) {

		if ( ! $number ) {
			return false;
		}

		$number = preg_replace( '/[^0-9]+/', '', $number );

		if ( $number && strlen( $number ) !== self::VALID_NIP_LENGTH ) {
			return false;
		}

		$arrSteps = [ 6, 5, 7, 2, 3, 4, 5, 6, 7 ];
		$intSum   = 0;

		for ( $i = 0; $i < 9; $i ++ ) {
			$intSum += $arrSteps[ $i ] * $number[ $i ];
		}

		$int          = $intSum % 11;
		$intControlNr = 10 === $int ? 0 : $int;

		return $intControlNr === (int) $number[9];
	}

	/**
	 * Is prefix and length valid
	 *
	 * @param string|null $value
	 *
	 * @return bool
	 */
	private function is_prefix_and_length_valid( $value ) {
		if ( ! $value ) {
			return false;
		}
		$length = strlen( $value );
		if ( self::VALID_NIP_LENGTH_WITH_PREFIX === $length ) {
			return 'PL' === strtoupper( substr( $value, 0, 2 ) );
		}

		return self::VALID_NIP_LENGTH === $length;
	}

	/**
	 * Checks if message is valid and produces $errorMessages.
	 *
	 * @param string $value
	 *
	 * @return bool
	 */
	private function is_valid( $value ) {
		if ( '' === $value ) {
			return true;
		}

		$value = preg_replace( '/(-([0-9]))+/', '$2', $value );

		return $this->is_valid_nip_number( $value ) || ! $this->is_prefix_and_length_valid( $value );
	}

	/**
	 * Hook to checkout to ensure valid NIP.
	 */
	public function hooks() {
		add_action( 'woocommerce_after_checkout_validation', function ( array $data, WP_Error $errors ) {
			$is_valid = $this->is_valid( $data[ $this->field_id ] );
			if ( ! $is_valid ) {
				$errors->add( self::INVALID_NIP_ERROR_CODE, esc_html__( 'Podany numer NIP jest nieprawidłowy', 'woocommerce-wfirma' ) );
			}
		}, 10, 2 );
	}
}
