<?php

namespace WPDesk\inFakt;

use Psr\Log\LoggerInterface;
use WC_Order;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\inFakt\Api\Api;
use WPDesk\inFakt\Documents\DocumentFactory;
use WPDesk\inFakt\Emails\EmailTrigger;
use WPDesk\inFakt\Helpers\DocumentHelper;
use WPDesk\inFakt\Settings\SettingsPage;
use WPDesk\inFakt\Validator\EuVatValidator;
use WPDesk\inFakt\Validator\NipValidator;
use WPDesk\inFakt\WooCommerce\ValidateVatNumber;

/**
 * Invoice Integration.
 */
class Integration implements Hookable {

	const EMAIL_PREFIX      = 'infakt_email_';
	const COMPANY_NIP_FIELD = 'billing_company_nip';

	/**
	 * @var Api
	 */
	private $api;

	/**
	 * @var DocumentFactory
	 */
	private $documents;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function __construct( Api $api, DocumentFactory $documents, LoggerInterface $logger ) {
		$this->api       = $api;
		$this->documents = $documents;
		$this->logger    = $logger;
	}

	/**
	 * Init hooks
	 */
	public function hooks() {
		add_filter( 'infakt_email_attachments', [ $this, 'email_attachments' ], 10, 3 );
		add_action( 'woocommerce_after_checkout_validation', [ $this, 'after_checkout_validation' ], 10 );
		add_action( 'wp_ajax_infakt_document', [ $this, 'pdf_document' ], 10 );
		add_action( 'wp_ajax_nopriv_infakt_document', [ $this, 'pdf_document' ], 10 );
		add_filter( 'woocommerce_order_actions', [ $this, 'woocommerce_order_actions' ], 11 );
		$this->order_actions_start();


		if ( DocumentHelper::get_infakt_option( SettingsPage::OPTION_VALIDATE_VAT_NUMBER ) === 'yes' ) {
			( new EuVatValidator( self::COMPANY_NIP_FIELD ) )->hooks();
			( new NipValidator( self::COMPANY_NIP_FIELD ) )->hooks();
		}
	}


	/**
	 * Add new actions on the order page
	 *
	 * @param array $actions actions.
	 *
	 * @return array
	 */
	public function woocommerce_order_actions( array $actions ): array {
		global $theorder;
		$order     = $theorder;
		$documents = $this->documents->get_documents( $order->get_id() );
		foreach ( $documents as $document ) {
			if ( $document->is_generated() ) {
				$email_action             = self::EMAIL_PREFIX . $document->get_type();
				$actions[ $email_action ] = sprintf( __( 'inFakt: Wyślij: %s', 'woocommerce-infakt' ), $document->get_formatted_name() );
			}
		}

		return $actions;
	}

	/**
	 * Fire order hook action for document types.
	 */
	public function order_actions_start() {
		if ( ! empty( $_POST['wc_order_action'] ) ) {
			$types = $this->documents->get_types();

			foreach ( $types as $type => $type_label ) {
				$email_action    = self::EMAIL_PREFIX . $type;
				$wc_order_action = sanitize_key( $_POST['wc_order_action'] );
				if ( $email_action === $wc_order_action ) {
					add_action( 'woocommerce_order_action_' . $email_action, function ( WC_Order $order ) use ( $type ) {
						$this->send_invoice( $order, $type );
					} );
				}
			}
		}

	}

	/**
	 * @param WC_Order $order order.
	 */
	private function send_invoice( WC_Order $order, $type ) {
		$mailer    = WC()->mailer();
		$mails     = $mailer->get_emails();
		$documents = $this->documents->get_documents( $order->get_id() );
		if ( ! empty( $mails ) ) {
			foreach ( $documents as $document ) {
				if ( $document->get_type() === $type ) {
					foreach ( $mails as $mail ) {
						if ( $mail->id !== 'customer_infakt_' . $document->get_type() ) {
							continue;
						}

						if ( $mail instanceof EmailTrigger ) {
							try {
								$mail->trigger( $order, $document );
								$order->add_order_note( __( 'Order details manually sent to customer.', 'woocommerce' ) );
							} catch ( \Exception $e ) {
								$order->add_order_note( sprintf( __( 'Błąd podczas wysyłania wiadomości zawierającej dokument: %s' ), $e->getMessage() ) );
							}
						}
					}

				}
			}
		}
	}

	/**
	 * @param $post_data
	 */
	public function after_checkout_validation( $post_data ) {
		$country     = $post_data['billing_country'] ?? '';
		$company     = $post_data['billing_company'] ?? '';
		$company_nip = $post_data['billing_company_nip'] ?? '';

		if ( ! empty( $company ) && empty( $company_nip ) ) {
			wc_add_notice( __( 'Enter VAT Number', 'woocommerce-infakt' ), 'error' );
		}

		if ( ! empty( $company_nip ) && empty( $company ) ) {
			wc_add_notice( __( 'Enter Company Name', 'woocommerce-infakt' ), 'error' );
		}

		if ( ! empty( $company_nip ) ) {
			( new ValidateVatNumber() )->validate( $company_nip, $country );
		}

	}

	/**
	 * Get PDF document.
	 */
	public function pdf_document() {
		try {
			if ( ! isset( $_REQUEST['order_id'] ) ) {
				wp_die( 'Brak ID zamówienia! ' );
			}
			if ( ! isset( $_REQUEST['type'] ) ) {
				wp_die( 'Ten odnośnik już wygasł.' );
			}
			if ( ! isset( $_REQUEST['hash'] ) ) {
				wp_die( 'Brak tokenu!.' );
			}

			if ( ! is_numeric( $_REQUEST['order_id'] ) ) {
				wp_die( 'Błędny numer zamówienia.' );
			}

			$type     = $_REQUEST['type'];
			$order_id = absint( $_REQUEST['order_id'] );
			$hash     = isset( $_REQUEST['hash'] ) ? $_REQUEST['hash'] : '';

			$documents = $this->documents->get_documents( $order_id );
			if ( isset( $documents[ $type ] ) ) {
				$document = $documents[ $type ];

				if ( $hash !== $document->get_hash() ) {
					wp_die( 'Nieznany token dla pobrania dokumentu!' );
				}

				if ( ! $document->is_generated() ) {
					wp_die( 'Dla wybranego zamówienia nie została jeszcze utworzona faktura.' );
				}

				try {
					$document_id = (int) $document->get_id();
					$pdf_content = $this->api->download_pdf( $document_id );
					$document->set_status( 'printed' );
				} catch ( \Exception $e ) {
					wp_die( 'Wystąpił bląd przy pobieraniu dokumentu: ' . $e->getMessage() );
				}

				if ( empty( $pdf_content ) ) {
					wp_die( 'Dla wybranego zamówienia nie została jeszcze wygenerowana faktura.' );
				}
			}

		} catch ( \Exception $e ) {
			wp_die( $e->getMessage() );
		}

		header( 'Content-type: application/pdf' );
		header( 'Content-Disposition: attachment; filename="' . $document->get_filename() . '"' );
		echo $pdf_content;
	}

	public function email_attachments( $attachments, $email_type, $order ) {
		if ( ! in_array( $email_type, [ 'customer_infakt_invoice', 'customer_infakt_proforma' ], true ) ) {
			return $attachments;
		}

		if ( ! $order ) {
			return $attachments;
		}

		$document_type = 'invoice';
		if ( $email_type === 'customer_infakt_proforma' ) {
			$document_type = 'proforma';
		}
		$documents = $this->documents->get_documents( $order->get_id() );
		$document  = $documents[ $document_type ];

		if ( ! $document || ! $document->is_generated() ) {
			return $attachments;
		}

		$mailer_hook = null;
		$mailer_hook = function ( $phpmailer ) use ( $document, &$mailer_hook ) {
			/** @var \PHPMailer $phpmailer */
			try {
				$attachment_content = $this->api->download_pdf( (int) $document->get_id() );
				$document->set_status( 'printed' );
				if ( $attachment_content ) {
					$phpmailer->addStringAttachment(
						$attachment_content,
						$document->get_filename()
					);
				}
			} catch ( \Exception $e ) {
				$this->logger->error( __CLASS__ . ': email_attachments: ' . $e->getMessage() );
			}

			remove_action( 'phpmailer_init', $mailer_hook );
		};

		if ( is_wp_error( $document ) ) {
			return $attachments;
		}

		$document_type = $document->get_type();
		if ( 'proforma' === $document_type && 'customer_infakt_proforma' === $email_type ) {
			add_action( 'phpmailer_init', $mailer_hook );
		}

		if ( 'invoice' === $document_type && 'customer_infakt_invoice' === $email_type ) {
			add_action( 'phpmailer_init', $mailer_hook );
		}

		return [];
	}

}
