<?php

namespace WPDesk\inFakt;

use WInfaktVendor\WPDesk\Dashboard\DashboardWidget;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\AbstractPlugin;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
use WInfaktVendor\WPDesk\View\Renderer\Renderer;
use WInfaktVendor\WPDesk\View\Renderer\SimplePhpRenderer;
use WInfaktVendor\WPDesk\View\Resolver\ChainResolver;
use WInfaktVendor\WPDesk\View\Resolver\DirResolver;
use WInfaktVendor\WPDesk_Plugin_Info;
use WPDesk\inFakt\Api\Api;
use WPDesk\inFakt\Documents\DocumentFactory;
use WPDesk\inFakt\Documents\Invoice;
use WPDesk\inFakt\Documents\Proforma;
use WPDesk\inFakt\Emails\RegisterEmails;
use WPDesk\inFakt\Extensions\GoodsServicesDesignation;
use WPDesk\inFakt\Logger\LoggerIntegration;
use WPDesk\inFakt\Settings\SettingsPage;
use WPDesk\inFakt\WooCommerce\CheckoutFields;
use WPDesk\inFakt\WooCommerce\GenerateDocument;
use WPDesk\inFakt\WooCommerce\GenerateForOrderStatus;
use WPDesk\inFakt\WooCommerce\OrderPostColumns;
use WPDesk\inFakt\WooCommerce\OrderPostMetaBox;

/**
 * Main plugin class. The most important flow decisions are made here.
 */
class Plugin extends AbstractPlugin implements HookableCollection {

	use HookableParent;

	/**
	 * @var string
	 */
	public $scripts_version = '1.9';

	/**
	 * @var string
	 */
	private $plugin_path;

	/**
	 * @var string
	 */
	private $plugin_dir;

	/**
	 * @var string
	 */
	private $template_path;

	/**
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * @param WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( WPDesk_Plugin_Info $plugin_info ) {
		$this->plugin_info = $plugin_info;
		parent::__construct( $this->plugin_info );
		$this->plugin_url    = $this->plugin_info->get_plugin_url();
		$this->plugin_dir    = $this->plugin_info->get_plugin_dir();
		$this->plugin_path   = trailingslashit( $this->plugin_info->get_plugin_dir() );
		$this->template_path = trailingslashit( $this->plugin_path . 'templates' );
		$this->settings_url  = admin_url( 'admin.php?page=wc-settings&tab=integration&section=integration-infakt' );
		$this->docs_url      = 'https://www.wpdesk.pl/docs/infakt-woocommerce-docs/';
		$this->support_url   = 'https://www.wpdesk.pl/support/';
		$this->init_renderer();
	}

	public function get_plugin_dir() {
		return $this->plugin_dir;
	}

	/**
	 * Set renderer.
	 */
	private function init_renderer() {
		$resolver = new ChainResolver();
		$resolver->appendResolver( new DirResolver( $this->template_path ) );
		$this->renderer = new SimplePhpRenderer( $resolver );
	}

	/**
	 * @return DocumentFactory
	 */
	public function register_documents(): DocumentFactory {
		$documents = new DocumentFactory();
		$documents->add_document_type( Invoice::TYPE, new Invoice() );
		$documents->add_document_type( Proforma::TYPE, new Proforma() );

		return $documents;
	}

	public function hooks() {
		parent::hooks();
		$logger    = new LoggerIntegration( 'infakt', 'infakt_settings' );
		$api       = new Api( $logger );
		$documents = $this->register_documents();
		SettingsPage::set_api( $api );

		$generate = new GenerateDocument( $api, $documents, $logger->get_logger() );
		$this->add_hookable( $logger );
		$this->add_hookable( new Integration( $api, $documents, $logger->get_logger() ) );
		$this->add_hookable( new RegisterEmails( $this ) );
		$this->add_hookable( new OrderPostColumns( $documents ) );
		$this->add_hookable( new GenerateForOrderStatus( $documents, $generate, $logger->get_logger() ) );
		$this->add_hookable( new OrderPostMetaBox( $documents, $api, $this->renderer ) );
		$this->add_hookable( new CheckoutFields() );
		$this->add_hookable( new GoodsServicesDesignation() );
		$this->add_hookable( $generate );
		( new DashboardWidget() )->hooks();
		$this->hooks_on_hookable_objects();

		add_filter( 'woocommerce_integrations', [ $this, 'woocommerce_integration' ] );
	}

	/**
	 * @param array $integrations
	 *
	 * @return array
	 */
	public function woocommerce_integration( array $integrations ) {
		$integrations[] = SettingsPage::class;

		return $integrations;
	}

	/**
	 * Enqueue admin scripts
	 */
	public function admin_enqueue_scripts() {
		global $current_screen;
		$section          = $_REQUEST['section'] ?? ''; //phpcs:ignore
		$is_settings_page = $current_screen->id === 'woocommerce_page_wc-settings' && $section === 'integration-infakt';
		if ( $current_screen && ( $current_screen->id === 'shop_order' || $current_screen->id === 'woocommerce_page_wc-orders' || $is_settings_page ) ) {
			wp_enqueue_script( 'selectWoo' );
			wp_enqueue_style( 'admin-infakt', $this->get_plugin_assets_url() . 'css/admin.min.css', '', $this->scripts_version );
			wp_register_script( 'admin-invoice-infakt', $this->get_plugin_assets_url() . 'js/admin.min.js', [ 'jquery' ], $this->scripts_version, true );
			wp_enqueue_script( 'admin-invoice-infakt' );
			wp_localize_script(
				'admin-invoice-infakt',
				'_infakt_admin',
				[
					'ajax_url'              => admin_url( 'admin-ajax.php' ),
					'security'              => wp_create_nonce( '_infakt_ajax_nonce' ),
					'select2_placeholder'   => __( 'Search...', 'flexible-invoices-core' ),
					'select2_min_chars'     => __( 'Minimum length %.', 'flexible-invoices-core' ),
					'select2_loading_more'  => __( 'More...', 'flexible-invoices-core' ),
					'select2_no_results'    => __( 'No results.', 'flexible-invoices-core' ),
					'select2_searching'     => __( 'Searching...', 'flexible-invoices-core' ),
					'select2_error_loading' => __( 'Cannot load data...', 'flexible-invoices-core' ),
				]
			);
		}
	}

	public function wp_enqueue_scripts() {
		parent::wp_enqueue_scripts();
		if ( is_checkout() ) {
			wp_enqueue_script( 'infakt-checkout', $this->get_plugin_assets_url() . 'js/checkout.js', [ 'jquery' ], $this->scripts_version, true );
		}
	}

}
