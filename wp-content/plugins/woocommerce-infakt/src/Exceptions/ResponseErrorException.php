<?php

namespace WPDesk\inFakt\Exceptions;

use InvalidArgumentException;
use Throwable;

class ResponseErrorException extends InvalidArgumentException {

	/**
	 * @var []
	 */
	private $errors;

	public function __construct( $message = "", $errors = "", $code = 0, Throwable $previous = null ) {
		parent::__construct( $message, $code, $previous );
		$this->errors = $errors;
	}

	/**
	 * @return array
	 */
	public function get_error_details(): array {
		$errors = [];
		foreach ( $this->errors as $field => $error ) {
			$errors[] = $field . ': ' . $error[0];
		}

		return $errors;
	}

}
