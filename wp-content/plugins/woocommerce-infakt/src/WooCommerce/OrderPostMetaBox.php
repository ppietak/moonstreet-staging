<?php

namespace WPDesk\inFakt\WooCommerce;

use Exception;
use WC_Order;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WInfaktVendor\WPDesk\View\Renderer\Renderer;
use WP_Post;
use WPDesk\inFakt\Api\Api;
use WPDesk\inFakt\Documents\DocumentFactory;
use WPDesk\inFakt\Helpers\DocumentHelper;
use WPDesk\inFakt\Helpers\PaymentMethods;

class OrderPostMetaBox implements Hookable {

	/**
	 * @var DocumentFactory
	 */
	private $documents;

	/**
	 * @var Api
	 */
	private $api;

	/**
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * @param DocumentFactory $documents
	 * @param Api             $api
	 * @param Renderer        $renderer
	 */
	public function __construct( DocumentFactory $documents, Api $api, Renderer $renderer ) {
		$this->documents = $documents;
		$this->api       = $api;
		$this->renderer  = $renderer;
	}

	/**
	 * @return array
	 */
	private function get_document_markings(): array {
		$markings_option = [
			'' => 'Brak',
		];
		$markings        = $this->api->get_documents_markings();
		foreach ( $markings as $marking ) {
			$name                              = $marking['code'] . ' ' . $marking['name'];
			$markings_option[ $marking['id'] ] = $name;
		}

		return $markings_option;
	}

	/**
	 * Fire hooks.
	 */
	public function hooks() {
		add_action( 'add_meta_boxes', [ $this, 'add_meta_boxes' ], 11 );
	}

	/**
	 * @throws Exception
	 */
	public function get_order_for_meta_box( $object ): WC_Order {
		if ( $object instanceof WP_Post ) {
			$order = wc_get_order( $object->ID );
			if ( $order ) {
				return $order;
			}
		} elseif ( $object instanceof WC_Order ) {
			return $object;
		}

		throw new Exception( 'Unknown order object!' );
	}

	/**
	 * Add meta boxes.
	 */
	public function add_meta_boxes() {
		add_meta_box(
			'infakt',
			__( 'inFakt', 'woocommerce-infakt' ),
			[ $this, 'render' ],
			[ 'shop_order', 'woocommerce_page_wc-orders' ],
			'side',
			'high'
		);
	}

	/**
	 * @param WC_Order|WP_Post|null $order_or_post_object
	 *
	 * @return void
	 */
	public function render( $order_or_post_object ) {
		try {
			$order  = $this->get_order_for_meta_box( $order_or_post_object );
			$documents = $this->documents->get_documents( $order->get_id() );

			echo $this->renderer->render( 'admin/generated', [
				'order'           => $order,
				'generate_mode'   => DocumentHelper::get_infakt_option( 'invoice_generate_mode' ),
				'generate_status' => DocumentHelper::get_infakt_option( 'invoice_generate_status' ),
				'documents'       => $documents,
			] );

			foreach ( $documents as $document ) {
				if ( is_wp_error( $document ) ) {
					return;
				}

				if ( in_array( $order->get_payment_method(), [ 'bacs', 'cod' ] ) && ! in_array( $order->get_status(), [ 'completed', 'processing' ] ) ) {
					$total = 0.0;
				} else {
					$total = $order->get_total();
				}

				$payment_method = $this->get_payment_method( $order );

				echo $this->renderer->render( 'admin/' . $document->get_type() . '-metabox', [
					'is_company'           => $document->is_only_company(),
					'order'                => $order,
					'document'             => $document,
					'generate_mode'        => DocumentHelper::get_infakt_option( $document->get_type() . '_generate_mode' ),
					'generate_status'      => DocumentHelper::get_infakt_option( $document->get_type() . '_generate_status' ),
					'markings'             => $this->get_document_markings(),
					'total'                => $total,
					'payment_methods'      => PaymentMethods::get(),
					'order_payment_method' => $payment_method,
				] );
			}
		} catch ( Exception $e ) {
			// logger
		}
	}

	/**
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function get_payment_method( WC_Order $order ): string {
		$payment_method = $order->get_payment_method();
		$methods        = PaymentMethods::get();

		if ( $payment_method === 'bacs' ) {
			$payment_method = 'transfer';
		}

		if ( $payment_method === 'cod' ) {
			$payment_method = 'delivery';
		}

        if ( strpos( $payment_method, 'przelewy24' ) !== false ) {
            return 'przelewy24';
        }

		if ( isset( $methods[ $payment_method ] ) ) {
			return $payment_method;
		}

		return 'other';
	}
}
