<?php

namespace WPDesk\inFakt\WooCommerce;

use WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData;
use WInfaktVendor\WPDesk\Library\CheckoutFields\Integration;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\inFakt\Helpers\DocumentHelper;
use WPDesk\inFakt\Settings\SettingsPage;


class CheckoutFields implements Hookable {

	const VAT_NUMBER_NAME = 'company_nip';
	const STREET_NUMBER   = 'street_number';
	const FLAT_NUMBER     = 'flat_number';


	public function hooks() {
		$generate_invoice = DocumentHelper::get_infakt_option( 'invoice_generate_mode' );

		$only_company = DocumentHelper::get_infakt_option( SettingsPage::OPTION_ONLY_COMPANY );
		if ( in_array( $generate_invoice, [ 'manual_ask', 'auto_ask' ] ) && ! $only_company ) {
			$checkout_fields[] = ( new FieldData( 'invoice_ask', __( 'I want an invoice', 'woocommerce-infakt' ) ) )
				->set_type( 'checkbox' )
				->set_position( 'company' )
				->set_priority( 30 )
				->set_in_account( false )
				->set_show( false );
		}

		$vat_number = ( new FieldData( self::VAT_NUMBER_NAME, __( 'Vat Number', 'woocommerce-infakt' ) ) )
			->set_position( 'company' )
			->set_priority( 30 );

		if ( $only_company ) {
			$vat_number->set_required();
		}

		$checkout_fields[] = $vat_number;

		$flat_number = DocumentHelper::get_infakt_option( SettingsPage::OPTION_CLIENT_ADDRESS2 );
		if ( $flat_number ) {
			$checkout_fields[] = ( new FieldData( self::STREET_NUMBER, __( 'Street Number', 'woocommerce-infakt' ) ) )
				->set_position( 'address_1' )
				->set_priority( 60 )
				->set_required()
				->show_in_shipping( true )
				->set_show( false );
			$checkout_fields[] = ( new FieldData( self::FLAT_NUMBER, __( 'Flat number', 'woocommerce-infakt' ) ) )
				->show_in_shipping( true )
				->set_priority( 60 )
				->set_position( 'address_1' )
				->set_show( false );

			add_filter( 'woocommerce_checkout_fields', [ $this, 'remove_fields_from_checkout' ] );
			add_filter( 'woocommerce_checkout_posted_data', [ $this, 'woocommerce_checkout_posted_data_filter' ] );
			add_filter( 'woocommerce_admin_billing_fields', [ $this, 'disable_address_2_field' ], 10, 1 );
			add_filter( 'woocommerce_admin_shipping_fields', [ $this, 'disable_address_2_field' ], 10, 1 );
			add_action( 'woocommerce_process_shop_order_meta', [ $this, 'custom_save_order_email' ], 10, 1 );
		}
		( new Integration( $checkout_fields ) )->hooks();
		$this->user_account_hooks();
	}

	/**
	 * @param array $fields
	 *
	 * @return array
	 */
	public function remove_fields_from_checkout( array $fields ): array {
		unset( $fields['billing']['billing_address_2'] );
		unset( $fields['shipping']['shipping_address_2'] );

		return $fields;
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 */
	public function woocommerce_checkout_posted_data_filter( array $data ): array {
		if ( isset( $data['billing_street_number'] ) && isset( $data['billing_flat_number'] ) ) {
			$billing_flat_number       = trim( $data['billing_flat_number'] );
			$billing_street_number     = trim( $data['billing_street_number'] );
			$data['billing_address_2'] = $this->prepare_address_2_value( $billing_street_number, $billing_flat_number );
		}

		if ( isset( $data['shipping_street_number'] ) && isset( $data['shipping_flat_number'] ) ) {
			$shipping_flat_number       = trim( $data['shipping_flat_number'] );
			$shipping_street_number     = trim( $data['shipping_street_number'] );
			$data['shipping_address_2'] = $this->prepare_address_2_value( $shipping_street_number, $shipping_flat_number );
		}

		return $data;
	}

	/**
	 * @param $street_number
	 * @param $flat_number
	 *
	 * @return string
	 */
	public function prepare_address_2_value( $street_number, $flat_number ) {
		return $street_number . ( $flat_number ? '/' . $flat_number : '' );
	}

	/**
	 * @param $post_id
	 *
	 * @return void
	 */
	public function custom_save_order_email( $post_id ) {
		update_post_meta( $post_id, '_billing_address_2', $this->prepare_address_2_value( $_POST['_billing_street_number'], $_POST['_billing_flat_number'] ) );
		update_post_meta( $post_id, '_shipping_address_2', $this->prepare_address_2_value( $_POST['_shipping_street_number'], $_POST['_shipping_flat_number'] ) );
	}

	/**
	 * @param $fields
	 *
	 * @return array
	 */
	public function disable_address_2_field( $fields ): array {
		$fields['address_2']['custom_attributes'] = [ 'disabled' => 'disabled' ];

		return $fields;
	}

	private function user_account_hooks() {
		add_filter( 'woocommerce_address_to_edit', function ( $address ) {
			unset( $address['billing_address_2'], $address['shipping_address_2'] );

			return $address;
		} );

		add_action( 'woocommerce_customer_save_address', function ( $user_id ) {
			$post_data = wp_unslash( $_POST );
			if ( isset( $post_data['billing_street_number'] ) ) {
				$value = $this->prepare_address_2_value( $post_data['billing_street_number'], $post_data['billing_flat_number'] );
				update_user_meta( $user_id, 'billing_address_2', $value );
			}

			if ( isset( $post_data['shipping_street_number'] ) ) {
				$value = $this->prepare_address_2_value( $post_data['shipping_street_number'], $post_data['shipping_flat_number'] );
				update_user_meta( $user_id, 'shipping_address_2', $value );
			}

		} );
	}

}
