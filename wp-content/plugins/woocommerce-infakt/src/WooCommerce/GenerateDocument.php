<?php

namespace WPDesk\inFakt\WooCommerce;

use Psr\Log\LoggerInterface;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\inFakt\Api\Api;
use WPDesk\inFakt\Documents\DocumentFactory;
use WPDesk\inFakt\Documents\DocumentType;
use WPDesk\inFakt\Emails\EmailTrigger;
use WPDesk\inFakt\Exceptions\ResponseErrorException;
use WPDesk\inFakt\Helpers\DocumentHelper;
use WPDesk\inFakt\Settings\SettingsPage;

class GenerateDocument implements Hookable {

	const FOREIGN_INVOICE  = 'merchandise';
	const PL_SLUG          = 'PL';
	const TRANSFER_PAYMENT = 'transfer';

	/**
	 * @var Api
	 */
	private $api;

	/**
	 * @var DocumentFactory
	 */
	private $documents;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param Api             $api
	 * @param DocumentFactory $documents
	 * @param LoggerInterface $logger
	 */
	public function __construct( Api $api, DocumentFactory $documents, LoggerInterface $logger ) {
		$this->api       = $api;
		$this->documents = $documents;
		$this->logger    = $logger;
	}

	/**
	 * Fire hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_generate_document', [ $this, 'wp_ajax_generate_document' ] );
	}

	/**
	 * @param DocumentType $document
	 * @param array        $args
	 * @param bool         $send
	 */
	public function generate_for_order_status( DocumentType $document, array $args, bool $send = true ) {
		try {
			$generate = $this->generate( $document, $args );
			if ( $send ) {
				$this->auto_send( $generate['document'] );
			}
		} catch ( ResponseErrorException $e ) {
			$name = 'infakt_error_' . $document->get_order_id() . '_' . $document->get_type();
			set_transient(
				$name,
				[
					'message' => $e->getMessage(),
					'errors'  => $e->get_error_details(),
				],
				10
			);
			$this->logger->error( __CLASS__ . ' : ' . __METHOD__ . PHP_EOL . $e->getMessage(), $e->get_error_details() );
		}
	}

	/**
	 * Manual generate document from order.
	 */
	public function wp_ajax_generate_document() {
		check_ajax_referer( '_infakt_ajax_nonce', 'security' );
		parse_str( $_REQUEST['post_data'] ?? '', $post_data );
		$type      = $_REQUEST['type'] ?? [];
		$order_id  = isset( $_REQUEST['order_id'] ) ? (int) $_REQUEST['order_id'] : [];
		$documents = $this->documents->get_documents( $order_id );
		if ( isset( $documents[ $type ] ) ) {
			$document = $documents[ $type ];
			try {
				$data     = $post_data[ $type ] ?? [];
				$response = $this->generate( $document, $data );
				if ( $document->is_allowed_to_auto_send() ) {
					$this->auto_send( $response['document'] );
				}
				unset( $response['document'] );
				wp_send_json_success( $this->json_response( 'Document created', $response ) );
			} catch ( ResponseErrorException $e ) {
				$name = 'infakt_error_' . $document->get_order_id() . '_' . $document->get_type();
				set_transient(
					$name,
					[
						'message' => $e->getMessage(),
						'errors'  => $e->get_error_details(),
					],
					HOUR_IN_SECONDS
				);
				$this->logger->error( __METHOD__ . PHP_EOL . $e->getMessage() . PHP_EOL . print_r( $e->get_error_details(), true ) );
				wp_send_json_error( $this->json_response( $e->getMessage(), $e->get_error_details(), 400 ) );
			}
		}
	}


	/**
	 * @param string $message
	 * @param array  $data
	 * @param string $code
	 *
	 * @return array
	 */
	private function json_response( string $message, array $data = [], $code = 200 ): array {
		return [
			'message' => $message,
			'details' => $data,
			'code'    => $code,
		];
	}

	/**
	 * @param DocumentType $document
	 * @param array        $args
	 *
	 * @return array
	 * @throws ResponseErrorException
	 */
	private function generate( DocumentType $document, array $args ): array {
		$order = $document->get_order();
		$args  = apply_filters( 'infakt_document_post_args', wp_parse_args( $args, $document->get_data() ), $order );

		if ( $args['client_country'] !== self::PL_SLUG ) {
			$args['sale_type'] = self::FOREIGN_INVOICE;
		}

		$payment_method = $args['payment_method'] ?? $document->get_payment_method();
		if ( self::TRANSFER_PAYMENT === $payment_method ) {
			$bank_account = $document->get_bank_account();

			if ( $bank_account ) {
				$args = array_merge( $args, $bank_account );
			}
		}

		$args['paid_price'] = DocumentHelper::infakt_format_price( $args['paid_price'] );

		if ( empty( $args['document_markings'] ) ) {
			unset( $args['document_markings'] );
		}
		if ( empty( $args['vat_exemption_reason'] ) ) {
			unset( $args['vat_exemption_reason'] );
		}

		$args['client_id'] = $this->api->get_or_create_client( $document->get_client_data(), $document->get_order() );

		$response = $this->api->create_document( $args, $document->get_order() );
		$document->update_order_data( $response );
		$this->add_generated_note( $document );
		$this->set_paid( $document );
		$name = 'infakt_error_' . $document->get_order_id() . '_' . $document->get_type();
		delete_transient( $name );

		return [
			'document_id'               => $document->get_id(),
			'document_type'             => $document->get_type(),
			'document_number'           => $document->get_number(),
			'document_formatted_number' => $document->get_formatted_name(),
			'order_id'                  => $document->get_order_id(),
			'download_url'              => esc_url( $document->get_url() ),
			'document'                  => $document,
		];

	}

	public function add_generated_note( DocumentType $document ) {
		$note = __( 'Wygenerowano fakturę VAT o nr %s <a href="%s">Wyświetl fakturę &raquo;</a>', 'woocommerce-infakt' );
		$document->get_order()->add_order_note(
			sprintf(
				$note,
				$document->get_number(),
				$document->get_url()
			)
		);
	}

	public function set_paid( DocumentType $document ): bool {
		if ( ! $document->is_generated() ) {
			return false;
		}

		$is_order_paid   = $document->get_order()->is_paid();
		$order_total     = $document->get_order()->get_total();
		$order_paid_date = $document->get_order()->get_date_paid();
		$order_total     = DocumentHelper::infakt_format_price( $order_total );

		if ( ! $is_order_paid || $document->get_paid_price() !== (float) $order_total ) {
			return false;
		}

		$this->api->set_paid( $document->get_id(), $order_paid_date );

		return $document->set_status( 'paid' );
	}

	/**
	 * @param DocumentType $document
	 */
	private function auto_send( DocumentType $document ) {
		$mailer     = WC()->mailer();
		$emails     = $mailer->get_emails();
		$email_slug = $document->get_email_slug();

		if ( isset( $emails[ $email_slug ] ) && $document->is_allowed_to_auto_send() ) {
			$email = $emails[ $email_slug ];
			if ( $email instanceof EmailTrigger ) {
				$email->trigger( $document->get_order(), $document );
			}
		}
	}

}
