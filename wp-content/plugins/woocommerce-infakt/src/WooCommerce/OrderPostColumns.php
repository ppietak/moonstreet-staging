<?php

namespace WPDesk\inFakt\WooCommerce;

use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\inFakt\Documents\DocumentFactory;

class OrderPostColumns implements Hookable {

	const COLUMN_NUMBER_SLUG = 'document_number';

	/**
	 * @var DocumentFactory
	 */
	private $documents;

	/**
	 * @param DocumentFactory $documents
	 */
	public function __construct( DocumentFactory $documents ) {
		$this->documents = $documents;
	}

	/**
	 * Fire hooks.
	 */
	public function hooks() {
		add_filter( 'manage_shop_order_posts_columns', [ $this, 'shop_order_posts_columns' ], 20 );
		add_action( 'manage_shop_order_posts_custom_column', [ $this, 'shop_order_posts_custom_column' ], 10, 2 );
		add_filter( 'manage_woocommerce_page_wc-orders_columns', [ $this, 'shop_order_posts_columns' ] );
		add_action( 'manage_woocommerce_page_wc-orders_custom_column', [ $this, 'shop_order_posts_custom_column' ], 10, 2 );
	}

	/**
	 * @param array $columns
	 *
	 * @return array
	 */
	public function shop_order_posts_columns( array $columns ): array {
		$order_actions_pos = array_search( $this->get_shop_order_actions_column_name(), array_keys( $columns ), true );

		return array_merge(
			array_slice( $columns, 0, $order_actions_pos ),
			[ self::COLUMN_NUMBER_SLUG => __( 'inFakt', 'woocommerce-infakt' ) ],
			array_slice( $columns, $order_actions_pos )
		);
	}

	/**
	 * @return string
	 */
	private function get_shop_order_actions_column_name(): string {
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.3', '<' ) ) {
			return 'order_actions';
		}

		return 'wc_actions';
	}

	/**
	 * @throws \Exception
	 */
	public function get_order_for_meta_box( $object ): int {
		if ( $object instanceof \WP_Post ) {
			return (int) $object->ID;
		} elseif ( $object instanceof \WC_Order ) {
			return (int) $object->get_id();
		} elseif ( is_integer( $object ) ) {
			return (int) $object;
		}

		throw new \Exception( 'Unknown order object!' );
	}

	/**
	 * @param string                      $column
	 * @param int|\WP_Post|\WC_Order|null $order_id
	 *
	 * @throws \Exception
	 */
	public function shop_order_posts_custom_column( $column, $is_order ) {
		if ( self::COLUMN_NUMBER_SLUG === $column ) {
			try {
				$order_id  = $this->get_order_for_meta_box( $is_order );
				$documents = $this->documents->get_documents( $order_id );
				foreach ( $documents as $document ) {
					if ( is_wp_error( $document ) ) {
						return;
					}
					$url = $document->get_url();
					if ( $url ) {
						printf( '<p><a href="%s" target="_blank">%s</a></p>', $url, $document->get_formatted_name() );
					}
				}
			} catch ( \Exception $e ) {
				// logger
			}
		}
	}

}
