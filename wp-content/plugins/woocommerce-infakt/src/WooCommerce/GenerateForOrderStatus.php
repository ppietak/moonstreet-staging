<?php

namespace WPDesk\inFakt\WooCommerce;

use Exception;
use Psr\Log\LoggerInterface;
use WC_Order;
use WInfaktVendor\WPDesk\Mutex\WordpressMySQLLockMutex;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\inFakt\Documents\DocumentFactory;

class GenerateForOrderStatus implements Hookable {

	/**
	 * @var DocumentFactory
	 */
	private $documents;

	/**
	 * @var GenerateDocument
	 */
	private $generate;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param DocumentFactory  $documents
	 * @param GenerateDocument $generate
	 * @param LoggerInterface  $logger
	 */
	public function __construct( DocumentFactory $documents, GenerateDocument $generate, LoggerInterface $logger ) {
		$this->documents = $documents;
		$this->generate  = $generate;
		$this->logger    = $logger;
	}

	public function hooks() {
		$order_statuses = wc_get_order_statuses();
		foreach ( $order_statuses as $status => $status_label ) {
			$status = str_replace( 'wc-', '', $status );
			add_action( 'woocommerce_order_status_' . $status, [ $this, 'should_auto_create' ], 10, 2 );
		}
	}

	/**
	 * @param int      $order_id
	 * @param WC_Order $order
	 */
	public function should_auto_create( int $order_id, WC_Order $order ) {
		$order_status = $order->get_status();
		foreach ( $this->documents->get_documents( $order_id ) as $document ) {
			if ( $document->is_only_company() ) {
				continue;
			}

			if ( is_wp_error( $document ) ) {
				continue;
			}

			try {
				$mutex = new WordpressMySQLLockMutex( '_infakt_mutex', 30 );
				if ( ! $mutex->acquireLock() ) {
					throw new Exception( 'Cannot acquire lock' );
				}
				try {
					if ( ! $document->is_generated() && $document->is_allowed_to_auto_generate() && in_array( $order_status, $document->get_auto_generate_statuses(), true ) ) {
						$this->generate->generate_for_order_status( $document, [], $document->is_allowed_to_auto_send() );
						sleep( 1 );
					}
				} finally {
					$mutex->releaseLock();
					unset( $document );
				}
			} catch ( Exception $e ) {
				$this->logger->error( $e->getMessage() );
			}
		}
	}
}
