<?php

namespace WPDesk\inFakt\Documents;

use WC_Order;
use WC_Order_Item;
use WC_Order_Item_Product;
use WC_Order_Item_Shipping;
use WC_Product;
use WC_Product_Variation;
use WC_Tax;
use WP_Error;
use WPDesk\inFakt\Extensions\FQIntegration;
use WPDesk\inFakt\Extensions\GoodsServicesDesignation;
use WPDesk\inFakt\Helpers\DocumentHelper;
use WPDesk\inFakt\Helpers\PaymentMethods;
use WPDesk\inFakt\Settings\SettingsPage;

class AbstractDocument implements DocumentType {

	const TYPE = 'invoice';

	/**
	 * @var int
	 */
	public $document_id = 0;

	/**
	 * @var string
	 */
	public $document_type = 'invoice';

	/**
	 * @var int
	 */
	public $document_number = 0;

	/**
	 * @var bool
	 */
	public $document_status = false;

	/**
	 * @var float
	 */
	public $document_paid_price = 0.0;

	/**
	 * @var WC_Order
	 */
	public $order;

	/**
	 * @var int
	 */
	public $order_id;

	/**
	 * @var array
	 */
	public $order_tax_rates = [];

	/**
	 * @var string
	 */
	private const TAX_EXEMPT_RATE = 'zw.';
	private const ZERO_RATE       = '0';

	/**
	 * @param int $order_id
	 *
	 * @return void|WP_Error
	 */
	public function init( int $order_id ) {
		$this->order = wc_get_order( $order_id );

		if ( ! $this->order ) {
			return new WP_Error( 'infakt', __( 'Nie ma takiego zamówienia.', 'woocommerce-infakt' ) );
		}

		$this->order_id = $this->order->get_id();
		$meta           = DocumentHelper::get_meta_data_for_document( $this->order, $this->get_type() );

		$this->document_id         = (int) $meta['document_id'];
		$this->document_type       = (string) $meta['document_type'];
		$this->document_number     = (string) $meta['document_number'];
		$this->document_status     = (string) $meta['document_status'];
		$this->document_paid_price = (float) $meta['document_paid_price'];
	}

	/**
	 * @return WC_Order
	 */
	public function get_order(): WC_Order {
		return $this->order;
	}

	/**
	 * @return bool
	 */
	public function is_generated(): bool {
		return $this->get_number() && ! empty( $this->get_id() ) && ! empty( $this->get_type() );
	}

	/**
	 * @return bool
	 */
	public function is_only_company(): bool {
		$only_company = DocumentHelper::get_infakt_option( SettingsPage::OPTION_ONLY_COMPANY );

		return $only_company && ! $this->is_company_order();
	}

	/**
	 * @return bool
	 */
	private function is_company_order(): bool {
		return ! empty( $this->order->get_billing_company() ) && ! empty( $this->get_order_billing_company_nip() );
	}

	/**
	 * @return int
	 */
	public function get_id(): int {
		return $this->document_id;
	}

	/**
	 * @return string
	 */
	public function get_type(): string {
		return static::TYPE;
	}

	/**
	 * @return string
	 */
	public function get_number(): string {
		return $this->document_number;
	}

	public function get_formatted_name(): string {
		$number = $this->get_number();
		if ( ! $number ) {
			return '';
		}

		$formatted_name = trim( sprintf( '%s %s', $this->get_label(), $this->get_number() ) );

		return ! empty( $formatted_name ) ? $formatted_name : false;
	}

	public function get_status(): bool {
		return $this->document_status;
	}

	public function get_paid_price(): float {
		return $this->document_paid_price;
	}

	public function get_data(): array {
		return [
			'currency'               => $this->get_order_currency(),
			'paid_price'             => $this->get_order_paid_price(),
			'kind'                   => 'vat',
			'payment_method'         => $this->get_payment_method(),
			'notes'                  => $this->get_order_notes(),
			'recipient_signature'    => $this->order->get_formatted_billing_full_name(),
			'seller_signature'       => DocumentHelper::get_infakt_option( 'seller_signature', '' ),
			'invoice_date'           => DocumentHelper::infakt_format_date(),
			'sale_date'              => $this->get_order_paid_date(),
			'invoice_date_kind'      => 'sale_date',
			'payment_date'           => $this->get_order_payment_date(),
			'check_duplicate_number' => false,
			'services'               => $this->get_services(),
			'vat_exemption_reason'   => DocumentHelper::get_infakt_option( 'vat_exemption_reason', '' ),
		];
	}

	/**
	 * @return array
	 */
	public function get_client_data(): array {
		return [
			'client_company_name'  => $this->get_order_client_company_name(),
			'client_first_name'    => $this->get_order_client_first_name(),
			'client_last_name'     => $this->get_order_client_last_name(),
			'client_tax_code'      => $this->get_order_client_country() !== 'PL' ? $this->get_order_client_country() . $this->get_order_client_company_nip() : $this->get_order_client_company_nip(),
			'client_street'        => $this->get_order_client_street(),
			'client_street_number' => $this->get_order_client_street_number(),
			'client_flat_number'   => $this->get_order_client_flat_number(),
			'client_city'          => $this->get_order_client_city(),
			'client_post_code'     => $this->get_order_client_postcode(),
			'client_country'       => $this->get_order_client_country(),
		];
	}

	/**
	 * @return int
	 */
	public function get_order_id(): int {
		return $this->order_id;
	}

	/**
	 * @return bool
	 */
	public function is_customer_want_document(): bool {
		return (bool) $this->order->get_meta( '_billing_' . $this->get_type() . '_ask' );
	}

	/**
	 * @return string
	 */
	public function get_order_currency(): string {
		return $this->order->get_currency();
	}

	/**
	 * Use string! Do not change type!!!
	 *
	 * @return float
	 */
	public function get_order_paid_price(): float {
		if ( $this->order->get_payment_method() === 'cod' && $this->order->get_status() !== 'completed' ) {
			return 0;
		}
		if ( $this->get_type() === 'proforma' ) {
			return 0;
		}

		return $this->order->is_paid() ? $this->order->get_total() : 0;
	}

	/**
	 * @return string
	 */
	public function get_order_payment_method(): string {
		return $this->order->get_payment_method();
	}

	/**
	 * @return string
	 */
    public function get_payment_method(): string {
        $payment_method = $this->get_order_payment_method();
        $methods        = PaymentMethods::get();

        if ( $payment_method === 'bacs' ) {
            $payment_method = 'transfer';
        }

        if ( $payment_method === 'cod' ) {
            $payment_method = 'delivery';
        }

        if ( strpos( $payment_method, 'przelewy24' ) !== false ) {
            return 'przelewy24';
        }

        if ( isset( $methods[ $payment_method ] ) ) {
            return $payment_method;
        }

        return 'other';
    }

	/**
	 * Returns order number. That number can also be string.
	 *
	 * @return string
	 */
	public function get_order_number() {
		if ( $this->order instanceof WC_Order ) {
			return $this->order->get_order_number();
		}

		return $this->get_order_id();
	}

	public function get_order_notes(): string {
		return sprintf( 'Zamówienie #%d', $this->get_order_number() );
	}

	public function get_order_client_company_name() {
		return $this->is_company_order() ? $this->order->get_billing_company() : '';
	}


	public function get_order_client_first_name() {
		return $this->order->get_billing_first_name();
	}

	public function get_order_client_last_name() {
		return $this->order->get_billing_last_name();
	}

	public function get_order_client_company_nip() {
		return $this->get_order_billing_company_nip();
	}

	public function get_order_client_street(): string {
		if ( DocumentHelper::get_infakt_option( SettingsPage::OPTION_CLIENT_ADDRESS2 ) ) {
			return sprintf( '%s', $this->get_order_client_address_1() );
		}

		return trim( sprintf( '%s %s', $this->get_order_client_address_1(), $this->get_order_client_address_2() ) );
	}

	public function get_order_client_street_number(): string {
		$address = $this->order->get_address();

		return $address['street_number'] ?? '';
	}

	public function get_order_client_flat_number(): string {
		$address = $this->order->get_address();

		return $address['flat_number'] ?? '';
	}

	public function get_order_client_address_1() {
		return $this->order->get_billing_address_1();
	}

	public function get_order_client_address_2() {
		return $this->order->get_billing_address_2();
	}

	public function get_order_client_city() {
		return $this->order->get_billing_city();
	}

	public function get_order_client_postcode() {
		$postcode = preg_replace( '/\D/', '', $this->order->get_billing_postcode() );

		return strlen( $postcode ) === 5 ? substr_replace( $postcode, '-', 2, 0 ) : '';
	}

	public function get_order_client_country() {
		return $this->order->get_billing_country();
	}

	public function get_order_paid_date() {
		$date = $this->order->get_date_paid();

		if ( is_null( $date ) ) {
			$date = $this->order->get_date_completed();
		}

		return DocumentHelper::infakt_format_date( $date );
	}

	/**
	 * @return false|int|string
	 */
	public function get_order_payment_date() {
		if ( 'cod' === $this->get_order_payment_method() ) {
			return DocumentHelper::infakt_get_default_payment_date();
		}

		return DocumentHelper::infakt_format_date();
	}

	public function get_order_tax_rates(): array {
		if ( empty( $this->order_tax_rates ) ) {
			$order_taxes = $this->order->get_taxes();

			if ( empty( $order_taxes ) ) {
				return [];
			}

			$this->order_tax_rates = array_values(
				array_map( static function ( $tax ) {
					return $tax->get_rate_id();
				}, $order_taxes )
			);
		}

		return $this->order_tax_rates;
	}

	public function get_services(): array {
		return array_merge(
			$this->get_items(),
			$this->get_shipping_services()
		);
	}

	private function get_gtu_code( WC_Order_Item $item ) {
		$code = '';
		if ( $item instanceof WC_Order_Item_Product ) {
			$code = get_post_meta( $item->get_product_id(), GoodsServicesDesignation::GTU_CODE_KEY, true );
			if ( ! empty( $code ) ) {
				return $code;
			}
		}

		return $code;
	}

	public function get_items(): array {
		$services   = [];
		$items      = $this->order->get_items( [ 'line_item', 'fee' ] );
		$calc_taxes = get_option( 'woocommerce_calc_taxes', '0' );

		foreach ( $items as $item ) {
			if ( 0.0 === (float) $this->get_item_price( $item ) ) {
				continue;
			}
			$fq = new FQIntegration( $item );

			$gtu_code = $this->get_gtu_code( $item );
			$service  = [
				'name'       => $this->get_item_name( $item ),
				'unit'       => $fq->get_item_unit(),
				'quantity'   => $this->get_item_quantity( $item ),
				'tax_symbol' => $this->get_tax_symbol( $item, $calc_taxes ),
			];
			if ( $gtu_code ) {
				$service['gtu_id'] = $gtu_code;
			}

			$price_type = $this->get_item_price_type();
			$price      = $this->get_item_price( $item );

			$service[ $price_type ] = DocumentHelper::infakt_format_price( $price );

			if ( $this->is_lump_sum_enabled() ) {
				$service = $this->set_lump_for_item( $service, $item );
			}

			$services[] = $service;
		}

		return $services;
	}

	/**
	 * @param $item
	 * @param $calc_taxes
	 *
	 * @return string
	 */
	private function get_tax_symbol( $item, $calc_taxes ): string {
		if ( 'yes' === $calc_taxes ) {
			$tax = $this->get_order_item_tax( $item );
			if ( $tax === '0' ) {
				$rate_zw = DocumentHelper::get_infakt_option( SettingsPage::OPTION_INVOICE_ZW_RATE );
				if ( $rate_zw === $item->get_tax_class() ) {
					$tax = self::TAX_EXEMPT_RATE;
				}
				$rate_zero = DocumentHelper::get_infakt_option( SettingsPage::OPTION_INVOICE_0_RATE );
				if ( $rate_zero === $item->get_tax_class() ) {
					$tax = self::ZERO_RATE;
				}
			}
		} else {
			$tax = self::TAX_EXEMPT_RATE;
		}

		return $tax;
	}

	/**
	 * @param WC_Order_Item $item
	 *
	 * @return string
	 */
	public function get_order_item_tax( WC_Order_Item $item ): string {
		if ( ! $item->is_type( 'fee' ) && $item->get_tax_status() !== 'taxable' ) {
			return '0';
		}

		return $this->get_tax( $item );
	}

	/*
	 * Get tax for order item or shipping method
	 *
	 * @param bool $item
	 *
	 * @return string
	 */
	public function get_tax( $item ): string {
		$taxes = $item->get_taxes();
		$taxes = ! empty( $taxes['total'] ) ? $taxes['total'] : false;
		if ( is_array( $taxes ) ) {
			$taxes = array_filter( $taxes, 'strlen' );
			if ( ! empty( $taxes ) ) {
				foreach ( $this->get_order_tax_rates() as $order_rate_id ) {

					if ( isset( $taxes[ $order_rate_id ] ) ) {
						$percent = WC_Tax::get_rate_percent( $order_rate_id );

						return (string) round( trim( $percent, '%' ) );
					}
				}
			}
		}

		return '0';
	}

	/**
	 * Is lump sum enabled.
	 *
	 * @return bool
	 */
	public function is_lump_sum_enabled() {
		return DocumentHelper::get_infakt_option( SettingsPage::OPTION_LUMP_SUM_ENABLED );
	}

	/**
	 * Get lump sum attribute.
	 *
	 * @return string
	 */
	public function get_lump_sum_attribute(): string {
		return DocumentHelper::get_infakt_option( SettingsPage::OPTION_LUMP_SUM_ATTRIBUTE, '' );
	}


	/**
	 * Get lump sum default.
	 *
	 * @return string
	 */
	public function get_lump_sum_default(): string {
		return DocumentHelper::get_infakt_option( SettingsPage::OPTION_LUMP_SUM_DEFAULT, '' );
	}

	/**
	 * Prepare float lump sum rate value from possible string values.
	 *
	 * @param string $lump_sum_rate Lump sum rate as string.
	 *
	 * @return float
	 */
	private function prepareLumpSumRate( string $lump_sum_rate ): float {
		$lump_sum_rate = str_replace( ',', '.', $lump_sum_rate );
		$lump_sum_rate = str_replace( '%', '', $lump_sum_rate );

		return floatval( $lump_sum_rate );
	}

	/**
	 * @param array         $service
	 * @param WC_Order_Item $order_item
	 *
	 * @return array
	 */
	private function set_lump_for_item( array $service, WC_Order_Item $order_item ): array {
		if ( $order_item->is_type( 'line_item' ) ) {
			$product = $order_item->get_product();

			if ( $product instanceof WC_Product ) {
				$lump_sum_rate = '';
				if ( ! empty( $this->get_lump_sum_attribute() ) ) {
					$lump_sum_rate = $this->prepareLumpSumRate( $product->get_attribute( $this->get_lump_sum_attribute() ) );
				}
				if ( empty( $lump_sum_rate ) ) {
					$lump_sum_rate = $this->prepareLumpSumRate( $this->get_lump_sum_default() );
				}
				$service['flat_rate_tax_symbol'] = $lump_sum_rate;
			}

			if ( $product instanceof WC_Product_Variation ) {
				$product = wc_get_product( $product->get_parent_id() );
				if ( $product ) {
					$lump_sum_rate = '';
					if ( ! empty( $this->get_lump_sum_attribute() ) ) {
						$lump_sum_rate = $this->prepareLumpSumRate( $product->get_attribute( $this->get_lump_sum_attribute() ) );
					}
					if ( empty( $lump_sum_rate ) ) {
						$lump_sum_rate = $this->prepareLumpSumRate( $this->get_lump_sum_default() );
					}

					$service['flat_rate_tax_symbol'] = $lump_sum_rate;
				}
			}
		} else {
			$lump_sum_rate                   = $this->prepareLumpSumRate( $this->get_lump_sum_default() );
			$service['flat_rate_tax_symbol'] = $lump_sum_rate;
		}

		return $service;
	}


	public function get_item_name( $item ) {
		$product = $item->get_product();
		$name = $item->get_name();

		if ($product->is_type('variation')) {
			$ticket = get_term_by('slug', $item->get_meta('pa_ticket') ?? '', 'pa_ticket');
			$scenario = get_term_by('slug', $item->get_meta('pa_scenario') ?? '', 'pa_scenario');
			$city = get_term_by('slug', $item->get_meta('pa_city') ?? '', 'pa_city');

			if ($ticket && $scenario && $city) {
				$name = sprintf(
					"Bilet wejścia %s %s (%s)",
					$ticket ? $ticket->name : '',
					$scenario ? $scenario->name : '',
					$city ? $city->name : ''
				);
			}
		}

		if ( empty( $name ) && $item->is_type( 'fee' ) ) {
			return __( 'Dodatkowa opłata', 'woocommerce-infakt' );
		}

		return $name;
	}

	public function get_item_quantity( $item ) {
		return $item->get_quantity();
	}

	public function get_item_price_type(): string {
		$include_tax = get_option( 'woocommerce_prices_include_tax', 'no' );

		return wc_string_to_bool( $include_tax ) ? 'gross_price' : 'net_price';
	}

	public function get_item_price( $item ) {
		$include_tax = get_option( 'woocommerce_prices_include_tax', 'no' );
		$total       = $item->get_total();

		return wc_string_to_bool( $include_tax ) ? $total + $item->get_total_tax() : $total;
	}

	public function get_bank_account() {
		$bacs_accounts = get_option( 'woocommerce_bacs_accounts', [] );

		if ( empty( $bacs_accounts ) ) {
			return false;
		}

		$bacs_account_id = DocumentHelper::get_infakt_option( 'bacs_account', 0 );

		if ( ! isset( $bacs_accounts[ $bacs_account_id ] ) ) {
			return false;
		}

		$account = $bacs_accounts[ $bacs_account_id ];

		return [
			'bank_name'    => ! empty( $account['bank_name'] ) ? $account['bank_name'] : '',
			'bank_account' => ! empty( $account['account_number'] ) ? $account['account_number'] : '',
			'swift'        => ! empty( $account['bic'] ) ? $account['bic'] : '',
		];
	}

	public function get_shipping_services(): array {
		$shipping       = [];
		$shipping_items = $this->order->get_items( 'shipping' );
		$calc_taxes     = get_option( 'woocommerce_calc_taxes', '0' );

		foreach ( $shipping_items as $item ) {
			if ( $item instanceof WC_Order_Item_Shipping ) {
				$net_price = DocumentHelper::infakt_format_price( $item->get_total() );

				if ( 0 === (int) $item->get_total() ) {
					continue;
				}

				if ( 'yes' === $calc_taxes ) {
					$tax_symbol = $this->get_tax( $item );
				} else {
					$tax_symbol = self::TAX_EXEMPT_RATE;
				}

				$shipping_item = [
					'name'       => ! empty( $item->get_name() ) ? $item->get_name() : __( 'Koszty dostawy', 'woocommerce-infakt' ),
					'unit'       => 'szt',
					'quantity'   => 1,
					'net_price'  => $net_price,
					'tax_symbol' => $tax_symbol,
				];

				if ( $this->is_lump_sum_enabled() ) {
					$shipping_item = $this->set_lump_for_item( $shipping_item, $item );
				}

				$shipping[] = $shipping_item;
			}
		}

		return $shipping;
	}

	public function get_filename() {
		if ( $this->is_generated() ) {
			return sprintf( '%s.pdf', mb_strtolower(
				preg_replace(
					'/[\\/ ]+/',
					'-',
					trim( $this->get_formatted_name() )
				)
			) );
		}

		return false;
	}

	/**
	 * @param string $status
	 *
	 * @return false
	 */
	public function set_status( string $status ): bool {
		if ( empty( $status ) ) {
			return false;
		}

		$this->document_status = $status;
		$this->order->update_meta_data( '_infakt_' . $this->get_type() . '_status', $status );
		$this->order->save();

		return true;
	}

	public function update_order_data( array $data ) {
		$document_meta = [
			'id'         => $data['id'] ?? 0,
			'number'     => $data['number'] ?? 0,
			'type'       => $data['kind'] === 'vat' ? 'invoice' : $data['kind'],
			'paid_price' => $data['paid_price'] ?? 0.0,
			'status'     => $data['status'] ?? '',
		];

		foreach ( $document_meta as $meta_key => $meta_value ) {
			$meta_key = $this->get_type() . '_' . $meta_key;
			$this->order->update_meta_data( '_infakt_' . $meta_key, $meta_value );

			$property_key        = str_replace( $this->get_type(), 'document', $meta_key );
			$this->$property_key = $meta_value;
		}

		$this->order->save();
	}

	/**
	 * @return string
	 */
	public function get_hash(): string {
		return md5( $this->document_id . $this->document_number . $this->order->get_date_created() );
	}

	/**
	 * @return string
	 */
	public function get_url(): string {
		return add_query_arg(
			[
				'action'   => 'infakt_document',
				'order_id' => $this->order->get_id(),
				'type'     => $this->get_type(),
				'hash'     => $this->get_hash(),
			],
			admin_url( 'admin-ajax.php' )
		);
	}

	public function get_email_send_url() {
		return add_query_arg(
			[
				'action'   => 'infakt_send_email',
				'order_id' => $this->order->get_id(),
				'type'     => $this->get_type(),
				'hash'     => $this->get_hash(),
			],
			admin_url( 'admin-ajax.php' )
		);
	}

	public function get_order_billing_company_nip() {
		return $this->order->get_meta( '_billing_company_nip' );
	}

	/**
	 * @return string
	 */
	public function get_label(): string {
		return esc_html__( 'Faktura', 'woocommerce-infakt' );
	}

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_generate(): bool {
		return false;
	}

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_send(): bool {
		return false;
	}

	/**
	 * @return array
	 */
	public function get_auto_generate_statuses(): array {
		$generate_status = DocumentHelper::get_infakt_option( $this->get_type() . '_generate_status', [] );
		if ( ! is_array( $generate_status ) ) {
			$generate_status = [ $generate_status ];
		}

		return $generate_status;
	}

	/**
	 * @return string
	 */
	public function get_email_slug(): string {
		return 'DocumentEmail';
	}
}
