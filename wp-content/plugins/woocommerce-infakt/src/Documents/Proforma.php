<?php

namespace WPDesk\inFakt\Documents;

use WPDesk\inFakt\Emails\RegisterEmails;
use WPDesk\inFakt\Helpers\DocumentHelper;

class Proforma extends AbstractDocument {

	const TYPE = 'proforma';

	/**
	 * @return string
	 */
	public function get_label(): string {
		return esc_html__( 'Proforma', 'woocommerce-infakt' );
	}

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_send(): bool {
		return (bool) DocumentHelper::get_infakt_option( $this->get_type() . '_auto_send', 'no' );
	}

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_generate(): bool {
		return true;
	}

	/**
	 * @return string
	 */
	public function get_email_slug(): string {
		return RegisterEmails::EMAIL_PROFORMA_SLUG;
	}

	public function get_data(): array {
		$data         = parent::get_data();
		$data['kind'] = self::TYPE;

		return $data;
	}

}
