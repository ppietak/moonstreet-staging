<?php

namespace WPDesk\inFakt\Documents;

use WC_Order;

interface DocumentType {

	/**
	 * @param int $order_id
	 *
	 * @return mixed
	 */
	public function init( int $order_id );

	/**
	 * @return bool
	 */
	public function is_generated(): bool;

	/**
	 * @return WC_Order
	 */
	public function get_order(): WC_Order;

	/**
	 * @return int
	 */
	public function get_order_id(): int;

	/**
	 * @return string
	 */
	public function get_type(): string;

	/**
	 * @return int
	 */
	public function get_number(): string;

	/**
	 * @return string
	 */
	public function get_formatted_name(): string;

	/**
	 * @return float
	 */
	public function get_paid_price(): float;

	/**
	 * @return array
	 */
	public function get_data(): array;

	/**
	 * @return array
	 */
	public function get_client_data(): array;

	/**
	 * @return string
	 */
	public function get_url(): string;

	/**
	 * @return string
	 */
	public function get_hash(): string;

	/**
	 * @return string
	 */
	public function get_label(): string;

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_generate(): bool;

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_send(): bool;

	/**
	 * @return array
	 */
	public function get_auto_generate_statuses(): array;

	/**
	 * @return string
	 */
	public function get_email_slug(): string;

	/**
	 * @return boolean
	 */
	public function is_only_company(): bool;

}
