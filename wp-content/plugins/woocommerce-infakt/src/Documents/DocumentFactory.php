<?php

namespace WPDesk\inFakt\Documents;

/**
 * Register documents.
 */
class DocumentFactory {

	/**
	 * @var DocumentType[]
	 */
	private $documents;

	/**
	 * @var array
	 */
	private $types = [];

	/**
	 * @param string       $type
	 * @param DocumentType $document
	 */
	public function add_document_type( string $type, DocumentType $document ) {
		$this->documents[ $type ] = $document;
		$this->types[ $type ]     = $document->get_label();
	}

	/**
	 * @return DocumentType[]
	 */
	public function get_documents( $order_id ): array {
		$documents = [];
		foreach ( $this->documents as $type => $document ) {
			$document->init( $order_id );
			$documents[ $type ] = $document;
		}

		return $documents;
	}

	/**
	 * @return array
	 */
	public function get_types(): array {
		return $this->types;
	}

}
