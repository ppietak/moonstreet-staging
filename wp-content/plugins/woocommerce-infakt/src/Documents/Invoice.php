<?php

namespace WPDesk\inFakt\Documents;

use WPDesk\inFakt\Emails\RegisterEmails;
use WPDesk\inFakt\Helpers\DocumentHelper;

class Invoice extends AbstractDocument {

	const TYPE = 'invoice';

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_generate(): bool {
		$generate_mode    = DocumentHelper::get_infakt_option( $this->get_type() . '_generate_mode' );
		$generate_invoice = $this->is_customer_want_document();

		return ( 'auto' === $generate_mode || ( 'auto_ask' === $generate_mode && $generate_invoice ) );
	}

	/**
	 * @return bool
	 */
	public function is_allowed_to_auto_send(): bool {
		return (bool) DocumentHelper::get_infakt_option( $this->get_type() . '_auto_send', 'no' );
	}

	/**
	 * @return string
	 */
	public function get_email_slug(): string {
		return RegisterEmails::EMAIL_INVOICE_SLUG;
	}

	/**
	 * @return array
	 */
	public function get_data(): array {
		$data      = parent::get_data();
		$procedure = DocumentHelper::get_infakt_option( $this->get_type() . '_document_markings' );

		if ( ! empty( $procedure ) ) {
			$data['document_markings_ids'] = [ $procedure ];
		}

		return $data;
	}

}
