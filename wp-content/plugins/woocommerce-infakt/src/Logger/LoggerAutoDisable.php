<?php

namespace WPDesk\inFakt\Logger;

use WInfaktVendor\WPDesk\Persistence\PersistentContainer;

/**
 * Auto disable
 */
class LoggerAutoDisable {
	/**
	 * @var PersistentContainer
	 */
	private $settings;

	/**
	 * @param PersistentContainer $container
	 */
	public function __construct( PersistentContainer $container ) {
		$this->settings = $container;
	}

	public function hooks() {
		add_action( 'admin_init', [ $this, 'disable_logger' ], 100 );
	}

	public function disable_logger() {
		if ( $this->settings->get_fallback( 'debug_mode' ) === 'yes' ) {
			$disable_time = (int) $this->settings->get_fallback( 'debug_mode_auto_disable', 0 );
			$time         = (int) get_option( 'infakt_debug_timestamp', 0 );
			if ( $disable_time && $time ) {
				if ( current_time( 'timestamp' ) > $time + $disable_time ) {
					$this->settings->delete( 'debug_mode' );
					delete_option( 'infakt_debug_timestamp' );
				}
			}
		}
	}

}
