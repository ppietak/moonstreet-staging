<?php

namespace WPDesk\inFakt\Logger;

use Psr\Log\AbstractLogger;
use WInfaktVendor\WPDesk\Logger\Settings;
use WInfaktVendor\WPDesk\Logger\SimpleLoggerFactory;

/**
 * Plugin logger.
 */
class PluginLogger extends AbstractLogger {

	/**
	 * Log name.
	 *
	 * @var string
	 */
	private $source;

	/**
	 * @param string $source
	 */
	public function __construct( string $source ) {
		$this->source = $source;
	}

	/**
	 * @return \WInfaktVendor\Monolog\Logger
	 */
	private function get_logger() {
		$settings             = new Settings();
		$settings->use_wp_log = false;

		return ( new SimpleLoggerFactory( $this->source, $settings ) )->getLogger();
	}

	/**
	 * @param       $level
	 * @param       $message
	 * @param array $context
	 *
	 * @return void
	 */
	public function log( $level, $message, array $context = [] ) {
		$data = is_array( $message ) || is_object( $message ) ? print_r( $message, true ) : $message;

		$this->get_logger()->log( $level, $data, array_merge( $context, [ 'source' => $this->source ] ) );
	}

}
