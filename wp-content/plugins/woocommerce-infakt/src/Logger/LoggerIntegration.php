<?php

namespace WPDesk\inFakt\Logger;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use WInfaktVendor\WPDesk\Persistence\Adapter\WordPress\WordpressSerializedOptionsContainer;
use WInfaktVendor\WPDesk\Persistence\PersistentContainer;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Logger Integration
 */
class LoggerIntegration implements Hookable {

	/**
	 * @var string
	 */
	private $source;

	/**
	 * @var WordpressSerializedOptionsContainer
	 */
	private $settings;

	/**
	 * @param string $source
	 * @param string $settings_name
	 */
	public function __construct( string $source, string $settings_name ) {
		$this->source   = $source;
		$this->settings = new WordpressSerializedOptionsContainer( 'woocommerce_integration-' . $settings_name );
	}

	public function hooks() {
		( new LoggerNotices( $this->settings ) )->hooks();
		( new LoggerAutoDisable( $this->settings ) )->hooks();
	}

	/**
	 * @return PersistentContainer
	 */
	public function get_settings(): PersistentContainer {
		return $this->settings;
	}

	/**
	 * @return LoggerInterface
	 */
	public function get_logger(): LoggerInterface {
		return $this->settings->get_fallback( 'debug_mode', 0 ) === 'yes' ? ( new PluginLogger( $this->source ) ) : new NullLogger();
	}

	/**
	 * @return bool
	 */
	public function should_exit(): bool {
		return $this->settings->get_fallback( 'debug_mode', 0 ) === 'yes' && $this->settings->get_fallback( 'debug_mode_with_exit', 0 ) === 'yes' && current_user_can( 'manage_options' );
	}
}
