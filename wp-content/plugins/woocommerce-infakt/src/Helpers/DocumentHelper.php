<?php

namespace WPDesk\inFakt\Helpers;

use WC_DateTime;
use WC_Order;

class DocumentHelper {

	/**
	 * Returns the correct meta keys for generated documents while maintaining backward compatibility.
	 *
	 * @param WC_Order $order
	 * @param string   $new_type
	 *
	 * @return array
	 */
	public static function get_meta_data_for_document( WC_Order $order, string $new_type ): array {
		$type = $order->get_meta( '_infakt_document_type' );
		if ( $type === 'vat' && $new_type === 'invoice' ) {
			$meta['document_id']         = $order->get_meta( '_infakt_document_id' );
			$meta['document_type']       = $type;
			$meta['document_number']     = $order->get_meta( '_infakt_document_number' );
			$meta['document_status']     = $order->get_meta( '_infakt_document_status' );
			$meta['document_paid_price'] = $order->get_meta( '_infakt_document_paid_price' );
		} elseif ( $type === 'proforma' && $new_type === 'proforma' ) {
			$meta['document_id']         = $order->get_meta( '_infakt_document_id' );
			$meta['document_type']       = $type;
			$meta['document_number']     = $order->get_meta( '_infakt_document_number' );
			$meta['document_status']     = $order->get_meta( '_infakt_document_status' );
			$meta['document_paid_price'] = $order->get_meta( '_infakt_document_paid_price' );
		} else {
			$meta['document_id']         = $order->get_meta( '_infakt_' . $new_type . '_id' );
			$meta['document_type']       = $order->get_meta( '_infakt_' . $new_type . '_type' );
			$meta['document_number']     = $order->get_meta( '_infakt_' . $new_type . '_number' );
			$meta['document_status']     = $order->get_meta( '_infakt_' . $new_type . '_status' );
			$meta['document_paid_price'] = $order->get_meta( '_infakt_' . $new_type . '_paid_price' );
		}

		return $meta;
	}

	public static function get_infakt_option( $name, $default = false ) {
		$options = get_option( 'woocommerce_integration-infakt_settings', false );
		if ( ! $options ) {
			return $default;
		}

		if ( ! isset( $options[ $name ] ) ) {
			return $default;
		}

		$output = $options[ $name ];

		if ( in_array( $output, [ 'true', 'yes' ] ) ) {
			return true;
		}

		if ( in_array( $output, [ 'false', 'no' ] ) ) {
			return false;
		}

		return $output;
	}

	public static function infakt_format_date( $date = false ) {
		if ( empty( $date ) ) {
			return current_time( 'Y-m-d' );
		}

		if ( $date instanceof WC_DateTime ) {
			return $date->format( 'Y-m-d' );
		}

		return date( 'Y-m-d', strtotime( $date ) );
	}

	public static function infakt_format_price( $price ): string {
		if ( is_string( $price ) ) {
			$price = trim( $price, ',. ' );
			$price = str_replace( ',', '.', $price );
		}

		return number_format( (float) $price, 2, '', '' );
	}

	public static function infakt_get_default_payment_date() {
		$payment_date = self::get_infakt_option( 'invoice_payment_date' );

		if ( $payment_date ) {
			return self::infakt_format_date( sprintf( '+%d days', $payment_date ) );
		}

		return self::infakt_format_date();
	}


}

