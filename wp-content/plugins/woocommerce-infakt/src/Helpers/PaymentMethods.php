<?php

namespace WPDesk\inFakt\Helpers;

class PaymentMethods {

	public static function get() {
		$methods['transfer']        = 'Przelew';
		$methods['cash']            = 'Gotówka';
		$methods['card']            = 'Karta płatnicza';
		$methods['barter']          = 'Barter';
		$methods['check']           = 'Czek';
		$methods['bill_of_sale']    = 'Weksel';
		$methods['delivery']        = 'Za pobraniem';
		$methods['compensation']    = 'Kompensata';
		$methods['accredited']      = 'Akredytywa';
		$methods['paypal']          = 'PayPal';
		$methods['instalment_sale'] = 'Sprzedaż ratalna';
		$methods['payu']            = 'PayU';
		$methods['tpay']            = 'Tpay';
		$methods['przelewy24']      = 'Przelewy24';
		$methods['dotpay']          = 'Dotpay';
		$methods['other']           = 'Inny';

		return $methods;
	}
}
