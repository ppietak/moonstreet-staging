<?php

namespace WPDesk\inFakt\Api;

use WPDesk\inFakt\Exceptions\ResponseErrorException;
use WPDesk\inFakt\Helpers\DocumentHelper;

class Request {

	/**
	 * @var string
	 */
	private $base_url;

	/**
	 * @param string $url
	 */
	public function __construct( string $url ) {
		$this->base_url = $url;
	}

	/**
	 * @param array $args
	 * @param array $data
	 *
	 * @return array
	 * @throws ResponseErrorException Response exception.
	 */
	public function get( array $args = [], array $data = [] ) {
		return (array) $this->request( 'GET', $args, $data );

	}

	/**
	 * @param array $args
	 * @param array $data
	 *
	 * @return array
	 * @throws ResponseErrorException Response exception.
	 */
	public function put( array $args = [], array $data = [] ) {
		return $this->request( 'PUT', $args, $data );
	}

	/**
	 * @param array $args
	 * @param array $data
	 *
	 * @return array
	 * @throws ResponseErrorException Response exception.
	 */
	public function post( array $args = [], array $data = [] ) {
		return $this->request( 'POST', $args, $data );
	}

	/**
	 * @param array $args
	 * @param array $data
	 *
	 * @return array
	 * @throws ResponseErrorException Response exception.
	 */
	public function delete( array $args = [], array $data = [] ) {
		return $this->request( 'DELETE', $args, $data );
	}

	/**
	 * @param string $method
	 * @param array  $args
	 * @param array  $data
	 *
	 * @return mixed|string
	 * @throws ResponseErrorException
	 */
	private function request( string $method = 'GET', array $args = [], array $data = [] ) {
		$url = $this->build_http_url( $method, $args, $data );

		$http_data = [
			'headers' => [
				'X-inFakt-ApiKey' => DocumentHelper::get_infakt_option( 'api_key' ),
				'Content-Type'    => 'application/json',
			],
			'timeout' => 30,
		];

		switch ( $method ) {
			default:
			case 'GET':
				$response = wp_safe_remote_get( $url, $http_data );
				break;
			case 'POST':
				$http_data['body'] = json_encode( $data );
				$response          = wp_safe_remote_post( $url, $http_data );
				break;
			case 'PUT':
				$http_data['body']   = json_encode( $data );
				$http_data['method'] = 'PUT';
				$response            = wp_safe_remote_request( $url, $http_data );
				break;
			case 'DELETE':
				$http_data['method'] = 'DELETE';
				$response            = wp_safe_remote_request( $url, $http_data );
		}

		$this->should_throw_error_exception( $response );
		$body = wp_remote_retrieve_body( $response );

		if ( ! isset( $args['decode'] ) || true === $args['decode'] ) {
			return json_decode( $body, true );
		}

		return $body;
	}

	/**
	 * @param $response
	 *
	 * @throws ResponseErrorException
	 */
	private function should_throw_error_exception( $response ) {
		if ( is_wp_error( $response ) ) {
			throw new ResponseErrorException( $response->get_error_message() );
		}

		$code = wp_remote_retrieve_response_code( $response );
		$body = wp_remote_retrieve_body( $response );

		if ( $code >= 400 ) {
			$body = json_decode( $body, true );
			if ( isset( $body['error'] ) ) {

				$errors = isset( $body['errors'] ) ? (array) $body['errors'] : [];
				throw new ResponseErrorException( $code . ' - ' . $body['error'], $errors );
			}
			throw new ResponseErrorException( 'Unknown request error.' );
		}
	}

	/**
	 * @param string $method
	 * @param array  $args
	 * @param array  $data
	 *
	 * @return string
	 */
	private function build_http_url( string $method = 'GET', array $args = [], array $data = [] ): string {
		$default = [
			'controller' => '',
			'id'         => '',
			'action'     => '',
		];


		$parts = array_map( 'urlencode', wp_parse_args( $args, $default ) );

		$path  = implode( '/', array_filter( $parts, 'strlen' ) );
		$query = '';

		if ( 'GET' === $method && ! empty( $data ) ) {
			$query = '?' . http_build_query( $data );
		}

		return trailingslashit( $this->base_url ) . $path . '.json' . $query;
	}
}
