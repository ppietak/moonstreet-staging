<?php

namespace WPDesk\inFakt\Api;

use Exception;
use Psr\Log\LoggerInterface;
use WC_Order;
use WInfaktVendor\WPDesk\Persistence\Adapter\WordPress\WordpressTransientContainer;
use WPDesk\inFakt\Exceptions\ResponseErrorException;
use WPDesk\inFakt\Helpers\DocumentHelper;
use WPDesk\inFakt\Logger\LoggerIntegration;

class Api {

	const CACHE_TRANSIENT_NAMESPACE = 'infakt_';
	const CACHE_BANK_ACCOUNTS       = 'bank_accounts';
	const CACHE_VAT_EXEMPTIONS      = 'vat_exemptions';
	const CACHE_MARKINGS            = 'markings';
	const CACHE_FLAT_RATES          = 'flat_rates1211';

	const URL = 'https://api.infakt.pl/v3';

	/**
	 * @var Request
	 */
	private $http;

	/**
	 * @var WordpressTransientContainer
	 */
	private $cache;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var bool
	 */
	private $logger_exit;

	/**
	 * @var array
	 */
	private $log_data = [];


	public function __construct( LoggerIntegration $logger_integration ) {
		$this->logger      = $logger_integration->get_logger();
		$this->logger_exit = $logger_integration->should_exit();
		$this->http        = new Request( self::URL );
		$this->cache       = new WordpressTransientContainer( self::CACHE_TRANSIENT_NAMESPACE, HOUR_IN_SECONDS );
	}

	/**
	 * @param array    $args
	 * @param WC_Order $order
	 *
	 * @return array
	 * @throws ResponseErrorException
	 */
	public function create_document( array $args, WC_Order $order ): array {
		$args = apply_filters( 'infakt/core/api/create', $args, $order );
		$this->logger->debug( $args );
		if ( $this->logger_exit ) {
			exit;
		}

		return $this->http->post(
			[ 'controller' => 'invoices' ],
			[ 'invoice' => $args ]
		);
	}

	/**
	 * @param int $id
	 *
	 * @return string
	 * @throws Exception
	 */
	public function download_pdf( int $id ): string {
		if ( ! $id ) {
			throw new Exception( 'Unknown document ID! ' );
		}

		try {
			$pdf = $this->http->get(
				[
					'controller' => 'invoices',
					'id'         => $id,
					'action'     => 'pdf',
					'decode'     => false,
				],
				[
					'document_type' => 'original',
					'locale'        => apply_filters( 'infakt/invoice/language', 'pl' ),
				]
			);
		} catch ( ResponseErrorException $e ) {
			throw new Exception( $e->getMessage() );
		}


		if ( isset( $pdf[0] ) && is_string( $pdf[0] ) ) {
			return $pdf[0];
		}
		throw new Exception( 'Cannot download PDF. Check PDF request.' );
	}

	/**
	 * @param bool $refresh
	 *
	 * @return array
	 */
	public function get_bank_account( bool $refresh = false ): array {
		$bank_accounts = (array) $this->cache->get_fallback( self::CACHE_BANK_ACCOUNTS, [] );
		if ( ! $this->cache->has( self::CACHE_BANK_ACCOUNTS ) || $refresh ) {
			try {
				$response = $this->http->get(
					[ 'controller' => 'bank_accounts' ],
					[ 'offset' => '0', 'limit' => '120' ]
				);

				if ( ! empty( $response['entities'] ) ) {
					foreach ( $response['entities'] as $row ) {
						$account['account_name']   = '';
						$account['bank_name']      = $row['bank_name'];
						$account['account_number'] = $row['account_number'];
						$bank_accounts[]           = $account;

					}
					$this->cache->set( self::CACHE_BANK_ACCOUNTS, $bank_accounts );
				}

			} catch ( ResponseErrorException $e ) {
				$this->logger->error( 'bank accounts error - ' . $e->getMessage() );
			}
		}

		return $bank_accounts;
	}

	/**
	 * @return bool
	 */
	public function is_connected(): bool {
		try {
			$response = $this->http->get(
				[ 'controller' => 'vat_rates' ]
			);
			if ( ! empty( $response['entities'] ) ) {
				return true;
			}
		} catch ( ResponseErrorException $e ) {
			$this->logger->debug( 'infakt connect:', [ $e->getMessage() ] );

			return false;
		}

		return false;
	}

	/**
	 * @param bool $refresh
	 *
	 * @return array
	 */
	public function get_documents_markings( bool $refresh = false ): array {
		$markings = (array) $this->cache->get_fallback( self::CACHE_MARKINGS, [] );
		if ( ! $this->cache->has( self::CACHE_MARKINGS ) || $refresh ) {
			try {
				$response = $this->http->get(
					[
						'controller' => 'documents_markings',
						'action'     => 'incomes',
					],
					[
						'offset' => '0',
						'limit'  => '20',
					]
				);

				if ( ! empty( $response['entities'] ) ) {
					foreach ( $response['entities'] as $row ) {
						$account['id']          = $row['id'];
						$account['code']        = $row['code'];
						$account['name']        = $row['short_description'];
						$account['description'] = $row['description'];
						$markings[]             = $account;
					}
				}
				$this->cache->set( self::CACHE_MARKINGS, $markings );
			} catch ( ResponseErrorException $e ) {
				$this->logger->error( 'markings error - ' . $e->getMessage() );
			}
		}

		return $markings;
	}

	/**
	 * @param bool $refresh
	 *
	 * @return array
	 */
	public function get_flat_rates( bool $refresh = false ): array {
		$rates = (array) $this->cache->get_fallback( self::CACHE_FLAT_RATES, [] );
		if ( ! $this->cache->has( self::CACHE_FLAT_RATES ) || $refresh ) {
			try {
				$response = $this->http->get(
					[
						'controller' => 'flat_rates',
					],
					[
						'offset' => '0',
						'limit'  => '20',
					]
				);

				if ( ! empty( $response['entities'] ) ) {
					foreach ( $response['entities'] as $row ) {
						$account['rate'] = $row['rate'];
						$account['name'] = $row['name'];
						$rates[]         = $account;
					}
				}
				$this->cache->set( self::CACHE_FLAT_RATES, $rates );
			} catch ( ResponseErrorException $e ) {
				$this->logger->error( 'markings error - ' . $e->getMessage() );
			}
		}

		return $rates;
	}

	public function get_vat_exemptions( $refresh = false ) {
		$vat_exemptions = $this->cache->get_fallback( self::CACHE_VAT_EXEMPTIONS, [] );
		if ( ! $this->cache->has( self::CACHE_VAT_EXEMPTIONS ) || $refresh ) {
			try {
				$response = $this->http->get(
					[ 'controller' => 'vat_exemptions' ],
					[ 'offset' => '0', 'limit' => '120' ]
				);
				if ( ! empty( $response['entities'] ) ) {
					foreach ( $response['entities'] as $row ) {
						$vat_exemptions[ $row['id'] ] = $row['description'];
					}
				}
				$this->cache->set( self::CACHE_VAT_EXEMPTIONS, $vat_exemptions );
			} catch ( ResponseErrorException $e ) {
				$this->logger->error( 'exemptions error - ' . $e->getMessage() );
			}
		}

		return $vat_exemptions;
	}

	/**
	 * @param int    $id
	 * @param string $paid_date
	 *
	 * @throws ResponseErrorException
	 */
	public function set_paid( int $id, string $paid_date ) {
		try {
			$this->http->post(
				[
					'controller' => 'invoices',
					'id'         => $id,
					'action'     => 'paid',
				],
				[ 'paid_date' => DocumentHelper::infakt_format_date( $paid_date ) ]
			);
		} catch ( ResponseErrorException $e ) {
			// do nothing.
		}
	}

	public function get_or_create_client( array $args, WC_Order $order ): int {
		$has_client = $this->get_client( $args, $order );
		if ( isset( $has_client['entities'][0]['id'] ) && count( $has_client['entities'] ) === 1 ) {
			return (int) $has_client['entities'][0]['id'];
		} else {
			$create_client = $this->create_client( $args, $order );

			if ( isset( $create_client['id'] ) ) {
				return (int) $create_client['id'];
			}
		}

		return 0;
	}

	private function get_client( $args, $order ): array {
		$args = apply_filters( 'infakt/core/api/client', $args, $order );
		$this->logger->debug( $args );
		if ( $this->logger_exit ) {
			$this->log['get_client'] = $args;

			return [];
		}
		if ( ! empty( $args['client_tax_code'] ) ) {
			// Get client by NIP
			$response = $this->http->get(
				[ 'controller' => 'clients' ],
				[
					'q' => [
						'nip_eq' => $args['client_tax_code'],
					],
				]
			);
		} else {
			// Get client by name, city, postal code.
			$response = $this->http->get(
				[ 'controller' => 'clients' ],
				[
					'q' => [
						'first_name_eq'  => $args['client_first_name'],
						'last_name_eq'   => $args['client_last_name'],
						'city_eq'        => $args['client_city'],
						'postal_code_eq' => $args['client_post_code'],
					],
				]
			);
		}

		return $response;
	}

	private function create_client( $args, $order ) {
		$args     = apply_filters( 'infakt/core/api/client', $args, $order );
		if ( $this->logger_exit ) {
			$this->log['create_client'] = $args;

			return [];
		}
		$response = $this->http->post(
			[ 'controller' => 'clients' ],
			[
				'client' => [
					'company_name'           => $args['client_company_name'],
					'first_name'             => $args['client_first_name'],
					'last_name'              => $args['client_last_name'],
					'street'                 => $args['client_street'],
					'street_number'          => $args['client_street_number'],
					'flat_number'            => $args['client_flat_number'],
					'city'                   => $args['client_city'],
					'country'                => $args['client_country'],
					'postal_code'            => $args['client_post_code'],
					'nip'                    => $args['client_tax_code'],
					'phone_number'           => $args['client_phone_number'],
					'business_activity_kind' => $args['client_company_name'] ? 'other_business' : 'private_person',
				],
			]
		);

		return $response;
	}

}
