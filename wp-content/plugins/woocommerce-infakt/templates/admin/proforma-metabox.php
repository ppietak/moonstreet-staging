<?php

use WPDesk\inFakt\Documents\DocumentType;
use WPDesk\inFakt\Helpers\DocumentHelper;

$params = isset( $params ) ? $params : [];
/**
 * @var \WC_Order $order
 */
$order = $params['order'];

/**
 * @var DocumentType $document
 */
$document             = $params['document'];
$is_generated         = $document->is_generated();
$payment_methods      = $params['payment_methods'];
$order_payment_method = $params['order_payment_method'];

$submit_confirm = 0;
if ( $is_generated ) {
	$submit_confirm = 1;
}

$date = date( 'd-m-Y', strtotime( DocumentHelper::infakt_get_default_payment_date() ) );
?>
<div class="row closed">
	<h3>
		<input
			type="button"
			data-order-id="<?php echo esc_attr( $document->get_order_id() ); ?>"
			data-type="<?php echo esc_attr( $document->get_type() ); ?>"
			data-confirm="<?php echo esc_attr( $submit_confirm ); ?>"
			class="infakt_generate button" name="infakt_action[generate_proform]"
			value="<?php esc_html_e( 'Wystaw proformę', 'woocommerce-infakt' ); ?>"/>
		<a href="#" class="expand"><?php esc_html_e( 'parametry', 'woocommerce-infakt' ); ?> [<span class="sign">+</span>]</a>
	</h3>
	<div class="data">
		<table>
			<tr>
				<th><label for="infakt_payment_date"><?php esc_html_e( 'Termin płatności', 'woocommerce-infakt' ); ?></label></th>
				<td>
					<input id="infakt_payment_date" name="proform[payment_date]" data-mindate="0" type="date" value="<?php echo esc_attr( $date ); ?>"/>
				</td>
			</tr>
			<tr>
				<th><label for="infakt_payment_method"><?php esc_html_e( 'Sposób zapłaty', 'woocommerce-infakt' ); ?></label></th>
				<td>
					<select id="infakt_payment_method" name="invoice[payment_method]">
						<?php foreach ( $payment_methods as $key => $value ): ?>
							<option <?php selected( $order_payment_method, $key ); ?>
								value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value ); ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="infakt_notes"><?php esc_html_e( 'Uwagi', 'woocommerce-infakt' ); ?></label></th>
			</tr>
			<tr>
				<td colspan="2"><textarea id="infakt_notes" name="proform[notes]" rows="4"><?php printf( esc_html__( 'Zamówienie #%s' ), $order->get_order_number() ); ?></textarea>
				</td>
			</tr>
		</table>
	</div>
</div>
