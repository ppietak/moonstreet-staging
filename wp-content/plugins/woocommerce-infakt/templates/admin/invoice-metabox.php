<?php

use WPDesk\inFakt\Documents\DocumentType;
use WPDesk\inFakt\Helpers\DocumentHelper;

$params = isset( $params ) ? $params : [];

/**
 * @var \WC_Order $order
 */
$order = $params['order'];

/**
 * @var DocumentType $document
 */
$document             = $params['document'];
$is_generated         = $document->is_generated();
$payment_methods      = $params['payment_methods'];
$order_payment_method = $params['order_payment_method'];
$markings             = $params['markings'];
$total                = $params['total'];
$submit_confirm       = 0;
if ( $is_generated ) {
	$submit_confirm = 1;
}

if ( ! isset( $payment_methods[ $order_payment_method ] ) ) {
	$order_payment_method = 'other';
}

$invoice_document_markings = DocumentHelper::get_infakt_option( 'invoice_document_markings', '' );
?>
<div class="row closed">
	<h3><input type="button" data-order-id="<?php echo $document->get_order_id(); ?>" data-type="<?php echo $document->get_type(); ?>" data-confirm="<?php echo $submit_confirm; ?>" class="infakt_generate button" name="infakt_action[generate_invoice]" value="<?php _e('Wystaw fakturę', 'woocommerce-infakt'); ?>" /> <a href="#" class="expand"><?php _e('parametry', 'woocommerce-infakt'); ?> [<span class="sign">+</span>]</a></h3>
	<div class="data">
		<table>
			<tr>
				<th><?php _e('Zapłacono', 'woocommerce-infakt'); ?></th>
				<td><input id="infakt_paid_price" name="invoice[paid_price]" type="text" value="<?php echo number_format( $total, 2, '.', ''); ?>" /></td>
			</tr>
			<tr>
				<th><?php _e('Sposób zapłaty', 'woocommerce-infakt'); ?></th>
				<td>
					<select id="infakt_payment_method" name="invoice[payment_method]">
						<?php foreach( $payment_methods as $key => $value ): ?>
							<option <?php selected( $order_payment_method, $key ); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<th><?php _e('Oznaczenie transakcji przychodowej ', 'woocommerce-infakt'); ?></th>
				<td>
					<select id="invoice_document_markings" name="invoice[document_markings]">
						<?php foreach( $markings as $key => $value ): ?>
							<option <?php selected( $key, $invoice_document_markings ); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<th><?php _e('Uwagi', 'woocommerce-infakt'); ?></th>
			</tr>
			<tr>
				<td colspan="2"><textarea id="infakt_notes" name="invoice[notes]" rows="4"><?php printf(__('Zamówienie #%s'), $order->get_order_number() ); ?></textarea></td>
			</tr>
		</table>
	</div>
</div>

