<?php

/**
 * @var \WC_Order $order
 */
$params     = isset( $params ) ? $params : [];
$documents  = $params['documents'];
$is_company = $is_company ?? false;
?>
<div class="row">
	<h3><?php _e( 'Wystawiono', 'woocommerce-infakt' ); ?></h3>
	<div class="data">
		<?php if ( $is_company ): ?>
			<?php _e( 'Pola nazwa firmy i NIP muszą być wypełnione!', 'woocommerce-infakt' ); ?>
		<?php else: ?>
			<?php
			foreach ( $documents as $type => $document ) {
				$name   = 'infakt_error_' . $document->get_order_id() . '_' . $document->get_type();
				$errors = get_transient( $name );
				if ( ! empty( $errors ) ) {
					$error_details = '';
					foreach ( $errors['errors'] as $error_detail ) {
						$error_details .= '<br/>' . $error_detail;
					}
					$error_html = '<p class="response-error">' . $document->get_label() . ': ' . $errors['message'] . $error_details . '</p>';
				} else {
					$error_html = '<p class="response-error"></p>';
				}
				$is_generated = $document->is_generated();

				if ( ! $is_generated ) {
					$ask_invoice = $document->is_customer_want_document();
					$html        = '<p>' . sprintf( __( '%s Brak!', 'woocommerce-infakt' ), $document->get_label() . ':' ) . '</p>';
					if ( ! $is_generated && $ask_invoice ) {
						$html = '<p class="description">' . sprintf( __( '%s Klient chce otrzymać fakturę!', 'woocommerce-infakt' ), $document->get_label() . ':' ) . '</p>';
					}
					echo '<div class="generate-wrapper-' . $type . '">' . $error_html . $html . '</div>';
				} else {
					$document_number = $document->get_number();
					$document_url    = $document->get_url();
					echo '<div class="generate-wrapper-' . $type . '"><p><a href="' . $document_url . '" target="_blank">' . $document->get_label() . ' ' . $document_number . '</a></p></div>';
				}
			}

			?>
		<?php endif; ?>
	</div>
</div>

