<?php
/**
 * Plugin Name: WooCommerce inFakt
 * Plugin URI: https://www.wpdesk.pl/sklep/infakt-woocommerce/
 * Description: Integracja WooCommerce z inFakt.
 * Version: 1.9.9
 * Author: WP Desk
 * Author URI: https://www.wpdesk.pl/
 * Text Domain: woocommerce-infakt
 * Domain Path: /lang/
 * Requires at least: 5.8
 * Tested up to: 6.4
 * WC requires at least: 8.1
 * WC tested up to: 8.4
 * Requires PHP: 7.4
 *
 * Copyright 2021 WP Desk Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package WPDesk\inFakt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/* Plugin release time */
$plugin_release_timestamp = '2021-11-29 10:35';
/* Plugin version */
$plugin_version = '1.9.9';


$plugin_name        = 'WooCommerce inFakt';
$plugin_class_name  = '\WPDesk\inFakt\Plugin';
$plugin_text_domain = 'woocommerce-infakt';
$product_id         = 'WooCommerce inFakt';
$plugin_file        = __FILE__;
$plugin_dir         = __DIR__;

$requirements = [
	'php'     => '7.4',
	'wp'      => '5.5',
	'plugins' => [
		[
			'name'      => 'woocommerce/woocommerce.php',
			'nice_name' => 'WooCommerce',
		],
	],
];

add_action( 'before_woocommerce_init', function() {
	if ( class_exists( '\Automattic\WooCommerce\Utilities\FeaturesUtil' ) ) {
		\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'custom_order_tables', __FILE__, true );
	}
} );

require __DIR__ . '/vendor_prefixed/wpdesk/wp-plugin-flow-common/src/plugin-init-php52.php';
