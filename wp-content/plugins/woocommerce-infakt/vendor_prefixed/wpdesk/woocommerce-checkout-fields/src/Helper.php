<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

/**
 * Main class for integrate library with plugin.
 *
 * @package WPDesk\Library\CheckoutFields
 */
class Helper
{
    public static function array_insert_after(array $fields, $key, array $new)
    {
        $keys = \array_keys($fields);
        $index = \array_search($key, $keys, \true);
        $pos = \false === $index ? \count($fields) : $index + 1;
        return \array_merge(\array_slice($fields, 0, $pos), $new, \array_slice($fields, $pos));
    }
    public static function get_user_field_data(\WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData $field, $type = '') : array
    {
        return [$field->get_name($type) => ['label' => $field->get_label(), 'description' => $field->get_description(), 'type' => $field->get_type()]];
    }
}
