<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

class Address
{
    /**
     * @var FieldData[]
     */
    private $checkout_fields;
    public function __construct(array $checkout_fields)
    {
        $this->checkout_fields = $checkout_fields;
    }
    public function hooks()
    {
        \add_filter('woocommerce_get_order_address', [$this, 'woocommerce_get_order_address_filter'], 100, 3);
        \add_filter('woocommerce_order_formatted_billing_address', [$this, 'woocommerce_order_formatted_billing_address_filter'], 100, 2);
        \add_filter('woocommerce_order_formatted_shipping_address', [$this, 'woocommerce_order_formatted_shipping_address_filter'], 100, 2);
        //add_filter( 'woocommerce_localisation_address_formats', [ $this, 'woocommerce_localisation_address_formats_filter' ], 100 );
        //add_filter( 'woocommerce_formatted_address_replacements', [ $this, 'woocommerce_formatted_address_replacements_filter' ], 100, 2 );
    }
    public function woocommerce_get_order_address_filter($address, $type, \WC_Order $order)
    {
        $checkout_fields = $this->checkout_fields;
        foreach ($checkout_fields as $checkout_field) {
            if ($checkout_field->is_billing() && $type === 'billing') {
                $field[$checkout_field->get_name()] = $order->get_meta($checkout_field->get_meta_name('b'));
                $address = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($address, 'address_2', $field);
            }
            if ($checkout_field->is_shipping() && $type === 'shipping') {
                $field[$checkout_field->get_name()] = $order->get_meta($checkout_field->get_meta_name('s'));
                $address = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($address, 'address_2', $field);
            }
        }
        return $address;
    }
    public function woocommerce_order_formatted_billing_address_filter($address, $order)
    {
        foreach ($this->checkout_fields as $checkout_field) {
            if ($checkout_field->is_billing()) {
                $address[$checkout_field->get_name()] = $order->get_meta($checkout_field->get_meta_name('b'));
            }
        }
        return $address;
    }
    public function woocommerce_order_formatted_shipping_address_filter($address, $order)
    {
        foreach ($this->checkout_fields as $checkout_field) {
            if ($checkout_field->is_shipping()) {
                $address[$checkout_field->get_name()] = $order->get_meta($checkout_field->get_meta_name('s'));
            }
        }
        return $address;
    }
    public function woocommerce_localisation_address_formats_filter($formats)
    {
        $fields = [];
        foreach ($this->checkout_fields as $checkout_field) {
            if ($checkout_field->get_show_in_localisation()) {
                $fields[] = "{" . $checkout_field->get_name() . "}\n";
            }
        }
        if (empty($fields)) {
            return $formats;
        }
        foreach ($formats as $country => $val) {
            $replacement_fields = \implode(' ', $fields);
            $formats[$country] = $val . $replacement_fields;
        }
        return $formats;
    }
    public function woocommerce_formatted_address_replacements_filter($fields, $args)
    {
        foreach ($this->checkout_fields as $checkout_field) {
            $value = isset($args[$checkout_field->get_name()]) ? $args[$checkout_field->get_name()] : '';
            if ($checkout_field->get_type() === 'checkbox') {
                $checkbox_value = $value === '1' ? \__('Tak', 'flexible-invoices-core') : \__('Nie', 'flexible-invoices-core');
                $fields['{' . $checkout_field->get_name() . '}'] = $checkout_field->get_label() . ':  ' . $checkbox_value;
            } else {
                $fields['{' . $checkout_field->get_name() . '}'] = $args[$checkout_field->get_name()];
            }
        }
        return $fields;
    }
}
