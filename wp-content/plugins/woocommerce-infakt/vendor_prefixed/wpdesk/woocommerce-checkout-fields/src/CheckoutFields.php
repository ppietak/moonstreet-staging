<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

use WInfaktVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WInfaktVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
/**
 * Main class for integrate library with plugin.
 *
 * @package WPDesk\Library\CheckoutFields
 */
class CheckoutFields
{
    use HookableParent;
    /**
     * @var FieldData[]
     */
    private $checkout_fields;
    public function __construct(array $checkout_fields)
    {
        $this->checkout_fields = $checkout_fields;
    }
    public function hooks() : void
    {
        \add_filter('woocommerce_billing_fields', [$this, 'woocommerce_billing_fields_filter'], 100);
        \add_filter('woocommerce_admin_billing_fields', [$this, 'woocommerce_admin_billing_fields_filter'], 100);
        \add_filter('woocommerce_shipping_fields', [$this, 'woocommerce_shipping_fields_filter'], 100);
        \add_filter('woocommerce_admin_shipping_fields', [$this, 'woocommerce_admin_shipping_fields_filter'], 100);
    }
    public function woocommerce_billing_fields_filter($fields) : array
    {
        $checkout_fields = $this->get_checkout_fields();
        foreach ($checkout_fields as $checkout_field) {
            if ($checkout_field->is_billing()) {
                $field[$checkout_field->get_name('b')] = $checkout_field->get_data();
                $fields = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($fields, $checkout_field->get_position('billing'), $field);
            }
        }
        return $fields;
    }
    public function woocommerce_admin_billing_fields_filter($fields) : array
    {
        $checkout_fields = $this->get_checkout_fields();
        foreach ($checkout_fields as $checkout_field) {
            if ($checkout_field->is_billing()) {
                $data = $checkout_field->get_admin_data();
                $field[$checkout_field->get_name()] = $this->prepare_admin_field_data_value($data);
                $fields = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($fields, $checkout_field->get_position(), $field);
            }
        }
        return $fields;
    }
    public function woocommerce_shipping_fields_filter($fields) : array
    {
        $checkout_fields = $this->get_checkout_fields();
        foreach ($checkout_fields as $checkout_field) {
            if ($checkout_field->is_shipping()) {
                $fields[$checkout_field->get_name('s')] = $checkout_field->get_data();
            }
        }
        return $fields;
    }
    public function woocommerce_admin_shipping_fields_filter($fields) : array
    {
        $checkout_fields = $this->get_checkout_fields();
        foreach ($checkout_fields as $checkout_field) {
            if ($checkout_field->is_shipping()) {
                $data = $checkout_field->get_admin_data();
                $field[$checkout_field->get_name()] = $this->prepare_admin_field_data_value($data);
                $fields = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($fields, $checkout_field->get_position(), $field);
            }
        }
        return $fields;
    }
    private function prepare_admin_field_data_value(array $data) : array
    {
        $data['class'] = 'short';
        if ($data['type'] === 'checkbox') {
            $data['type'] = 'select';
            $data['options'] = ['0' => 'Nie', '1' => 'Tak'];
        }
        return $data;
    }
    /**
     * @return FieldDataGetters[]
     */
    private function get_checkout_fields() : array
    {
        return $this->checkout_fields;
    }
}
