<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

class User
{
    /**
     * @var FieldData[]
     */
    private $checkout_fields;
    public function __construct(array $checkout_fields)
    {
        $this->checkout_fields = $checkout_fields;
    }
    /**
     * Fires hooks
     */
    public function hooks()
    {
        \add_filter('woocommerce_customer_meta_fields', [$this, 'woocommerce_customer_meta_fields'], 100);
        \add_action('woocommerce_checkout_update_user_meta', [$this, 'save_checkout_data'], 100, 2);
    }
    public function woocommerce_customer_meta_fields($fields)
    {
        foreach ($this->checkout_fields as $checkout_field) {
            if ($checkout_field->get_in_account()) {
                if ($checkout_field->is_billing() && $fields['billing']['fields']) {
                    $fields['billing']['fields'] = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($fields['billing']['fields'], $checkout_field->get_position('billing'), \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::get_user_field_data($checkout_field, 'b'));
                }
                if ($checkout_field->is_shipping() && $fields['shipping']['fields']) {
                    $fields['shipping']['fields'] = \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::array_insert_after($fields['shipping']['fields'], $checkout_field->get_position('shipping'), \WInfaktVendor\WPDesk\Library\CheckoutFields\Helper::get_user_field_data($checkout_field, 's'));
                }
            }
        }
        return $fields;
    }
    /**
     * Update customer vat number.
     *
     * @param int   $user_id   User ID.
     * @param array $post_data Post data.
     *
     * @internal You should not use this directly from another application
     */
    public function save_checkout_data($user_id, $post_data)
    {
        if ($user_id) {
            foreach ($this->checkout_fields as $checkout_field) {
                if ($checkout_field->is_billing()) {
                    \update_user_meta($user_id, $checkout_field->get_name('b'), \sanitize_text_field(\wp_unslash($post_data[$checkout_field->get_name('b')])));
                }
                if ($checkout_field->is_shipping()) {
                    \update_user_meta($user_id, $checkout_field->get_name('s'), \sanitize_text_field(\wp_unslash($post_data[$checkout_field->get_name('s')])));
                }
            }
        }
    }
}
