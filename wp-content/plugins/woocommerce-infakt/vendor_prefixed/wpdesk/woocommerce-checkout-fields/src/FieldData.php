<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

class FieldData implements \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldDataGetters
{
    const BILLING_PREFIX_META = '_billing_';
    const SHIPPING_PREFIX_META = '_shipping_';
    /**
     * @var bool
     */
    protected $show_in_account = \true;
    /**
     * @var bool
     */
    protected $is_billing = \true;
    /**
     * @var bool
     */
    protected $is_shipping = \false;
    protected $show_in_localisation = \false;
    /**
     * @var array
     */
    private $field_declaration = ['name' => 'field_name', 'type' => 'text', 'required' => \false, 'label' => '', 'description' => '', 'placeholder' => '', 'clear' => \true, 'class' => ['form-row-wide'], 'label_class' => [], 'priority' => 100, 'attributes' => [], 'position' => 'phone', 'show' => \true];
    public function __construct($name, $label)
    {
        $this->set_name($name);
        $this->set_label($label);
    }
    public function set_name(string $name) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['name'] = $name;
        return $this;
    }
    /**
     * @param string $type ('' = empty, b = billing, s = shipping )
     *
     * @return string
     */
    public function get_name(string $type = '') : string
    {
        if ($type === 'b') {
            $prefix = 'billing_';
        } elseif ($type === 's') {
            $prefix = 'shipping_';
        } else {
            $prefix = '';
        }
        return $prefix . $this->field_declaration['name'];
    }
    /**
     * @param string $type ( b = billing, s = shipping )
     *
     * @return string
     */
    public function get_meta_name(string $type = 'b') : string
    {
        $prefix = self::SHIPPING_PREFIX_META;
        if ($type === 'b') {
            $prefix = self::BILLING_PREFIX_META;
        }
        return $prefix . $this->field_declaration['name'];
    }
    public function get_description()
    {
        return $this->field_declaration['description'];
    }
    public function set_description(string $description) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['description'] = $description;
        return $this;
    }
    public function set_type(string $type) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['type'] = $type;
        return $this;
    }
    public function get_type() : string
    {
        return $this->field_declaration['type'];
    }
    public function set_required() : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['required'] = \true;
        return $this;
    }
    public function get_required() : bool
    {
        return $this->field_declaration['required'];
    }
    public function set_position(string $position) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['position'] = $position;
        return $this;
    }
    /**
     * @param string $type ('' = empty, b = billing, s = shipping )
     *
     * @return string
     */
    public function get_position(string $type = '') : string
    {
        if ($type === 'b') {
            $prefix = 'billing_';
        } elseif ($type === 's') {
            $prefix = 'shipping_';
        } else {
            $prefix = '';
        }
        return $prefix . $this->field_declaration['position'];
    }
    public function set_label(string $label) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['label'] = $label;
        return $this;
    }
    public function get_label() : string
    {
        return $this->field_declaration['label'];
    }
    public function set_placeholder(string $placeholder) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['placeholder'] = $placeholder;
        return $this;
    }
    public function get_placeholder() : string
    {
        return $this->field_declaration['placeholder'];
    }
    public function set_class(array $class = []) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['class'] = $class;
        return $this;
    }
    public function get_class() : array
    {
        return $this->field_declaration['class'];
    }
    public function set_label_class(array $label_class) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['label_class'] = $label_class;
        return $this;
    }
    public function get_label_class() : array
    {
        return $this->field_declaration['label_class'];
    }
    public function set_priority(int $priority) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['priority'] = $priority;
        return $this;
    }
    public function get_priority() : int
    {
        return $this->field_declaration['priority'];
    }
    public function set_attributes(array $attributes) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['attributes'] = $attributes;
        return $this;
    }
    public function get_attributes() : array
    {
        return $this->field_declaration['attributes'];
    }
    public function get_in_account() : bool
    {
        return $this->show_in_account;
    }
    public function set_in_account(bool $value) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->show_in_account = $value;
        return $this;
    }
    public function set_show(bool $show) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->field_declaration['show'] = $show;
        return $this;
    }
    public function get_show() : bool
    {
        return $this->field_declaration['show'];
    }
    public function show_in_shipping(bool $value) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->is_shipping = $value;
        return $this;
    }
    public function is_shipping() : bool
    {
        return $this->is_shipping;
    }
    public function show_in_billing(bool $value) : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->is_billing = $value;
        return $this;
    }
    public function is_billing() : bool
    {
        return $this->is_billing;
    }
    public function show_in_localisation() : \WInfaktVendor\WPDesk\Library\CheckoutFields\FieldData
    {
        $this->show_in_localisation = \true;
        return $this;
    }
    public function get_show_in_localisation() : bool
    {
        return $this->show_in_localisation;
    }
    public function get_data() : array
    {
        return $this->field_declaration;
    }
    public function get_admin_data() : array
    {
        return ['label' => $this->get_label(), 'description' => $this->get_description(), 'show' => $this->get_show(), 'type' => $this->get_type()];
    }
}
