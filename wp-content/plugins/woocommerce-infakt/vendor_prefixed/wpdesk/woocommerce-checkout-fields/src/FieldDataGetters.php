<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

interface FieldDataGetters
{
    public function get_name(string $type = '');
    public function get_description();
    public function get_type() : string;
    public function get_required() : bool;
    public function get_label() : string;
    public function get_placeholder() : string;
    public function get_class() : array;
    public function get_label_class() : array;
    public function get_priority() : int;
    public function get_attributes() : array;
    public function get_in_account() : bool;
    public function get_position(string $type = '') : string;
    /**
     * Get all field data.
     *
     * @return array
     */
    public function get_data() : array;
    /**
     * Get data for admin field declaration.
     *
     * @return array
     */
    public function get_admin_data() : array;
    /**
     * Should show this field in shipping section.
     *
     * @return bool
     */
    public function is_shipping() : bool;
    /**
     * Should show this field in billing section.
     *
     * @return bool
     */
    public function is_billing() : bool;
    /**
     * Should show in address localisation.
     *
     * @return bool
     */
    public function get_show_in_localisation() : bool;
}
