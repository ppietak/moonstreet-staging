<?php

namespace WInfaktVendor\WPDesk\Library\CheckoutFields;

/**
 * Main class for integrate library with plugin.
 *
 * @package WPDesk\Library\CheckoutFields
 */
class Integration
{
    /**
     * @var FieldData[]
     */
    private $checkout_fields;
    public function __construct(array $checkout_fields)
    {
        $this->checkout_fields = $checkout_fields;
    }
    public function hooks()
    {
        (new \WInfaktVendor\WPDesk\Library\CheckoutFields\CheckoutFields($this->checkout_fields))->hooks();
        (new \WInfaktVendor\WPDesk\Library\CheckoutFields\Address($this->checkout_fields))->hooks();
        (new \WInfaktVendor\WPDesk\Library\CheckoutFields\User($this->checkout_fields))->hooks();
    }
}
