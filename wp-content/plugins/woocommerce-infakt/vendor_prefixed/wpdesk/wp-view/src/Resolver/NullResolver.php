<?php

namespace WInfaktVendor\WPDesk\View\Resolver;

use WInfaktVendor\WPDesk\View\Renderer\Renderer;
use WInfaktVendor\WPDesk\View\Resolver\Exception\CanNotResolve;
/**
 * This resolver never finds the file
 *
 * @package WPDesk\View\Resolver
 */
class NullResolver implements \WInfaktVendor\WPDesk\View\Resolver\Resolver
{
    public function resolve($name, \WInfaktVendor\WPDesk\View\Renderer\Renderer $renderer = null)
    {
        throw new \WInfaktVendor\WPDesk\View\Resolver\Exception\CanNotResolve("Null Cannot resolve");
    }
}
