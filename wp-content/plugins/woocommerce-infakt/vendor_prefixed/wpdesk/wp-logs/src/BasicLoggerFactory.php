<?php

namespace WInfaktVendor\WPDesk\Logger;

use WInfaktVendor\Monolog\Handler\HandlerInterface;
use WInfaktVendor\Monolog\Logger;
use WInfaktVendor\Monolog\Registry;
/**
 * Manages and facilitates creation of logger
 *
 * @package WPDesk\Logger
 */
class BasicLoggerFactory implements \WInfaktVendor\WPDesk\Logger\LoggerFactory
{
    /** @var string Last created logger name/channel */
    private static $lastLoggerChannel;
    /**
     * Creates logger for plugin
     *
     * @param string $name The logging channel/name of logger
     * @param HandlerInterface[] $handlers Optional stack of handlers, the first one in the array is called first, etc.
     * @param callable[] $processors Optional array of processors
     * @return Logger
     */
    public function createLogger($name, $handlers = array(), array $processors = array())
    {
        if (\WInfaktVendor\Monolog\Registry::hasLogger($name)) {
            return \WInfaktVendor\Monolog\Registry::getInstance($name);
        }
        self::$lastLoggerChannel = $name;
        $logger = new \WInfaktVendor\Monolog\Logger($name, $handlers, $processors);
        \WInfaktVendor\Monolog\Registry::addLogger($logger);
        return $logger;
    }
    /**
     * Returns created Logger by name or last created logger
     *
     * @param string $name Name of the logger
     *
     * @return Logger
     */
    public function getLogger($name = null)
    {
        if ($name === null) {
            $name = self::$lastLoggerChannel;
        }
        return \WInfaktVendor\Monolog\Registry::getInstance($name);
    }
}
