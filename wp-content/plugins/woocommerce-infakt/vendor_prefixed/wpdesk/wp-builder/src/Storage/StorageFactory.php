<?php

namespace WInfaktVendor\WPDesk\PluginBuilder\Storage;

class StorageFactory
{
    /**
     * @return PluginStorage
     */
    public function create_storage()
    {
        return new \WInfaktVendor\WPDesk\PluginBuilder\Storage\WordpressFilterStorage();
    }
}
