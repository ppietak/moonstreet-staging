<?php

namespace WInfaktVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassNotExists extends \RuntimeException
{
}
