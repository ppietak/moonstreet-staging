<?php

namespace WInfaktVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassAlreadyExists extends \RuntimeException
{
}
