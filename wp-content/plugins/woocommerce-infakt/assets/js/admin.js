( function ( $ ) {
	$( '#infakt .row .expand' ).click( function ( event ) {
		var $this = $( this ),
			row = $this.closest( '.row' );

		event.preventDefault();
		row.toggleClass( 'closed' );

		var sign = $this.find( '.sign' );
		if ( ! row.hasClass( 'closed' ) ) {
			sign.text( '-' );
		} else {
			sign.text( '+' );
		}
	} );

	if ( $.fn.selectWoo ) {
		let vat_exemption_reason_select = jQuery( '#woocommerce_integration-infakt_vat_exemption_reason' );
		if ( vat_exemption_reason_select.length ) {
			$( '#woocommerce_integration-infakt_vat_exemption_reason' ).selectWoo();
		}

		let select_woo_multiple = jQuery( '.woo-select2-multiple' );
		if ( select_woo_multiple.length ) {
			$( '.woo-select2-multiple' ).selectWoo( {
				multiple: true
			} );
		}
	}

	jQuery( '.infakt_generate' ).click( function () {
		let type = jQuery( this ).attr( 'data-type' );
		let order_id = jQuery( this ).attr( 'data-order-id' );
		let do_confirm = jQuery( this ).attr( 'data-confirm' );
		let wrapper = jQuery( '.generate-wrapper-' + type );
		var post_data = jQuery( this ).closest( '.row' ).find( '.data input, .data select, .data textarea' ).serialize();
		var data = {
			'action': 'generate_document',
			'type': type,
			'order_id': order_id,
			'post_data': post_data,
			'security': _infakt_admin.security,
		};

		let r = false;
		if ( do_confirm === '1' ) {
			r = confirm( 'Czy chcesz wystawić ponownie dokument?' );
			if ( r === true ) {
				issue_document( data, wrapper )
			} else {
				return false;
			}
		} else {
			issue_document( data, wrapper )
		}

		jQuery( this ).attr( 'data-confirm', 1 );

		return false;
	} );

	function issue_document( data, wrapper ) {
		let type = data.type;
		var wrap_doc = jQuery( '.generate-wrapper-' + type );
		var wrap = jQuery( '.generate-wrapper-' + type + ' .response-error' );
		wrap_doc.html( '<span class="spinner" style="margin: 0 0; float: none !important; visibility: visible !important;"></span>' );
		jQuery( '.generate-empty' ).hide();
		jQuery.post( _infakt_admin.ajax_url, data, function ( response ) {
			if ( ! response.success ) {
				let errors = response.data.details;
				let error_details = '<br/>';
				if ( errors.length > 0 ) {
					for ( i = 0; i < errors.length; i++ ) {
						error_details += errors[ i ];
						error_details += '<br/> ';
					}
				}
				wrapper.html( '<div class="response-error">' + response.data.message + error_details + '</div>' );
			} else {
				let number = response.data.details.document_formatted_number;
				let url = response.data.details.download_url;
				let link = '<p><a href="' + url + '" target="_blank">' + number + '</a></p>';
				wrapper.html( link );
			}
		} );
	}


	var infakt_menu_wrapper = jQuery( '.infakt-js-nav-tab-wrapper a' );
	infakt_menu_wrapper.click( function () {
		let active_tab = jQuery( this ).attr( 'href' ).replace( '#', '' );
		jQuery( this ).parent().find( '.nav-tab-active' ).removeClass( 'nav-tab-active' );
		jQuery( this ).addClass( 'nav-tab-active' );
		jQuery( '.infakt-form-table' ).hide();
		jQuery( '.table-' + active_tab ).show();
		wpCookies.set( 'infakt-settings-tab', active_tab );
	} );

	var tab_cookie = wpCookies.get( 'infakt-settings-tab' );
	if ( tab_cookie ) {
		var tab_element = jQuery( '.nav-tab-' + tab_cookie );
		tab_element.click();
	} else {
		infakt_menu_wrapper.first().click();
	}


	function infakt_lump_sum() {
		let element = jQuery( '#woocommerce_integration-infakt_lump_sum_enabled' );
		if ( element.length ) {
			let checked = element.is( ':checked' );
			let lump_sum_elements = jQuery( '.infakt-lump-sum' ).closest( 'tr' );
			if ( checked ) {
				lump_sum_elements.show();
			} else {
				lump_sum_elements.hide();
			}
		}
	}

	jQuery( '#woocommerce_integration-infakt_lump_sum_enabled' ).change( function () {
		infakt_lump_sum();
	} );
	infakt_lump_sum();

	function infakt_debug() {
		let element = jQuery( '#woocommerce_integration-infakt_debug_mode' );
		if ( element.length ) {
			let checked = element.is( ':checked' );
			let debug_elements = jQuery( '.infakt-debug-mode' ).closest( 'tr' );
			if ( checked ) {
				debug_elements.show();
			} else {
				debug_elements.hide();
			}
		}
	}

	jQuery( '#woocommerce_integration-infakt_debug_mode' ).change( function () {
		infakt_debug();
	} );
	infakt_debug();


} )( jQuery );
