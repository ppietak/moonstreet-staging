(function($) {

	let invoice_filed_ask = jQuery('#billing_invoice_ask');
	let field_nip = jQuery('#billing_company_nip_field');
	if( invoice_filed_ask.length ) {
		field_nip.hide();
		if( invoice_filed_ask.prop('checked') ) {
			field_nip.show();
		}

		invoice_filed_ask.click( function( e ) {
			if( $(this).prop('checked') ) {
				field_nip.show();
			} else {
				field_nip.hide();
			}
		});
	}

})(jQuery);
