jQuery(document).ready(function(a) {
    const email = document.querySelector('#billing_email');
    const first_name_field = document.querySelector('#billing_first_name');
    const last_name_field = document.querySelector('#billing_last_name');
    const signup = document.querySelector('#woo_ml_subscribe');

    if (jQuery('#woo_ml_preselect_enabled')?.val() == 'yes') {
        jQuery('#woo_ml_subscribe').prop('checked', true);
    }

    if (email !== null) {
        email.addEventListener('blur', (event) => {
            validateMLSub();
        });
    }

    if (first_name_field !== null) {
        first_name_field.addEventListener('blur', (event) => {
            if(first_name_field.value.length > 0) {
                validateMLSub();
            }
        });
    }

    if (last_name_field !== null) {
        last_name_field.addEventListener('blur', (event) => {
            if(last_name_field.value.length > 0) {
                validateMLSub();
            }
        });
    }

    if (signup !== null) {
        a(document).on('change', signup, function(event) {
            validateMLSub();
        });
    }

    function validateMLSub() {
        if(email !== null && email.value.length > 0) {
            checkoutMLSub();
        }
    }

    function checkoutMLSub() {
        /** set cookie before sending request to server
         * since multiple checkout update requests can be sent
         * and server cookies won't get updated, so send the saved
         * cookie as a request parameter
        **/

        if (!getCookie('mailerlite_checkout_token')) {
            var now = new Date();
            now.setTime(now.getTime() + 48 * 3600 * 1000);
            document.cookie = `mailerlite_checkout_token=${(+new Date).toString()}; expires=${now.toUTCString()}; path=/`;
        }

        const accept_marketing = document.querySelector('#woo_ml_subscribe').checked;

        let first_name = '';
        let last_name = '';

        if (first_name_field !== null) {
            first_name = first_name_field.value;
        }

        if (last_name_field !== null) {
            last_name = last_name_field.value;
        }

        jQuery.ajax({
            url: woo_ml_public_post.ajax_url,
            type: "post",
            data: {
                action: "post_woo_ml_email_cookie",
                email: email.value,
                signup: accept_marketing,
                language: woo_ml_public_post.language,
                first_name: first_name,
                last_name: last_name,
                cookie_mailerlite_checkout_token:getCookie('mailerlite_checkout_token')
            }
        })
    }
});

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) {
        return parts.pop().split(';').shift()
    }
    return null;
}